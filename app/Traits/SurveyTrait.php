<?php

namespace App\Traits;
// model
use App\Models\Marketing\Motif;
use App\Models\Report\Ranking;
use App\Models\Report\Brand;
use App\Models\Report\DepoToko;
use App\Models\Report\Depo;
use App\Models\Blog\Ukuran;
use App\Models\Marketing\NotifSurvey;
use App\Models\Marketing\DepoSurvey;
use App\Models\Marketing\ImageSurvey;
use App\Models\Marketing\DataPenjualanSurvey;
use App\Models\Marketing\StatusSurvey;
use App\Models\Report\BarangPenjualan;
use App\Models\Report\Regional;

// library
use DataTables;
use PDF;
use Spatie\Image\Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

use \Carbon\Carbon;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Spatie\ImageOptimizer\OptimizerChain;
use Spatie\ImageOptimizer\Optimizers\Jpegoptim;
use Spatie\ImageOptimizer\Optimizers\Pngquant;

trait SurveyTrait
{
    public $brand,$size,$depo, $regionalDepo;
    public $survey;
    public $survey_by_year = null;
    public $tot_vote = null;
    public $tot_jual = null;
    public $tot_order = null;
    public $image = null;
    public $bulan = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    
    public function boot_survey()
    {
        $this->depo = $this->get_all_depo();
        $this->regionalDepo = $this->get_reg_depo();
        $this->brand = $this->get_brand();
        $this->size = $this->get_size();
    }
    // generate random nomer survey
    public function no_survey()
    {
        return  'C'.Str::upper(Str::random(1)).'J'.Str::upper(Str::random(1)).'F'.Str::upper(Str::random(1)).'I'.'-'.strtotime('now');
    }
    // get all survey status by id;
    public function get_survey_by($id)
    {
        $this->survey = NotifSurvey::with(['depo_survey','depo_detail','status','ukuran','image'])->find($id);
        return $this;
    }
    public function cek_status_code(array $status)
    {
        return in_array($this->survey->status_id,$status);
    }
    public function pdf_print_survey($data)
    {
        $survey = collect($this->data_pdf_print_survey($data));
        $pdf = PDF::loadView('backend._report.pdf_print_survey',compact('survey'))
                        ->setPaper('a4')
                        ->setOrientation('landscape')
                        ->setOption('margin-bottom', 0);
        return $pdf->inline($survey['no_survey'].'.pdf');
    }   
    protected function get_vote($vote,$order,$customer)
    {
        $vote = json_decode($vote, true);
        $order = json_decode($order, true);
        $this->tot_vote = array_replace(['1'=>0,'2'=>0,'3'=>0,'4'=>0,'5'=>0],array_count_values($vote));
        $result = array();
        foreach (array_values($order) as $i => $element) {
            $key = array_keys($element);
            foreach ($key as $color) {
                $result[$color][] = $element[$color];
            }
        }
        $this->tot_order = $result;
        return collect($customer['form_respon'])->flatten()->transform(function ($item, $key) use ($vote,$order){
                $y = explode('/',$item);
                if(count($y) == 2)
                {
                    $vote_arr = [null,null,null,null,null];
                    $vote_arr[((integer)$vote[$y[0]]-1)] = $vote[$y[0]];
                    return [
                        'customer_id'=>$y[0],
                        'name'=>$y[1], 
                        'vote'=>$vote_arr, 
                        'order'=>$order[$y[0]]
                    ];
                }else{
                    return false;
                }
        })->filter()->values()->toArray(); 
    }
    protected function barang_jual($depo,$kode_barang)
    {
        $coll = BarangPenjualan::select(['id_toko','jumlah_kirim'])
        ->where('id_depo',$depo)
        ->where('kode_barang','like', Str::substr($kode_barang, 0, 6).'%')
        ->get();
        return $coll->groupBy('id_toko')->map(function($item, $key){
            return $item->sum('jumlah_kirim');
        })
        ->toArray();
    }
    protected function image_survey($image)
    {
        $imgs = [];
        $optimizerChain = OptimizerChainFactory::create();
        foreach ($image as $v) {
            $img = Image::load(public_path($v->image_link));
            $l = $img->getWidth();
            $t = $img->getHeight();
            if($l > 3000)
            {
                $lb = ($l - ($l*30/100));
                $tg = ($t - ($t*30/100));
                $img->width($lb)->height($tg)->save();
                    // Image::load(public_path($v->image_link))
                    //     ->optimize([Jpegoptim::class => [
                    //         '--all-progressive',
                    //     ]])
                    //     ->save();
            }
            if($l < $t )
            {
                $img->orientation(90)->save();
            }
            array_push($imgs,$v->image_link);

            // $optimizerChain
            //     ->setTimeout(10)
            //     ->optimize(public_path($v->image_link));
        }
        return $imgs;
    }
    protected function data_pdf_print_survey($data)
    {
        $collect = new \stdClass;
        $collect->no_survey = $data->survey->survey_no;
        $collect->depo = $data->survey->depo_detail->nama_depo;
        $collect->merek = $data->survey->brand;
        $collect->nama_motif = $data->survey->motif;
        $collect->ukuran = $data->survey->ukuran->title;
        $collect->harga_depo = $data->survey->harga;
        $collect->tgl_produksi = $data->survey->tgl_produksi;
        $collect->warna = array_replace(['#','#','#','#'],explode(',',$data->survey->warna));
        $collect->vote_order = $this->get_vote(
            $data->survey->depo_survey->vote,
            $data->survey->depo_survey->order,
            $data->survey->depo_survey->customer
        );
        $collect->jual = $this->barang_jual(
            $data->survey->depo_detail->id_depo,
            $data->survey->kode_barang
        );
        $collect->tot_jual = array_sum($collect->jual);
        $collect->tot_vote = $this->tot_vote;
        $collect->tot_order = $this->tot_order;
        $collect->status = $data->survey->status_id;
        $collect->image = $this->image_survey($data->survey->image);
        $collect->durasi = $data->survey->created_at->format('d/m/Y').' s/d '.$data->survey->depo_survey->s_end->format('d/m/Y');
        return $collect;
    }
    public function proses_by_year($stat_code)
    {
        if($this->survey_by_year)
        {
            return $this->survey_by_year->where('status_id',$stat_code)->groupBy(function($d){
                return $d->tgl->format('m');
            })->map(function ($item, $key) {
                return $item->count();
            });

            
        }else{
            throw new \Exception("Harus get by year dulu: \$this->get_by_year(string \$year)", 0);
        }
    }
    public function group_status_survey()
    {
        $status1 = $this->proses_by_year(1);
        $status2 = $this->proses_by_year(2);
        $status3 = $this->proses_by_year(3);
        $status4 = $this->proses_by_year(4);
        $status5 = $this->proses_by_year(5);
        $status6 = $this->proses_by_year(6);
        $status7 = $this->proses_by_year(7);
        $status8 = $this->proses_by_year(8);
        $status9 = $this->proses_by_year(9);
        return compact('status1','status2','status3','status4','status5','status6','status7','status8','status9');
    }
    public function get_by_year($year)
    {
        $this->survey_by_year = NotifSurvey::with(['depo_detail','ukuran','status'])->whereYear('tgl',$year)->get();
        return $this;
    }
    public function get_by_month($data)
    {   
        return $data->groupBy([
            function($item){
                return $item['tgl']->format('F');
            },
            function($depo){
                return $depo->depo_detail->nama_depo;
            },
            'motif'
        ]);
    }
    public function get_all_status()
    {
        return StatusSurvey::all();
    }
    public function summary()
    {
        $this->survey = NotifSurvey::with(['depo_survey','depo_detail','status','ukuran'])->select('mr_notif_survey.*');
        return Datatables::of($this->survey)
        ->make(true);
    }
    
    public function get_all_depo()
    {
        return Depo::all();
    }
    public function get_reg_depo()
    {
        return Regional::with('depo_regional')->orderBy('urut_reg')->get();
    }
    public function get_brand()
    {
        return Brand::all();
    }
    public function get_size()
    {
        return Ukuran::all();
    }

    public function notif_survey_table($query)
    {
        return DataTables::of($query)
                ->addColumn('status',function($sr){
                    if(!isset($sr->status->id))
                    {
                        $status =  $sr->status;
                    }else{
                        $status =  $sr->status->status;
                    }
                    switch ($status) {
                        case 'Depo Melakukan Survey':
                            return '<button class="btn btn-danger btn-xs">'.$status.'</button>';
                            break;
                        case 'Proses Survey':
                            return '<button class="btn btn-success btn-xs">'.$status.'</button>';
                            break;
                        case 'Kadep Approval':
                                return '<button class="btn btn-warning btn-xs">'.$status.'</button>';
                                break;
                        case 'RM Approval':
                            return '<button class="btn btn-warning btn-xs">'.$status.'</button>';
                            break;
                        case 'Approve':
                            return '<button class="btn btn-primary btn-xs">'.$status.'</button>';
                            break;
                        case 'Finish Survey':
                            return '<button class="btn btn-primary btn-outline btn-xs">'.$status.'</button>';
                            break;
                        case 'Waiting Approval':
                                return '<button class="btn btn-info btn-xs">'.$status.'</button>';
                                break;
                        case 'Reject Survey':
                                return '<button class="btn btn-danger btn-xs">'.$status.'</button>';
                                break;
                        default:
                        return '<button class="btn btn-success btn-xs">'.$status.'</button>';
                            break;
                    }
                    
                })
                ->addColumn('action', function ($sr) {
                    $x = $sr;
                    $action = '';
                    if(!isset($sr->status->id))
                    {
                        return $sr;
                    }
                    if(in_array($sr->status->id,[1,2]))
                    {
                        $action = "<li class=\"\"><a class=\"dropdown-item\" href=\"#\">#</a></li>";
                    }
                    if(in_array($sr->status->id,[3]))
                    {
                        $action = "<li class=\"\"><a class=\"dropdown-item \" href=\"/report/survey/review_depo/{$sr->id}\">Review</a></li>";
                    }
                    if(in_array($sr->status->id,[8,6,9]))
                    {
                        $action = "<li class=\"\"><a class=\"dropdown-item\" href=\"/report/survey/print_survey/{$sr->id}\" target=\"_blank\">Print Survey</a></li>";
                        $action .= "<li class=\"\"><a class=\"dropdown-item\" href=\"/report/survey/batal_depo_survey/{$sr->id}\" >Void Survey</a></li>";

                    }
                    if(in_array($sr->status->id,[6,7,8]))
                    {
                        $action = "<li class=\"\"><a class=\"dropdown-item\" href=\"/report/survey/print_survey/{$sr->id}\" target=\"_blank\">Print Survey</a></li>";
                        $action = "<li class=\"\"><a class=\"dropdown-item\" href=\"/report/survey/batal_depo_survey/{$sr->id}\" >Void Survey</a></li>";
                        $action .= "<li class=\"\"><a class=\"dropdown-item\" href=\"/report/survey/finish_survey/{$sr->id}\">Finish Survey !</a></li>";
                    }

                    if(auth()->user()->name == 'wisnu' && in_array($sr->status->id,[8,6,9]))
                    {
                        $action = "<li class=\"\"><a class=\"dropdown-item\" href=\"/report/survey/print_survey/{$sr->id}\" target=\"_blank\">Print Survey</a></li>";
                        $action .= "<li class=\"\"><a class=\"dropdown-item\" href=\"/report/survey/batal_depo_survey/{$sr->id}\" >Void Survey</a></li>";
                        $action .= "<li class=\"\"><a class=\"dropdown-item \" href=\"/report/survey/delete_survey/{$sr->id}\">Delete</a></li>";
                    }

                    return "<div class=\"btn-group\">
                                <button data-toggle=\"dropdown\" class=\"btn btn-primary btn-xs dropdown-toggle\" aria-expanded=\"false\">
                                        Action 
                                </button>
                                <ul class=\"dropdown-menu\" x-placement=\"bottom-start\">
                                    {$action}
                                </ul>
                            </div>";
                })
                ->rawColumns(['action','status'])
                ->make();  
    }
    public function depo_survey_table()
    {
        $data = NotifSurvey::with(['depo_detail','status','ukuran','depo_survey']);
                
                if(!in_array('super-admin',$this->role))
                {
                    $data->where('depo',Auth::user()->depo_id);
                }
                $data->whereIn('status_id',[1,5,4,2,8,7,6,9])->orderBy('updated_at','desc');
        return DataTables::of($data)
                ->addColumn('action', function ($sr) {
                    if(in_array($sr->status->id,[9])){
                        return '<span class="text-success font-bold fa-2x fa fa-check no-paddings no-margins"></span>';
                    }
                    if(in_array($sr->status->id,[1,2,5]))
                    {
                    return "<div class=\"btn-group\">
                                <button data-toggle=\"dropdown\" class=\"btn btn-primary btn-xs dropdown-toggle\" aria-expanded=\"false\">
                                        Action 
                                </button>
                                <ul class=\"dropdown-menu\" x-placement=\"bottom-start\">
                                    <li><a class=\"dropdown-item\" href=\"/report/survey/proses_depo_survey/{$sr->id}\">Proses</a></li>
                                    <li><a class=\"dropdown-item\" href=\"/report/survey/depo_survey?print_form={$sr->id}\" target=\"_blank\">Print Form Survey</a></li>
                                    <li><a class=\"dropdown-item\" href=\"/report/survey/post_depo_survey/{$sr->id}\">Kirim Survey</a></li>
                                    <li><a class=\"dropdown-item\" href=\"/report/survey/batal_depo_survey/{$sr->id}\">Batal Survey</a></li>
                                </ul>
                            </div>";
                    }else{
                        return '<span class="text-warning font-bold fa-2x fa fa-check no-paddings no-margins"></span>';
                    }
                    
                    
                })
                ->addColumn('status',function($sr){
                    return "<a class=\" font-bold \">{$sr->status->status}</a>";
                })
                ->rawColumns(['action','status'])
                ->make(); 
    }
    public function status()
    {
        $this->survey = NotifSurvey::with(['depo_survey','depo_detail','status','ukuran'])
        ->where('status_id',3)
        ->select('mr_notif_survey.*');
        return Datatables::of($this->survey)
        ->addColumn('survey', function ($q) {
            $v = json_decode($q->depo_survey->vote, true);
            $v = array_values($v);
            foreach(array_count_values($v) as $k => $v){
                $jml[$k] = $v;
              }
            
            dd($jml);
        })
        ->make(true);
    }
    // save image form survey dari kirim survey depo
    public function optimize_upload_image($img_url)
    {
        $img = Image::load(public_path($img_url));
        $img->optimize([Jpegoptim::class => ['--all-progressive']]);
            $l = $img->getWidth();
            $t = $img->getHeight();
            if($l > 3000)
            {
                $lb = ($l - ($l*30/100));
                $tg = ($t - ($t*30/100));
                $img->width($lb)->height($tg);
            }
            if($l < $t )
            {
                $img->orientation(90);
            }
        $img->save();
    }
    public function pdf_form_survey($data)
    {
        $m = $data;
        $pdf = PDF::loadView('backend._report.pdf_form_survey',compact('m'))
                    ->setPaper('a4')
                    ->setOrientation('landscape')
                    ->setOption('margin-bottom', 0);
        return $pdf->inline('form_survey.pdf');
    }
    public function save_proses_survey($r)
    {
        $data = NotifSurvey::with(['depo_detail','status','ukuran','depo_survey'])
                ->where('survey_no',$r->survey_no)->first();
        $data->harga = $r->form_respon[0]['harga_depo'];
        $data->save();

        $depo_survey = DepoSurvey::firstOrNew(array('notif_id' => $data->id));
        // $depo_survey = new DepoSurvey;
        $depo_survey->s_start = Carbon::now();
        $depo_survey->status_id = 2;
        $depo_survey->customer = $r->toArray();
        $depo_survey->notif_id = $data->id;
        $depo_survey->save();
        $data->status_id = 2;
        $data->save();
    }
}