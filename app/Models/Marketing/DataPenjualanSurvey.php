<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class DataPenjualanSurvey extends Model
{
    // protected $connection = 'acura_conn';
    protected $table = 'mr_penjualan_survey';
    // protected $primaryKey = 'id_brt';
    // protected $casts = [
    //     'customer' => 'array',
    //     's_start'=>'datetime:d-m-Y H:i:s',
    //     's_end'=>'datetime:d-m-Y H:i:s'
    // ];
    protected $fillable = ['sj','id_cus','brand','kw','kd_barang','ukuran','motif','item','jual','retur','net','depo','region','customer'];
    // public function depo()
    // {
    //     return $this->hasOne('App\Models\Report\Depo', 'id_depo', 'depo');
    // }
    // public function status()
    // {
    //     return $this->hasOne('App\Models\Marketing\StatusSurvey', 'id', 'status_id');
    // }
}
