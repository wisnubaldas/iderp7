<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class MenuModule extends Model
{
    protected $hidden = ['created_at','updated_at'];
    public function menus()
    {
        return $this->hasMany('App\Models\Admin\MenuModel', 'menu_modules_id');
    }
}
