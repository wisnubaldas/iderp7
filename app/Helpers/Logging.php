<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
class Logging {
    public $table;
    public function __construct()
    {
        $this->table = DB::table('logging');
    }
    public function create_log(array $data)
    {
        $this->table->insert($data);
    }
    public function countByType(int $type)
    {
        return $this->table->where('tipe_log',$type)->count();
    }
    
}