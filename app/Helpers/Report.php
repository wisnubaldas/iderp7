<?php
namespace App\Helpers;
// Class alias SurveyHelper
use Illuminate\Support\Arr;
class Report 
{
    public $crot;
    public $report_data;
    public $ss = 0; 
    public $s = 0; 
    public $cs = 0; 
    public $ts = 0; 
    public $sts = 0;
    public $pss = 0; public $ps = 0; public $pcs = 0; public $pts = 0; public $psts = 0; public $s_all = 0;
    public $brand; public $motif; public $ukuran; public $warna;
    public $tot_toko_survey = 20;
    public $grand_total_region = [];

    public $sum_by_vote_region = [
        'ss'=>0,'s'=>0,'cs'=>0,'ts'=>0,'sts'=>0
    ];
    public $tot_by_vote_region = [
        'ss'=>0,'s'=>0,'cs'=>0,'ts'=>0,'sts'=>0
    ];
    public $tot_all_sum = []; // total sumary all depo 

    public $pgt_ss;
    public $pgt_s;
    public $pgt_cs;
    public $pgt_ts;
    public $pgt_sts;
    public $gt_order;
    
    public function __construct($dataReport) {
        $this->report_data = $dataReport;
    }
    // public function header_warna()
    // {
    //     $head = [];
    //     foreach($this->report_data['summary_region'] as $v)
    //     {
    //         $zz = [];
    //         foreach($v['survey']['order'] as $z)
    //         {
    //             array_push($zz,key($z));
    //         }
    //        array_push($head, $zz);
    //     }
    //     return array_unique($head);
    // }
    public function ble_ketek()
    {
        $br = []; $mf = []; $uk = []; $wr = [];
        foreach($this->report_data['head_tpl'] as $v)
        {
            array_push($br,$v['brand']);
            array_push($mf,$v['motif']);
            array_push($uk,$v['ukuran']);
            array_push($wr,$v['warna']);
        }

        switch(count($wr)){
            case 1: 
                $wr = array_merge($wr,['£','££','£££']);
            break;
            case 2:
                $wr = array_merge($wr,['££','£££']);
            break;
            case 3:
                $wr = array_merge($wr,['£££']);
            break;
                $wr = $wr;
            default;
        }
        
        $this->brand = implode(',',$br);
        $this->motif = implode(',',$mf);
        $this->ukuran = implode(',',$uk);
        $this->warna = $wr;
        return $this;
    }
    // public function parsinge()
    // {
    //     $gion = [];
    //     $motif = null;
    //     foreach($this->report_data['summary_region'] as $x)
    //     {
    //         $z = [];
    //         if (isset($x['survey']['vote'])) {
    //             $z['voting'] = array_count_values($x['survey']['vote']);
    //         }
    //         if (isset($x['survey']['order'])) {
    //             foreach ($x['survey']['order'] as $v) {
    //                 $z['order'] = $v;
    //                 $motif = array_keys($v);
    //             }
    //         }
    //         $z['depo'] = $x['nama_depo'];
    //         $z['reg'] = $x['nama_reg'];
    //         $z['id_reg'] = $x['id_reg'];
    //         $gion[$x['urut_reg']][] = $z;
    //     }
    //     ksort($gion);
    //     $this->regional = collect($gion)->groupBy('id_reg')->collapse();
    //     return $this;
    // }
    public function vote($vote)
    {
       
        isset($vote['1'])?
            $this->ss = $vote['1']:$this->ss = 0;
        isset($vote['2'])?
            ($this->s = $vote['2']):$this->s = 0;
        isset($vote['3'])?
            ($this->cs = $vote['3']):$this->cs = 0;
        isset($vote['4'])?
            ($this->ts = $vote['4']):$this->ts = 0;
        isset($vote['5'])?
            ($this->sts = $vote['5']):$this->sts = 0;
        $this->pss = ($this->ss/(20/100));
        $this->ps = ($this->s/(20/100)); 
        $this->pcs = ($this->cs/(20/100)); 
        $this->pts = ($this->ts/(20/100)); 
        $this->psts = ($this->sts/(20/100));
        
        $this->s_all = ($this->sts+$this->ts+$this->cs+$this->s+$this->ss);
        $this->p_all = ($this->psts+$this->pts+$this->pcs+$this->ps+$this->pss);
        return $this;
    }
    public $nilai_motif = 0;
    public function sub_total_x($item,$bagi)
    {
        $z = round($item,2)*$bagi/100;
        $this->nilai_motif += $z;
        return number_format($z,2,',','.');
    }
    public $tot_sum_vote_depo = ['ss'=>0,'s'=>0,'cs'=>0,'ts'=>0,'sts'=>0];
    public function sum_vote_depo($vote,$label)
    {
        switch($label){
            case 'ss':
                (array_key_exists('1',$vote))?$this->tot_sum_vote_depo['ss'] += $vote['1']:0;
                return (array_key_exists('1',$vote))?$vote['1']:0;
            break;
            case 's':
                (array_key_exists('2',$vote))?$this->tot_sum_vote_depo['s'] += $vote['2']:0;
                return (array_key_exists('2',$vote))?$vote['2']:0;
            break;
            case 'cs':
                (array_key_exists('3',$vote))?$this->tot_sum_vote_depo['cs'] += $vote['3']:0;
                return (array_key_exists('3',$vote))?$vote['3']:0;
            break;
            case 'ts':
                (array_key_exists('4',$vote))?$this->tot_sum_vote_depo['ts'] += $vote['4']:0;
                return (array_key_exists('4',$vote))?$vote['4']:0;
            break;
            case 'sts':
                (array_key_exists('5',$vote))?$this->tot_sum_vote_depo['sts'] += $vote['5']:0;
                return (array_key_exists('5',$vote))?$vote['5']:0;
            break;
        }
    }
    public function sub_tot_vote_depo()
    {
        return (object)$this->tot_sum_vote_depo;
    }
    public $sub_tot_persen = 0;
    public function tot_persen_vote($jml)
    {
        $this->sub_tot_persen += $jml;
        return $jml;
    }
    public function tot_per_vote($flag,$jmlDepo)
    {
        switch ($flag) {
            case 'pss':
                $this->sum_by_vote_region['ss'] += ($this->pss/$jmlDepo);
                $this->tot_by_vote_region['ss'] += ($this->pss/$jmlDepo);
                
                break;
            case 'ps':
                $this->sum_by_vote_region['s'] += ($this->ps/$jmlDepo);
                $this->tot_by_vote_region['s'] += ($this->ps/$jmlDepo);
                break;
            case 'pcs':
                $this->sum_by_vote_region['cs'] += ($this->pcs/$jmlDepo);
                $this->tot_by_vote_region['cs'] += ($this->pcs/$jmlDepo);
                break;
            case 'pts':
                # code...
                $this->sum_by_vote_region['ts'] += ($this->pts/$jmlDepo);
                $this->tot_by_vote_region['ts'] += ($this->pts/$jmlDepo);
                break;
            case 'psts':
                # code...
                $this->sum_by_vote_region['sts'] += ($this->psts/$jmlDepo);
                $this->tot_by_vote_region['sts'] += ($this->psts/$jmlDepo);
                break;
            default:
                # code...
                break;
        }
        
        return $this;
    }
    public function reset_tot_vote()
    {
        $this->sum_by_vote_region = [
            'ss'=>0,'s'=>0,'cs'=>0,'ts'=>0,'sts'=>0
        ];
    }
    protected function cek_status_vote($ss)
    {
        $result = [];
        isset($ss['1'])?
            $result['1'] = $ss['1']:$result['1'] = 0;
        isset($ss['2'])?
            $result['2'] = $ss['2']:$result['2'] = 0;
        isset($ss['3'])?
            $result['3'] = $ss['3']:$result['3'] = 0;
        isset($ss['4'])?
            $result['4'] = $ss['4']:$result['4'] = 0;
        isset($ss['5'])?
            $result['5'] = $ss['5']:$result['5'] = 0;
        return $result;
    }
    protected function generate_vote($data)
    {
        $all_vote = [];
        foreach($data as $v)
        {
            array_push($all_vote,$this->cek_status_vote($v['voting']));
        }
        return $all_vote;
    }
    public function sub_tot($vote)
    {
        $vote_all = $this->generate_vote($vote);
        $vote_all = collect($vote_all);
        $this->ss = $vote_all->sum('1');
        $this->s = $vote_all->sum('2');
        $this->cs = $vote_all->sum('3');
        $this->ts = $vote_all->sum('4');
        $this->sts = $vote_all->sum('5');

        // $this->pss = ($this->ss/count($vote));
        // $this->ps = ($this->s/count($vote)); 
        // $this->pcs = ($this->cs/count($vote)); 
        // $this->pts = ($this->ts/count($vote)); 
        // $this->psts = ($this->sts/count($vote));
        // $this->p_all = ($this->psts+$this->pts+$this->pcs+$this->ps+$this->pss/count($vote));
        $this->s_all = ($this->sts+$this->ts+$this->cs+$this->s+$this->ss);

        $this->ns_ss = ($this->pss*5);
        $this->ns_s = ($this->ps*4);
        $this->ns_cs = ($this->pcs*3);
        $this->ns_ts = ($this->pts*2);
        $this->ns_sts = ($this->psts*1);
        $this->ns_s_all = ($this->ns_sts+$this->ns_ts+$this->ns_cs+$this->ns_s+$this->ns_ss);
        
        // total persentase
        $this->pgt_ss += $this->pss;
        $this->pgt_s += $this->ps;
        $this->pgt_cs += $this->pcs;
        $this->pgt_ts += $this->pts;
        $this->pgt_sts += $this->psts;
        return $this;
    }
    public function brand_order($order)
    {
        $ord = [];
        foreach($this->ble_ketek()->warna as $b)
        {
            if(isset($order[$b]))
            {
                array_push($ord,intval($order[$b]));
            }else{
                array_push($ord,0);
            }
        }
        $this->orderan = $ord;
        return $this;
    }
    public function tot_order($warna,$orderan)
    {
        $result = 0;
        foreach($orderan as $mek)
        {
            if(isset($mek['order'][$warna]))
            {
                $result += $mek['order'][$warna];
            }
        }
        $this->sub_tot = $result;
        $this->gt_order += $result;
        return $this;
    }
    public function depo_order()
    {
        $this->crot = $this->report_data['summary_toko'];
        $this->crot->depo_order = $this->get_order_depo($this->crot->order);
        $x = collect($this->crot->depo_order);
        $this->crot->total = $x->sum();
        $this->crot->tot_survey = $x->count()*20;
        return $this;
    }
    protected function get_order_depo($order)
    {
        return collect($order)->map(function($x)
        {
            return collect($x)->map(function($z){
                return collect($z)->sum();
            })->sum();
        });
        
    }
    public $total_order = [];
    public function region($warna,$order)
    {
        $this->order = collect($order)->pluck($warna)->sum();
        array_push($this->total_order,[$warna => $this->order]);
        return $this;
    }
    public function total_order()
    {
        $o = collect($this->total_order)->groupBy(function ($item, $key) {
            return array_keys($item);
        });
        $o->transform(function($a,$b){
            return $a->flatten()->sum();
        });
        $this->grand_total_region[] = $o;
       return $o;
    }
    public function grand_total_region()
    {
        $z = [];
        foreach($this->grand_total_region as $x => $y)
        {
            foreach($y as $a => $b)
            {
                if(!array_key_exists($a,$z))
                {
                   $z[$a] = $b;
                }else{
                    $z[$a] += $b;
                }
            }
        }
        return $z;
    }
    public $t_order = [];
    public $gr_t_order = 0;
    public function t_order($index,$t_order)
    {
        if(!isset($this->t_order[$index]))
        {
            $this->t_order[$index] = 0;
        }
        $this->t_order[$index] += $t_order;
        return $this;
    }

    public $nil_motif;
    public function tot_nilai_motif($nil,$bagi)
    {
        $this->nil = round(($nil*$bagi/100),2);
        $this->nil_motif += $this->nil;
        return $this;        
    }
    public function nil_motif()
    {
        $this->nil = $this->nil_motif;
        $this->nil_motif = null;
        return $this;
    }
   public function grandTotal($sub)
   {
       return $sub;
   }

   public $css = [];
   public function chart_sum_subtot($region,$vote)
   {
       $this->css[$region][] = $vote;
   }
   public function generate_chart_sum_subtot()
   {
       $region = collect($this->css);
       $this->region = $region->keys();
       $this->data = $region->values();
       return $this;
   }
   public $csn = [];
   public function chart_sum_nilai($region,$vote)
   {
       $this->csn[$region][] = $vote;
   }
   public function generate_chart_sum_nil()
   {
       $region = collect($this->csn);
       $this->region = $region->keys();
       $this->datax = $region->values();
       return $this;
   }
   public $cdkb;
   public function chart_data_komentar(array $arr)
   {
       $this->cdkb = array_values($arr);
   }
   public function tofloat($num) {
    $dotPos = strrpos($num, '.');
    $commaPos = strrpos($num, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
        ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
  
    if (!$sep) {
        return floatval(preg_replace("/[^0-9]/", "", $num));
    }

    return floatval(
        preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
        preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
    );
}
}
