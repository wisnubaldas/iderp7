<?php
namespace App\Helpers;
// Class alias SurveyHelper
use Illuminate\Support\Arr;
class Survey 
{
    public static function status_vote($status = 0)
    {
        switch ($status) {
            case 1:
                return 'SANGAT SUKA';
                break;
            case 2:
                return 'SUKA';
                break;
            case 3:
                return 'CUKUP SUKA';
                break;
            case 4:
                return 'TIDAK SUKA';
                break;
            case 5:
                return 'SANGAT TIDAK SUKA';
                break;
            default:
                return 'TIDAK VOTE';
                break;
        }
    }
    public static function toko(string $nama)
    {
       $nama = explode('/',$nama);
       $s = new self;
       $s->id = $nama[0];
       $s->nama = $nama[1];
       return $s;
    }
    public static function warna(string $warna)
    {
        return explode(',',$warna);
    }
    public static function vote_to_icon($vote,$posisi = 0)
    {
        if($vote == $posisi)
        {
            return '<i class="fa fa-check" aria-hidden="true"></i>';
        }else{
            return '';
        }

    }
    public static function send_wa(string $messages)
    {
        // Send Message
            $my_apikey = "Q37SVWEU5YJ8ANTWS9CE";
            $destination = "+6282213957498";
            $message = $messages;
            $api_url = "http://panel.rapiwha.com/send_message.php";
            $api_url .= "?apikey=". urlencode ($my_apikey);
            $api_url .= "&number=". urlencode ($destination);
            $api_url .= "&text=". urlencode ($message);
            $my_result_object = json_decode(file_get_contents($api_url, false));
            echo "<br>Result: ". $my_result_object->success;
            echo "<br>Description: ". $my_result_object->description;
            echo "<br>Code: ". $my_result_object->result_code; 
    }
    public static function cek_null_array($arr)
    {
        return empty(array_filter($arr, function ($a) { return $a !== null;}));
    }
    public static function tofloat($num) {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
      
        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        }
    
        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );
    }
    
}
