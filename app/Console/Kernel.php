<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\RankingJob;
use App\Jobs\PenjualanHariIni;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        // cleaning log
        // $schedule->command('activitylog:clean')->daily();
        // $schedule->job(new Heartbeat)->everyFiveMinutes();
        
        $r =  [
            "brand" => "G",
            "start" => "01-2019",
            "end" => "01-2019",
            "rank" => "50",
            "user_name" => "wisnu",
            "user_email" => "wisnubaldas@gmail.com",
            "user_id" => 1,
            "parseRankingFile" => "januari_g"
        ];
        $filePath = storage_path('logs/cron_schedule.log');
        // $schedule->job(new RankingJob($r))->monthlyOn(1, '01:00');
        $schedule->job(new PenjualanHariIni)->timezone('Asia/Jakarta')->dailyAt('01:00')->appendOutputTo($filePath);
        $schedule->job(new RankingJob($r))->timezone('Asia/Jakarta')->daily()->runInBackground(); // setiap hari
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
