<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Marketing\DataPenjualanSurvey;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class DataPenjualanImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new DataPenjualanSurvey([
            'sj' => $row['sj'],
            'id_cus' => $row['id_customer'],
            'customer' => $row['nama_customer'],
            'brand' => $row['brand'],
            'kw' => $row['kw'],
            'kd_barang' => $row['barang'],
            'ukuran' => $row['ukuran'],
            'motif' => $row['motif'],
            'item' => $row['item'],
            'jual' => $row['jual'],
            'retur' => $row['retur'],
            'net' => $row['net'],
            'depo' => $row['depo'],
            'region' => $row['regional'],
        ]);
        
    }
    public function headingRow(): int
    {
        return 4;
    }
}
