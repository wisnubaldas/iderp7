<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;
use Maatwebsite\Excel\Concerns\Importable;
class CustomersExcel implements WithMultipleSheets
{
    // /**
    // * @param Collection $collection
    // */
    // public function collection(Collection $collection)
    // {
    //     foreach ($collection as $row) 
    //     {
    //         dump($row);
    //     }
    // }
    use Importable;
    public function sheets(): array
    {
        return [
            'ATENA' => new Atena(),
            'MUSTIKA' => new Mustika(),
            'PICASSO'=> new Picasso(),
            'HARMONY'=> new Harmoni(),
        ];
    }
}
