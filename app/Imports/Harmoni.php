<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class Harmoni implements WithHeadingRow, ToCollection
{
    public function headingRow(): int
    {
        return 4;
    }

    public function collection(Collection $collection)
    {
       foreach ($collection as $key => $value) {
               return [
                   'id_depo'=>$value['ID DEPO'],
                   'id_toko'=>$value['ID CUSTOMER'],
                   'nama_toko'=>$value['NAMA TOKO'],
               ];
           
       }
    }
}
