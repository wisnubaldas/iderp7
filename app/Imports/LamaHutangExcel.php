<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Report\Lshjadlh;
class LamaHutangExcel implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        unset($collection[0]);
        foreach ($collection as $row) 
        {
            unset($row[0]);
            $kd_barang = $row[1];
            $motif = $row[2];
            $harga_pabrik = $row[3];
            $harga_normal = $row[4];
            $discount = ($harga_normal-($harga_normal*3/100));
            $bunga_1 = ($harga_normal+($harga_normal*1/100));
            $bunga_2 = ($harga_normal+($harga_normal*2/100));
            $bunga_3 = ($harga_normal+($harga_normal*3/100));
            $bunga_4 = ($harga_normal+($harga_normal*4/100));
            $bunga_5 = ($harga_normal+($harga_normal*5/100));
            $keterangan = $row[5];
            Lshjadlh::insert(compact('kd_barang','motif','harga_pabrik','harga_normal','discount','bunga_1','bunga_2','bunga_3','bunga_4','bunga_5','keterangan'));
            // User::create([
            //     'name' => $row[0],
            // ]);
        }
    }
}
