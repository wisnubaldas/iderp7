<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSurvey extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->redirect = url()->previous().'#survey-form';
        return [
            'tgl' => 'required|max:255',
            'tgl_produksi' => 'required|max:255',
            'motif' => 'required',
            'warna' => 'required',
            'brand' => 'required',
            'depo' => 'required',
            'ukuran' => 'required',
            'customer'=>'required',
            'harga'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'tgl.required' => 'Tanggal harus di isi',
            'motif.required' => 'Motif keramik harus di isi',
            'warna.required' => 'Warna keramik harus di isi maksimal 4 warna',
            'brand.required' => 'Brand keramik harus di isi',
            'depo.required' => 'Depo keramik harus di isi',
            'ukuran.required' => 'Ukuran keramik harus di isi',
            'customer.required'=>'Cusomer harus di isi',
            'harga.required' => 'Harga harus di set',
            'tgl_produksi.required'=>'Tanggal harus di isi',

        ];
    }
}
