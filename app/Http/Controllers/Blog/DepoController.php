<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\UploadFile;
use App\Models\Blog\Distributor;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
class DepoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return \view('backend.blog.index');
    }
    public function store(Request $r)
    {
        $data = json_decode($r->data);
        $save = new Distributor;
        $save->perusahaan = $data->perusahaan;
        $save->map = $data->map;
        $save->marker = $data->marker;
        $save->lokasi = $data->lokasi;
        $save->alamat = $data->alamat;
        $save->phone_1 = $data->phone_1;
        $save->phone_2 = $data->phone_2;
        $save->fax = $data->fax;
        $save->mail = $data->mail;
        $save->lat = $data->lat;
        $save->long = $data->long;
        $save->ig = $data->ig;
        $save->ig_link = $data->ig_link;
        $save->fb = $data->fb;
        $save->fb_link = $data->fb_link;
        if ($r->hasFile('logo')) {
            $file = UploadFile::upload($r->file('logo'),'public/logo');
            $save->logo = $file['file_path'];
        }
        $save->save();
        return $save;
    }
    public function grid(Request $request)
    {
        $length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
        
        $query = Distributor::eloquentQuery($sortBy, $orderBy, $searchValue);

        $data = $query->paginate($length);
        
        return new DataTableCollectionResource($data);
    }
    public function delete(Request $r)
    {
        return Distributor::find($r->id)->forceDelete();
    }
}
