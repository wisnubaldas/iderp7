<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog\Brand;
use App\Models\Blog\Produk;
use App\Models\Blog\Barang;
use App\Models\Blog\Type;
use App\Models\Blog\Ukuran;
use App\Models\Blog\ImageBarang;

use App\Helpers\UploadFile;
class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::select(['id','title','b_brand_id'])->get();
        $brand = Brand::select(['id','nama'])->get();
        $type = Type::select(['id','code','title'])->get();
        $ukuran = Ukuran::select(['id','title','height','width'])->get();
        $connate = \compact('produk','brand','type','ukuran');
        return \view('backend.blog.barang',\compact('connate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $pr = new Barang;
            $pr->title = $request->title;
            $pr->desc = $request->desc;
            $pr->b_brand_id = $request->b_brand_id;
            $pr->b_type_id = $request->b_type_id;
            $pr->b_produk_id = $request->b_produk_id;
            $pr->b_ukuran_id = $request->b_ukuran_id;
            $pr->save();
            $img = [];
            foreach ($request->all() as $i => $v) {
                if(strstr($i, 'img' ))
                {
                    array_push($img,$v);
                }
            }
            
            $file = UploadFile::multiple_image($img,'public\blog\img\barang',['width'=>$request->width,'height'=>$request->height]);
            foreach ($file as $v) {
                $saveImg = new ImageBarang;
                $saveImg->nama = $v['nama'];
                $saveImg->url_s = $v['url_s'];
                $saveImg->url_m = $v['url_m'];
                $saveImg->url_l = $v['url_l'];
                $saveImg->b_barang_id = $pr->id;
                $saveImg->save();
            }
           return response()->json([
                'status' => $pr->id
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
