<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\UploadFile;
use Illuminate\Support\Str;
use App\Models\Blog\Slide;
use Illuminate\Support\Facades\Storage;
use Image;
class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slide = Slide::all();
        return \view('backend.blog.slide',compact('slide'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'slide_text_id' => 'required|max:255',
            'slide_desc_id' => 'required|max:255',
            'slide_text_en' => 'required|max:255',
            'slide_desc_en' => 'required|max:255',
        ]);

        if($request->file())
        {
            $file = $request->file('slide_img');
            $image = Image::make($file->getRealPath()); // not sure about needing "getRealPath()" but hey, it works
            $imageName = UploadFile::make_name().'.'.explode('/',$image->mime())[1];
            $image->resize(940, 420)
                    ->save(base_path('public/img/slider/'.$imageName));
            $slide = new Slide;
            $slide->slide_text_id = $request->slide_text_id;
            $slide->slide_desc_id = strip_tags($request->slide_desc_id, '<br>');
            $slide->slide_text_en = $request->slide_text_en;
            $slide->slide_desc_en = $request->slide_desc_en;
            $slide->slide_link = $request->slide_link;
            $slide->slide_img = $imageName;
            $slide->save();
        }
        // copy file hasil upload ke cjfi
        $src = '/var/www/html/iderp7/public/img/slider/'.$imageName;
        $dst = '/var/www/html/cjfi_r1/images/home/slide/'.$imageName;
        copy($src, $dst);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $slide = Slide::find($id);
       $slide->delete();
       return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slide::find($id);
        $slide->delete();
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
