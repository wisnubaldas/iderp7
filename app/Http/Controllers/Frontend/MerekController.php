<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\Route;   
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\UploadFile;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use Illuminate\Support\Facades\URL;
use App\Models\Blog\Brand;
use App\Models\Blog\Produk;
use App\Models\Blog\Barang;
class MerekController extends Controller
{
    protected $brand;
    public function __construct()
    {
        $this->brand = Brand::all();
    }
    public function index($br = null,$produk = null)
    {
        if($br && $produk)
        {
            $barang = Barang::where(['b_brand_id'=>$br,'b_produk_id'=>$produk])->get();
            return \view('frontend.blog.koleksi_barang',\compact('barang'));
        }
        if($br)
        {   
            $produk = Produk::where('b_brand_id',$br)->get();
            $barang = Barang::where('b_brand_id',$br)->paginate(1);
            return \view('frontend.blog.koleksi_produk',\compact('produk','barang'));
        }
        $brand = $this->brand;
        return \view('frontend.blog.koleksi_merek',\compact('brand'));
    }
    public function merek(Request $request, $merek)
    {
        $routeCollection = Route::getRoutes()->get();
        foreach ($routeCollection as $value) {
            if(isset($value->action['as']) && $value->action['as'] == $merek)
            {
                return redirect()->route($merek);
            }
        }
        return abort(403,'Nor Route');
        // $x = \Request::route()->getName($merek);
        // dd($x);
        
        // if ($request->route()->named($merek)) {
        //     return redirect()->route($merek);
        // }
    
        
    }

}
