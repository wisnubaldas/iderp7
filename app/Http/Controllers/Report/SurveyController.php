<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
// model
use App\Models\Report\Ranking;
use App\Models\Report\Brand;
use App\Models\Report\DepoToko;
use App\Models\Report\Depo;
use App\Models\Report\Regional;
use App\Models\Blog\Ukuran;
use App\Models\Marketing\NotifSurvey;
use App\Models\Marketing\Motif;
use App\Models\Marketing\DepoSurvey;
use App\Models\Marketing\ImageSurvey;
use App\Http\Requests\StoreSurvey;
use App\DataTables\SurveyNotifDataTable;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\URL;
use DB;
use PDF;
use SnappyImage;
use Image;
use App\Helpers\Survey as SurveyHelper;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotifSurvey as MailNotifSurvey;
use View;
use App\Imports\CustomersExcel;
use Maatwebsite\Excel\Facades\Excel;
use App\Message;
use App\Imports\DataPenjualanImport;
use App\Helpers\UploadFile;
use App\Traits\SurveyTrait;
class SurveyController extends Controller
{
    use SurveyTrait;
    protected $role;
    public function __construct()
    {
        $this->middleware('auth');  
        $this->boot_survey(); 
    }
    public function index()
    {
        $brand = $this->brand;
        $depo = $this->regionalDepo;
        $ukuran = $this->size;
        return view('backend._report.survey', compact('depo','ukuran','brand'));
    }

    public function submitSurvey(StoreSurvey $r)
    {
        $s = new NotifSurvey;
        $s->survey_no = $this->no_survey();
        $s->tgl = Carbon::create($r->tgl);
        $s->tgl_produksi = Carbon::create($r->tgl_produksi);
        $s->brand = $r->brand;
        $s->depo = $r->depo;
        $s->motif = $r->motif;
        $s->ukuran_id = $r->ukuran;
        $s->customer = $r->customer;
        $s->warna = $r->warna;
        $s->p_harga = $r->harga;
        $s->kode_barang = $r->kode_barang;
        $s->status_id = 1;
        $s->save();
        $motif = new Motif;
        $motif->nama = $r->motif;
        $motif->save();
        return back();
    }

    public function grid_status()
    {
        return view('backend._report.grid_status');
    }

    // data tables status survey
    public function get_status()
    {
        // bikin custom search data
        $sr = NotifSurvey::with(['depo_detail','status','ukuran']);
        return $this->notif_survey_table($sr); 
    }
    public function getDepo(Request $r)
    {
        return DepoToko::select(['id_toko','nama_toko'])->where('id_depo',$r->id)->get();
    }
    public function getCustomer(Request $r)
    {
        $toko = $this->customers($r->id,$r->depo);
        return DataTables::of($toko)->make();
    }
    public function depo_survey(Request $r)
    {
        $this->role = Auth::user()->getRoleNames()->toArray();
        if (request()->ajax() && $r->segment(4) == 'proses_survey') { 
                return $this->depo_survey_table();
        }

        if($r->print_form)
        {
            $m = NotifSurvey::with(['depo_survey'])->find($r->print_form);
            // return view('backend._report.pdf_form_survey', compact('m'));
            if($m->status_id != 2){
                return back()->withErrors(['Form tidak dapat di cetak sebelum di proses....!!!!',500]);
            }
            return $this->pdf_form_survey($m);
        }
        return view('backend._report.depo_survey');
    }
    public function batal_depo_survey($id) // hook gagal survey ke lapangan
    {
        $data = NotifSurvey::find($id);
        $data->status_id = 1;
        $data->save();
        return back();
    }
    public function proses_depo_survey($id) // proses depo
    {
        $data = NotifSurvey::with(['depo_detail','status','ukuran'])->find($id);
        if(!in_array($data->status_id,[1,5]))
        {
            return back()->withErrors(['Survey sedang dalam proses di lapangan...!!!',500]);
        }
        $customer = $this->customers($data->customer,$data->depo);
        return view('backend._report.depo_proses',compact('data','customer'));
    }
    public function save_proses_depo_survey(Request $r)
    {
        $this->save_proses_survey($r);
    }
    public function post_depo_survey($id)
    {
        $ds = NotifSurvey::with(['depo_detail','status','ukuran','depo_survey','image'])
                          ->find($id);
        if($ds->status_id == 1)
        {
            return back()->withErrors(['Survey tidak bisa di kembalikan sebelum melakukan survey....!!!',500]);
        }

        return view('backend._report.post_depo_survey',compact('ds'));
    }

    public function update_data_survey(Request $r){ // save data untuk depo survey
        
        $s = NotifSurvey::with(['depo_detail','status','ukuran','depo_survey','image'])
                          ->where('survey_no',$r->survey_no)
                          ->first();

        // $customer_survey = [];
        $s->depo_survey->s_end = Carbon::now();
        $s->status_id = 3;

        if(!$r->image)
        {
            return back()->withErrors(['Dokumen survey harus di sertakan, lengkapi dokumen survey',500]);
        }else{
            $images = ImageSurvey::whereIn('id',explode(',',$r->image))->get();
            foreach($images as $image)
            {
                $image->notif_id = $s->id;
                $image->save();
            }
        }

        foreach ($r->all() as $k => $v) {

            if($k == 'vot')
            {
                $cek = collect($v)->contains('0');
                if($cek)
                {
                    return back()->withErrors(['Customer survey belom lengkap, silahkan isi dengan lengkap',500]);
                }
                $s->depo_survey->vote = $v;
            }

            if($k == 'order')
            {
                $s->depo_survey->order = $v;
            }
            
        }
        $s->depo_survey->status_id = 3;
        $s->depo_survey->save();
        $s->save();
        // dd($s->survey_no);
        
        return redirect()->route('report.deposurvey');      
    }
    public function review_depo($id,$siapa = null,Request $r)
    {
        
        $data = NotifSurvey::with(['depo_detail','status','ukuran','depo_survey','image'])->find($id);
        
        
        if(in_array($data->status_id,[2]))
        {
            return back()->withErrors(['Survey belom dapat direview....!!!',500]);
        }
        $vote = json_decode($data->depo_survey->vote, true);
        $order = json_decode($data->depo_survey->order, true);
        $customer = $data->depo_survey->customer;
        foreach ($data->depo_survey->customer['form_respon'] as $k => $v) {
            $customer['survey'][$k] = [
                'id_form'=>$v['form_id'],
                'nama_sales'=>$v['sales']
            ];
            foreach ($v['customer'] as $i => $val) {
                $id = SurveyHelper::toko($val['id'])->id;
                $nama = SurveyHelper::toko($val['id'])->nama;
                // dump($vote[$id]);
                // bug ngga ada nama customer nya
                    $customer['survey'][$k]['hasil'][] = [
                        'id'=>$id,
                        'nama'=>$nama,
                        'voting'=>isset($vote[$id])?SurveyHelper::status_vote($vote[$id]):SurveyHelper::status_vote(0),
                        'order'=>$order[$id]
                    ];
            }
        }
        if ($r->hasValidSignature()) {
            $apprf_data = ['siapa'=>$siapa];
            $res = compact('data','apprf_data','customer');
        }else{
            $res = compact('data','customer');
        }
        
        return view('backend._report.review_depo',$res);
    }
    
    public function reject_survey($id) // approve kadep rm lewat wa
    {
        $dNotif = NotifSurvey::where('survey_no',$id)->with(['depo_survey','depo_detail','status','ukuran'])->first();
        $user_id = Auth::user()->hasAnyPermission(['staff']);
        if($user_id)
        {
            $dNotif->status_id = 5;
            $dNotif->save();
            $dNotif->depo_survey->status_id = 5;
            $dNotif->depo_survey->save();
            return back()->with('success', 'Data survey tidak di setujui...');  
        }
        return back()->withErrors(['Anda tidak mempunyai hak akses...!!!']);
        
    }
    public function edit_survey($id)
    {
        $survey = NotifSurvey::with(['depo_survey','image'])->where('survey_no',$id)->first();
        // dd($survey->customer);
        return view('backend._report.survey_edit')->with('survey',$survey);
    }
    public function update_survey($id,Request $request)
    {
        $survey = NotifSurvey::with(['depo_survey','image'])->where('survey_no',$id)->first();
        $survey->depo_survey->vote = $request->vote;
        $survey->depo_survey->order = $request->order;
        $survey->depo_survey->save();
        return $survey;
    }
    public function approve_kadep($id,$user)
    {
        $user_id = User::find($user)->permissions;
        foreach ($user_id as $key => $value) {
            if($value->name == 'kadep'){
                $data = NotifSurvey::with(['depo_survey'])->find($id);
                $data->depo_survey->status_id = 6;
                $data->depo_survey->save();
                $data->status_id = 6;
                $data->save();
                return redirect()->route('report.deposurvey');
            }
            if($value->name == 'rm')
            {
                $data = NotifSurvey::with(['depo_survey'])->find($id);
                $data->depo_survey->status_id = 7;
                $data->depo_survey->save();
                $data->status_id = 7;
                $data->save();
                return redirect()->route('report.deposurvey');

            }
        }
        return redirect()->route('report.deposurvey')->withErrors(['Anda tidak mempunyai hak akses...!!!']);
    }
   

    // print form hasil survey
    public function print_survey($id)
    {
            $m = $this->get_survey_by($id);
            if(!$m->cek_status_code([8,7,6,9])){
                return back()->withErrors(['Form tidak dapat di cetak sebelum di proses....!!!!',500]);
            }
            return $this->pdf_print_survey($m);
    }

    public function approve_survey($id,Request $r) //
    {
        // approve survey
        $dNotif = NotifSurvey::where('survey_no',$id)->with(['depo_survey','depo_detail','status','ukuran'])->first();
        $user_id = Auth::user()->hasAnyPermission(['staff']);

        if($user_id)
        {
            $dNotif->status_id = 8;
            $dNotif->save();
            $dNotif->depo_survey->status_id = 8;
            $dNotif->depo_survey->save();

            return back()->with('success', 'Data survey delah di setujui...');  
        }
        // return back()->withErrors(['Anda tidak mempunyai hak akses...!!!']);
        

        // return view('backend._report.approve_reject',compact('data'));
    }
    public function upload_data_customer(Request $r)
    {
        if ($r->hasFile('image')) {
            return (new CustomersExcel)->toCollection($r->file('image'));
        }
    }
    public function depo_review_reject($id)
    {

        return redirect()->route('report.survey.proses',$id);
    }
    protected function customers($data,$depo)
    {
        return DepoToko::select(['id_toko','nama_toko'])
                            ->whereIn('id_toko',$data)
                            ->where('id_depo',$depo)->get();
    }
    public function custoner_depo(Request $request)
    {
        return DepoToko::select(['id_toko','nama_toko'])
                            ->whereIn('id_toko',$request->customer)
                            ->where('id_depo',$request->depo)->get();
        
    }
    public function upload_penjualan(Request $request)
    {
        if ($request->isMethod('post')) {
            Excel::import(new DataPenjualanImport, $request->file('file'));
        }
        return view('backend._report.upload_penjualan');
    }
    public function delete_survey($id)
    {
         NotifSurvey::find($id)->delete();
         ImageSurvey::where('notif_id','=',$id)->delete();
         DepoSurvey::where('notif_id','=',$id)->delete();
         return back();
    }

    // survey sudah selesai
    public function finish_survey($id)
    {
        $notif = NotifSurvey::find($id);
        $notif->update(['status_id'=>9]);
        return back();
    }

    public function delete_image($id,$name)
    {
        UploadFile::remove_file('../storage/app/public/image_survey/'.$name);
        return ImageSurvey::find($id)->delete();
    }
    public function add_image($notif_id,Request $request)
    {
        $fileName =  Str::random(40).'.jpeg';
        $uploadedFile = $request->file('image'); 
        if ($uploadedFile->isValid()) {
            $uploadedFile->move('../storage/app/public/image_survey/',$fileName);
            $this->optimize_upload_image('/storage/image_survey/'.$fileName);
        }
        if($notif_id !== '0')
        {
            $notif_id = NotifSurvey::where('survey_no',$notif_id)->first()->id;
        }
        
        $img = new ImageSurvey;
        $img->notif_id = $notif_id;
        $img->image_name = $fileName;
        $img->image_link = '/storage/image_survey/'.$fileName;
        $img->image = null;
        $img->save();
        return $img;
    }
    public function rotate_image($id)
    {
        $i = ImageSurvey::find($id);
        $img = Image::make('../storage/app/public/image_survey/'.$i->image_name);
        $img->rotate(90);
        $img->save('../storage/app/public/image_survey/'.$i->image_name);
        return $i;
    }
}