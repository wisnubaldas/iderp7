<?php
namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
// model
use App\Models\Report\Ranking;
use App\Models\Report\Brand;
use App\Models\Report\Depo;
use App\Models\Report\MasterTarget;

class TargetController extends Controller
{

	function __construct()
	{
		$this->middleware('auth');
	}
	public function index()
	{
		return view('backend._report.target');
	}
	public function get_depo()
	{
		$result = [];
		foreach (MasterTarget::get() as $v) {
			$depo = Depo::find($v->m_depo_id)->toArray();
			array_push($result,array_merge($v->toArray(),compact('depo')));
		}
		return response($result);
	}
	public function save_target(Request $r)
	{
		$target = MasterTarget::find($r->where);
		$target->target = $r->value;
		$target->save();
	}

}