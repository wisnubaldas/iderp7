<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Str;
// model
use App\Models\Marketing\Motif;
use App\Models\Report\Ranking;
use App\Models\Report\Brand;
use App\Models\Report\DepoToko;
use App\Models\Report\Depo;
use App\Models\Blog\Ukuran;
use App\Models\Marketing\NotifSurvey;
use App\Models\Marketing\DepoSurvey;
use App\Models\Marketing\ImageSurvey;
use App\Models\Marketing\DataPenjualanSurvey;
use PDF;
use DOMPDF;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\View\Components\Alert;
use App\Helpers\Survey;
use App\Helpers\Report;
use App\Helpers\UploadFile;
use Image;
use App\Traits\SurveyTrait;
class ReportSurveyController extends Controller
{
    use SurveyTrait;
    protected $head_tpl;
    public function __construct()
    {
        $this->middleware('auth');
        $this->boot_survey(); 
    }
    
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return $this->status();
        }
        $data_include = [
            'status_survey'=>$this->get_all_status(),
            'data_survey'=>$this->get_by_year(today()->format('Y'))->group_status_survey(),
            'per_bulan'=>$this->get_by_month($this->survey_by_year),
            'bulan'=>$this->bulan,
        ];
        
        return view('backend._report.survey_report',compact('data_include'));
    }
    public function grid()
    {
        $sr = NotifSurvey::
        with(['status','ukuran'])
        // ->where('status_id',4)
        ->groupBy('motif')        
        ->orderBy('updated_at','desc');
        return DataTables::of($sr)
        ->addColumn('status',function($sr){
            return '<button class="btn btn-primary btn-xs">'.$sr->status->status.'</button>';
        })
        ->addColumn('action', function ($sr) {
            // dd($sr->status->id);
            $two = "<li class=\"\"><a class=\"dropdown-item \" href=\"/report/survey/report/create/{$sr->motif}\">Create Report</a></li>";
            return "<div class=\"btn-group\">
                        <button data-toggle=\"dropdown\" class=\"btn btn-primary btn-xs dropdown-toggle\" aria-expanded=\"false\">
                                Action 
                        </button>
                        <ul class=\"dropdown-menu\" x-placement=\"bottom-start\">
                            {$two}
                        </ul>
                    </div>";
        })
        ->rawColumns(['action','status'])
        ->make();  
    }
    public function create($motif)
    {        
        $m = NotifSurvey::with(['depo_survey','depo_detail','status','ukuran'])
                            ->where('motif',$motif)->get();
        // return view('backend._report.pdf_viewer', compact('m'));
            
        // $view = View('backend._report.pdf_viewer');

        $pdf = PDF::loadView('backend._report.pdf_viewer',compact('m'));
        return $pdf->download('invoice.pdf');
    }
    public function get_motif(Request $request)
    {
        $req = $request->fullUrl().'&pdf=1';

        $allNull = Survey::cek_null_array($request->all());
        if($allNull)
        {
            return View::make("components.alert",['message'=>'Field tidak boleh kosong']);
        }
        if (!empty(request()->get('created_at'))) {
            $waktu = explode(' to ',$request->created_at);
            $x = Carbon::createFromFormat('d/m/Y', $waktu[0])->format('Y-m-d 00:00:00');
            $y = Carbon::createFromFormat('d/m/Y', $waktu[1])->format('Y-m-d 00:00:00');

            $hasil = NotifSurvey::ignoreRequest(['perpage','created_at','pdf'])
                            ->whereBetween('tgl',[$x, $y])
                            ->where('status_id',9)
                            ->filter()
                            ->with(['depo_survey','depo_detail','status','ukuran'])
                            ->orderByDesc('id')
                            ->get();
            if($hasil->count() == 0)
            {
                return View::make("components.alert",['message'=>'Data tidak di temukan']);
            }

            $summary_region = $this->summary_region($hasil);
            $summary_toko = $this->summary_toko_data($hasil);
            $head_tpl = $this->head_tpl;
            $penjualan = $this->get_penjualan($hasil);
            if($request->pdf)
            {
               
                return $this->make_pdf($summary_region,$head_tpl,$request->kesimpulan,$request->diajukan,$penjualan);
            }
            

            return $this->view_summary(compact('summary_toko','summary_region','head_tpl','req'));
        } else {
            $hasil =  NotifSurvey::ignoreRequest(['perpage','created_at','pdf'])
                ->where('status_id',9)
                ->filter()
                ->with(['depo_survey','depo_detail','status','ukuran'])
                ->orderByDesc('id')->get();
            if($hasil->count() == 0)
            {
                return View::make("components.alert",['message'=>'Data tidak di temukan']);
            }
            $summary_toko = $this->summary_toko_data($hasil);
            $summary_region = $this->summary_region($hasil);
            $head_tpl = $this->head_tpl;
            $penjualan = $this->get_penjualan($hasil);

            if($request->pdf)
            {
                return $this->make_pdf($summary_region,$head_tpl,$request->kesimpulan,$request->diajukan,$penjualan);
            }

            return $this->view_summary(compact('summary_toko','summary_region','head_tpl','req'));
        }
    }
    protected function get_penjualan($data)
    {
        $kode_barang = $data->pluck('kode_barang')->unique();
        $return = DataPenjualanSurvey::whereIn('item',$kode_barang)->get();
            return $return->groupBy('region')->map(function($collect){
                return $collect->groupBy('depo')->map(function($net){
                    return $net->sum('net');
                })->toArray();
            });
    }
    protected function make_pdf($summary_region,$head_tpl,$kesimpulan,$diajukan,$penjualan = [])
    {
        // return view('backend._report.pdf_report_survey',compact('summary_region','head_tpl','kesimpulan','diajukan','penjualan'));
        $nama = UploadFile::make_name();
        $pdf = PDF::loadView('backend._report.pdf_report_survey',compact('summary_region','head_tpl','kesimpulan','diajukan','penjualan'))
                ->setPaper('a4')
                ->setOption('enable-javascript', true)
                ->setOption('javascript-delay', 1500)
                ->setOption('no-stop-slow-scripts', true)
                ->setOption('enable-smart-shrinking', true)
                // ->setOption('load-error-handling','ignore')
                // ->setOrientation('landscape')
                ->setOption('margin-bottom', 0)
                ->setOption('margin-top', 15);
        return $pdf->download($nama.'.pdf');
    }
    protected function head_template($hasil)
    {
        return $hasil->map(function($a,$b){
            $brand =  $a['brand'];
            $motif = $a['motif'];
            $ukuran = $a['ukuran']->title;
            $warna = $a['warna'];
            return compact('brand','motif','ukuran','warna'); 
        })->unique()->values();
    }

    protected function summary_region($hasil)
    {
        // dd($hasil);
        $this->head_tpl = $this->head_template($hasil);
        $de_reg = $this->depo->keyBy('id_depo')
                    ->groupBy('id_reg')
                    ->map(function($item,$key)
                    {
                        return collect($item)->map(function($x){
                            return [
                                'id_depo'=>$x['id_depo'],
                                'nama_depo'=>$x['nama_depo'],
                                'id_reg'=>$x['id_reg'],
                                'nama_reg'=>$x['depo_regional']['nama_reg'],
                                'urut_reg'=>$x['depo_regional']['urut_reg'],
                            ];
                        });
                    })->sortBy(function($item,$key){
                            return $item->first()['urut_reg'];
                    });
        // @dump($de_reg);
        $data = $hasil->map(function ($item, $key) {
            return $item->only(['depo','depo_survey','depo_detail']);
        })->keyBy('depo')->toArray();
        // dump($data);
        $result = [];
        foreach($de_reg as $id => $el)
        {
            foreach($el as $idx => $elx)
            {
                $zz = (isset($data[$elx['id_depo']])?$data[$elx['id_depo']]:false);
                $xx = collect($zz['depo_survey'])
                        ->only(['vote','order'])
                        ->map(function($z){
                                return json_decode($z,true);
                        })->toArray();
                $elx['survey'] = $xx;
                $el = $elx;
                array_push($result,$el);
            }
        }
        return $result;
    }
    protected function summary_toko_data($hasil)
    {
        $depoData = $hasil->sortBy('depo_detail.urut_depo');
        $x = (object)[];
        foreach ($depoData as $key => $value) {
            $x->depo[] = $value['depo_detail']->nama_depo;
            $x->vote[] = json_decode($value['depo_survey']->vote);
            $x->order[] = json_decode($value['depo_survey']->order);
        }
        return $x;
    }
    protected function view_summary($hasil)
    {
        $summary = View::make('components.survey.summary-toko')
                        ->with(compact('hasil'));
            return $summary->renderSections();
    }
    public function get_all_motif(Request $r)
    {
        $data = NotifSurvey::with(['depo_survey','depo_detail','status','ukuran'])->find($r->data);
        return View::make('backend._report.pdf_sumary_report',compact('data'));
    }
}
