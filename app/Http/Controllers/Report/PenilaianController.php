<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Models\Report\Ranking;
use App\Models\Report\Brand;
use App\Models\Report\Depo;
use App\Models\Report\MasterTarget;
use App\Models\Report\PenjualanPerhari;
class PenilaianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('backend._report.penilaian');
    }
    public function get_data()
    {
    	$depo = Depo::get();
    	$brand = Brand::get();
    	$target = MasterTarget::all();
    	return response()->json(compact('depo','brand','target'));
    }
    public function get_penjualan(Request $r)
    {
    	$date = new Carbon($r->tanggal);
        $tglSelect = $date->format('Y-m-d');
        $tglSatu = $date->firstOfMonth()->format('Y-m-d');
        $file = PenjualanPerhari::whereBetween('tgl',
                                    [
                                        $tglSatu,
                                        $tglSelect
                                    ])->get();
        if($file->count() > 1)
        {
            $x = [];
            foreach ($file as $v) {
                array_push($x,$v->data);
            }
            return response()->json(['data'=>$x]);
        }
        return response()->json(['status'=>'data tidak di temukan']);
    }
}