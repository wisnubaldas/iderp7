<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Report\Lshjadlh;
use DataTables;
use App\Imports\LamaHutangExcel;
use App\Exports\LamaHutangExcel as LamaHutangExcelExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ReportSimHargaJualController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('backend.lshjdlh.index');
    }
    public function save(Request $request)
    {
        $x = new Lshjadlh;
        $x->kd_barang = $request->kd_barang;
        $x->motif = $request->motif;
        $x->harga_pabrik = $request->harga_pabrik;
        $x->harga_normal = $request->harga_normal;
        $x->keterangan = $request->keterangan;
        $disc = json_decode($request->disc);
        if(!$disc)
        {
            return back();
        }
        $x->discount = $disc->discount;
        $x->bunga_1 = $disc->bunga_1;
        $x->bunga_2 = $disc->bunga_2;
        $x->bunga_3 = $disc->bunga_3;
        $x->bunga_4 = $disc->bunga_4;
        $x->bunga_5 = $disc->bunga_5;
        $x->save();
        return back();
        // dump($disc);
        // dump($request->all());
    }
    public function edit($id)
    {
        $x = Lshjadlh::find($id);
        return view('backend.lshjdlh.edit')->with('editData',$x);
    }
    public function update($id,Request $request)
    {
        $x = Lshjadlh::find($id);
        $x->kd_barang = $request->kd_barang;
        $x->motif = $request->motif;
        $x->harga_pabrik = $request->harga_pabrik;
        $x->harga_normal = $request->harga_normal;
        $x->keterangan = $request->keterangan;
        $disc = json_decode($request->disc);
        if($disc)
        {
            $x->discount = $disc->discount;
            $x->bunga_1 = $disc->bunga_1;
            $x->bunga_2 = $disc->bunga_2;
            $x->bunga_3 = $disc->bunga_3;
            $x->bunga_4 = $disc->bunga_4;
            $x->bunga_5 = $disc->bunga_5;
        }
        $x->save();
        return redirect()->route('lshjadlh');
    }
    public function grid()
    {
        $model = Lshjadlh::query();
        return DataTables::eloquent($model)
                ->filter(function ($query) {
                    // dump(request()->all());
                        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", request()->search['value'])) {
                            $query->where('created_at', 'like', "%" . request()->search['value'] . "%");
                        }else{
                            $query
                            ->where('kd_barang', 'like', "%" . request()->search['value'] . "%")
                            ->orWhere('motif', 'like', "%" . request()->search['value'] . "%")
                            ->orWhere('harga_pabrik', 'like', "%" . request()->search['value'] . "%")
                            ->orWhere('harga_normal', 'like', "%" . request()->search['value'] . "%");
                        }
                })
                ->editColumn('kd_barang', '{{Str::upper($kd_barang)}}')
                ->editColumn('motif', '{{Str::upper($motif)}}')
                ->editColumn('harga_pabrik', '{{number_format($harga_pabrik)}}')
                ->editColumn('harga_normal', '{{number_format($harga_normal)}}')
                ->editColumn('bunga_1', '{{number_format($bunga_1)}}')
                ->editColumn('bunga_2', '{{number_format($bunga_2)}}')
                ->editColumn('bunga_3', '{{number_format($bunga_3)}}')
                ->editColumn('bunga_4', '{{number_format($bunga_4)}}')
                ->editColumn('bunga_5', '{{number_format($bunga_5)}}')
                ->editColumn('discount', '{{number_format($discount)}}')
                ->addColumn('action', '<a href="/report/lshjadlh/edit/{{$id}}" id="edit-col" clas="btn btn-info btn-outline"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>')
                ->rawColumns(['action'])
                ->toJson();
    }
    public function upload(Request $request)
    {
        \request()->validate(['file' => 'mimes:csv,excel,vnd.msexcel,xlsx,xls']);
        if ($request->hasFile('file')) {
            Excel::import(new LamaHutangExcel, $request->file('file'));
            return back()->with('success', 'All good!');
        }
    }
    public function export(Request $request)
    {
        // dump($request->all());
        // return Lshjadlh::find(explode(',',$request->ids));
        $name = Str::random(40);
        
        Excel::store(new LamaHutangExcelExport($request->ids), 'public/excel/'.$name.'.xlsx');
        return ['link'=>'/storage/excel/'.$name.'.xlsx'];
    }
}
