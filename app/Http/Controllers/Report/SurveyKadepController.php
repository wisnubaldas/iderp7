<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Str;
// model
use App\Models\Report\Ranking;
use App\Models\Report\Brand;
use App\Models\Report\DepoToko;
use App\Models\Report\Depo;
use App\Models\Report\Regional;
use App\Models\Blog\Ukuran;
use App\Models\Marketing\NotifSurvey;
use App\Models\Marketing\Motif;
use App\Models\Marketing\DepoSurvey;
use App\Models\Marketing\ImageSurvey;
use App\Http\Requests\StoreSurvey;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\URL;
use DB;
use PDF;
use SnappyImage;
use Image;
use App\Helpers\Survey as SurveyHelper;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotifSurvey as MailNotifSurvey;
use View;
use App\Imports\CustomersExcel;
use Maatwebsite\Excel\Facades\Excel;
use App\Message;
use App\Imports\DataPenjualanImport;
class SurveyKadepController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        // return view('backend._report.approve_kadep.index')->with('depo_id',auth()->user()->depo_id);
    }
    public function confirm($id)
    {
        if(Auth::user()->jabatan == 'rm')
        {
            $notif = NotifSurvey::find($id);
            $notif->update(['status_id'=>7]);
            $depo = DepoSurvey::where('notif_id',$id);
            $depo->update(['status_id'=>7]);
            return back(); 
        }
        if(Auth::user()->jabatan == 'kadep')
        {
            $notif = NotifSurvey::find($id);
            $notif->update(['status_id'=>6]);
            $depo = DepoSurvey::where('notif_id',$id);
            $depo->update(['status_id'=>6]);
            return back(); 
        }
        return back()->withErrors(['msg', 'Error user tidak di kenal']);
    }
    public function detail_survey($id)
    {
        $survey = NotifSurvey::with(['depo_survey'])->where('survey_no',$id)->first();
        $customer = array_keys(json_decode($survey->depo_survey->vote, true));
        $customer = DepoToko::select(['id_toko','nama_toko'])
                        ->whereIn('id_toko',$customer)
                        ->where('id_depo',$survey->depo)
                        ->get();
        return view('backend._report.approve_kadep.detail',compact('survey','customer'));
    }
}
