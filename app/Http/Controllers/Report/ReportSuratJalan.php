<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
// model
use App\Models\Report\Ranking;
use App\Models\Report\Brand;
use App\Models\Report\Depo;
use Illuminate\Support\Facades\Http;
use PDF;
use App\Helpers\Logging;
use Illuminate\Support\Facades\View;
class ReportSuratJalan extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        
        return view('backend.surat_jalan.index');
    }
    public function get_sj(Request $r)
    {
        $prod = 'http://192.168.0.201:9090';
        // 'SJ-20110483
        $response = Http::get($prod.'/PrintSuratJalan/get', [
            'sj' => $r->sj,
        ]);
        
        if(count($response->json()) > 0)
        {
            $log = new Logging;
            $log->create_log([
                    'tipe_log'=>1,
                    'user_id'=>auth()->user()->id,
                    'mark'=>$r->sj,
                    'created_at'=>now(),
            ]);

            $x = collect($response->json())->unique(function ($item) {

                return $item['no_sj'].$item['kode_barang'].$item['jml'];

            });
            if(isset($r->pdf))
            {
                $pdf = PDF::loadView('backend._report.pdf_surat_jalan',['sj'=>$x->groupBy('no_sj')])
                            // ->setPaper('letter')
                            // ->setOrientation('landscape')
                            ->setOption('page-width', '216')
                            ->setOption('page-height', '139.7')
                            ->setOption('margin-top', 0)
                            ->setOption('margin-right', 0)
                            ->setOption('margin-bottom', 0);
                return $pdf->download($r->sj.'.pdf');
            }
            return View::make('components.surat_jalan',['sj'=>$x]);
        }else{
            return View::make('components.alert',['message'=>'Nomer Surat Jalan tidak ditemukan']);
        }
       
    }
    
}
