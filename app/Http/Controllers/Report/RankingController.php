<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
// model
use App\Models\Report\Ranking;
use App\Models\Report\Brand;
use App\Models\Report\Depo;

class RankingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('backend.ranking.index');
    }
    public function get_rangking(Request $r)
    {

        $date = new Carbon($r->datePicker);
        $file = Ranking::select(['url','id','tanggal','data_ranking'])->where('tanggal',$date->format('Y-m-d'))->firstOrFail();
        if($file)
        {
            // $file = Storage::disk('local')->get($file->url);
            // foreach (json_decode($file) as $i => $v) {
            //     if(\count($v) != 0)
            //     {
            //         $result[$i] = $v;
            //     }
            // }
            return collect(['date'=>$date->format('d F, Y'),'data'=>json_decode($file->data_ranking)]);
        }else{
            abort('422','data tidak ditemukan');

        }
    }
    public function get_brand()
    {
        return Brand::all();
    }
    public function get_depo()
    {
        return Depo::orderBy('urut_depo')->get();
    }
}
