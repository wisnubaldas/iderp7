<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\MenuModule;
use App\Models\Admin\Menu;
use App\Models\Blog\Distributor;
use App\Models\Blog\Slide;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Str;
// model
use App\Models\Report\Brand;
use App\Models\Report\DepoToko;
use App\Models\Report\Depo;
use App\Models\Blog\Ukuran;
use App\Models\Marketing\NotifSurvey;
use App\Models\Marketing\DepoSurvey;
use App\Models\Marketing\ImageSurvey;
use App\Http\Requests\StoreSurvey;
use App\DataTables\SurveyNotifDataTable;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\URL;
use DB;
class WelcomeController extends Controller
{
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Slide::all();
        return view('frontend.blog.index',\compact('data'));
    }
    public function approve($id,$siapa)
    {
        
        $data = NotifSurvey::with(['depo_detail','status','ukuran','depo_survey','image'])->find($id);
        $customer = $this->customers($data->depo_survey->customer,$data->depo);
        return view('backend._report.review_depo',compact('data','customer','apprf_data'));   
    }
    protected function customers($data,$depo)
    {
        return DepoToko::select(['id_toko','nama_toko'])
                            ->whereIn('id_toko',$data)
                            ->where('id_depo',$depo)->get();
    }

    public function php_info()
    {
        echo phpinfo();
    }
    public function distributor()
    {
        return Distributor::all();
    }

    protected function alamat()
    {
        return [
                    [
                        'JAKARTA',
                        'Jl. Jembatan III Barat Blok E No. 4, Penjaringan, 
                                  Jakarta Utara - INDONESIA Email : pluit@picasso-tile.com'
                    ],
                    [
                        'BEKASI',
                        ' Dusun Krajan 2, Desa Warung Bambu,
                        Kec. Karawang Timur, Kab. Karawang - INDONESIA
                        Email : Bekasi@picasso-tile.com'
                    ],
                    [
                        'BOGOR',
                        'Jl. Mayor Oking, Ciri Mekar,
                        Cibinong – INDONESIA
                        Email : Bogor@picasso-tile.com'
                    ],
                    [
                        'SERANG',
                        ' Jl. Kaserangan Kp. Nambo, Desa Kaserangan,
                        Kec. Ciruas, Kab. Serang – INDONESIA
                        Email : serang@picasso-tile.com'
                    ],
                    [
                        'LAMPUNG',
                        ' Dusun Kejadian Kurungan Nyawa, Kec. Gedong Tataan,
                        Kab. Pesawaran, Lampung – INDONESIA
                        Email : lampung@picasso-tile.com'
                    ],
                    [
                        'BANDUNG',
                        'Komp. Pergudangan De Prima, Kav. 7 - 8 (D1 - D2),
                        Kec. Bojongsoang, Kab. Bandung – INDONESIA
                        Email : bandung@picasso-tile.com'
                    ],
                    [
                        'CIREBON',
                        ' Jl. Cirebon – Tegal Km. 11 No. 59, Astanajapura,
                        Cirebon – INDONESIA
                        Email : cirebon@picasso-tile.com'
                    ],
                    [
                        'TASIKMALAYA',
                        ' Jl. Letjen Mashudi, Kel. Setiajaya,
                        Kec. Cibeureum, Kota Tasikmalaya – INDONESIA
                        Email : tasikmalaya@picasso-tile.com'
                    ],
                    [
                        'SUKABUMI',
                        'Jl. Pembangunan, Kel. Babakan,
                        Kec. Cibeureum, Kota Sukabumi – INDONESIA
                        Email : sukabumi@picasso-tile.com'
                    ],
                    [
                      'SEMARANG',
                    'Kawasan Industri Candi Blok, Jl. Gatot Subroto XIX No. 16
                    Kel. Ngalian, Kec. Ngalian, Kota Semarang – INDONESIA
                    Email : semarang@picasso-tile.com'
                    ],
                    [
                        'YOGYAKARTA',
                        'Jl. Ringroad Barat Gatak, Desa Tamantirto,
                        Kec. Kasihan, Bantul, Yogyakarta – INDONESIA
                        Email : yogyakarta@picasso-tile.com'
                    ],
                    [
                        'PURWOKERTO',
                        'Jl. Diponegoro No. 8 Pamijen, Sokaraja Kulon,
                        Purwokerto – INDONESIA
                        Email : purwokerto@picasso-tile.com'
                    ],                 
                    [
                        'SOLO',
                       ' Desa Banyudono, Kec. Banyudono,
                        Kab. Boyolali, Solo – INDONESIA
                        Email : solo@picasso-tile.com'
                    ],
                    [
                        'SURABAYA',
                        'Komp. Pergudangan Tambak Langon Indah, Blok B1,
                        Jl. Raya Tambak Langon No. 27, Surabaya – INDONESIA
                        Email : surabaya@picasso-tile.com'
                    ],                 
[
    'JEMBER',
    'Jl. Wolter Monginsidi, Desa Rowo Indah,
    Jember – INDONESIA
    Email : jember@picasso-tile.com'
],
[
    'KEDIRI',
    'Jl. Kediri Blitar, Blabak, Kandat,
    Kab. Kediri – INDONESIA
    Email : kediri@picasso-tile.com'
],
[
    'MALANG',
    'Jl. Raya Karangsono No. 155, Desa Kebon Agung,
    Kec. Pakisaji, Kab. Malang – INDONESIA
    Email : malang@picasso-tile.com'
],
[
    'BALI',
    'Jl. Raya Mengwitani No. 88 A, Banjar Jumpayah,
    Kec. Mengwitani, Kab. Badung, Bali – INDONESIA
    Email : bali@picasso-tile.com'
],
[
    'BENGKULU',
    'Jl. Hibrida Raya Ujung No. 4 A-D, Kel. Pagar Dewa,
    Kec. Selebar, Bengkulu – INDONESIA
    Email : bengkulu@picasso-tile.com'
],
[
    'PALEMBANG',
    'Jl. By Pass Terminal Alang-Alang Lebar Km. 12,
    Ruko No. 8, Palembang – INDONESIA
    Email : palembang@picasso-tile.com'
],
[
    'JAMBI',
    'Jl. Lingkar Selatan (Simpang Ahok), Kel. Lingkar Selatan,
    Kec. Jambi Selatan, Kota Jambi – INDONESIA
    Email : jambi@picasso-tile.com'
],
                    [
                        'MEDAN',
                   ' Jl. Kayu Putih, Komp. Pergudangan Kayu Putih Estate No. 3B,
                    Kota Medan – INDONESIA
                    Email : medan@picasso-tile.com'
                    ],
                    [
                        'PEKANBARU',
                        'Jl. Garuda Sakti Km. 3, Komp. Pergudangan Angkasa II Blok A7,
                        Pekanbaru – INDONESIA
                        Email : pekanbaru@picasso-tile.com'
                    ],
[
    'PADANG',
    'Jl. Batuang Taba No. 22 Kel. Batuang Taba No. 20,
    Padang – INDONESIA
    Email : padang@picasso-tile.com'
],
[
    'BANGKA',
   ' Jl. Koba Lama Km. 8 No. 430, Pangkalan Baru Minfo,
    Komp. Pergudangan Asun, Pangkal Pinang, Bangka – INDONESIA
    Email : bangka@picasso-tile.com'
],
[
    'MAKASSAR',
    'Jl. Ir. Soetami (Jl. Damang) No. 186 Kel. Parangloe,
    Kec. Tamalanrea, Makassar – INDONESIA
    Email : makassar@picasso-tile.com'
],
[
    'BANJARMASIN',
    'Jl. Ir. Soebarjo (Jl. Lingkar Pelabuhan) No. 778,
    Banjarmasin – INDONESIA
    Email : banjarmasin@picasso-tile.com'
],
[
    'PONTIANAK',
    'Jl. Adi Sucipto Km. 8.9 No. B4, Pontianak – INDONESIA
    Email : pontianak@picasso-tile.com'
],
[
    'MANADO',
'Jl. Ring Road Bizz Blok C No. 1,
    Manado – INDONESIA
    Email : manado@picasso-tile.com'
]
                    
            ];
    }

}
