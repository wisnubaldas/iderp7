<?php

namespace App\Http\Controllers\Sys;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use App\Models\Admin\Menu;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DataTables;

class RolebaseController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $role = Role::all();
        $permission = Permission::all();
    	return view('backend.system.rolebase',compact('role','permission'));
    }
   	public function grid(Request $request)
   	{
           $r = Role::all();
            return DataTables::of($r)
            ->addColumn('permission',function($sr){
                $permission = Role::findByName($sr->name)->permissions;
                $x = '<div class="form-group">';
                foreach ($permission as $key => $value) {
                    $y = ' <input  checked type="checkbox" class="form-control" value="'.$value->id.    '"> ';
                    $x .= '<label class="checkbox-inline i-checks m-l-md">'.$y.$value->name.'</label>';
                }
                $x .= '</div>';
                return $x;
            })
            ->addColumn('action', function ($sr) {
                // dd($sr->status->id);
                return '<span class="label label-danger" >'.$sr->id.'</span>';
            })
            ->rawColumns(['action','permission'])
            ->make();  
    }
    public function update(Request $request)
    {   
        // dump($request->params);
        $user = User::find($request->params['id']);
        $user->name = $request->params['name'];
        $user->email = $request->params['email'];
        $user->save();
        return $user->id;
        // return $this->input->all();
    }
    public function insert($stat,Request $request)
    {
        if ($stat == 'role') {  
            $request->validate([
                'role' => 'required|max:255',
            ]);
            Role::create(['name' => $request->role]);
            return back();
        }
        if($stat == 'permission')
        {
            $request->validate([
                'permission' => 'required|max:255',
            ]);
            Permission::create(['name' => $request->permission]);
            return back();
        }
        if($stat == 'async')
        {
            $r = $request->async_role;
            $p = $request->async_permission;
            if(count($r) == 1 && count($p) > 1)
            {
                $role = Role::findByName($r[0]);
                $role->syncPermissions($p);
                return back();
            }elseif (count($r) > 1 && count($p) == 1) {
                $permission = Permission::whereName($p[0])->first();
                $permission->syncRoles($r);
                return back();
            }else{
                return back()->withErrors(['Salah satu harus di async role atau permission nya']);
            }
        }
        if($stat == 'revoke')
        {
            if(isset($request->async_role))
            {
                // $permission = Permission::whereName($request->async_permission)->first();
                // $role->revokePermissionTo($permission);
                return back();
            }
            if(isset($request->async_permission))
            {
                // $role = Role::findByName($request->async_role);
                // $permission->removeRole($role);
                return back();
            }

        }
        return back()->withErrors(['Terjadi kesalahan request pada server']);
    }
    public function get_permission()
    {
        $role = Role::all();
        $permission = Permission::all();
        return compact('role','permission');
    }
}
