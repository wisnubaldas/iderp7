<?php

namespace App\Http\Controllers\Sys;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use App\Models\Admin\Menu;
use App\Models\Admin\MenuModule;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Report\Depo;
use DataTables;
use Illuminate\Support\Facades\Crypt;
class UserManagerController extends Controller
{
    private $role;
    private $permission;
	public function __construct()
    {
        $this->middleware('auth');
        $this->role = Crypt::encryptString('role');
        $this->permission = Crypt::encryptString('permission');
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            return self::grid_user();
        }
    	return view('backend.system.user_manager');
    }
    private static function grid_user()
    {
        
        return DataTables::of(User::query())
            ->setRowId('id')
            ->setRowClass(function ($user) {
                return $user->id % 2 == 0 ? 'bg-silver no-margins no-paddings font-bold' : 'bg-aqua no-margins no-paddings font-bold';
            })
            ->setRowData([
                'id' => 'test',
            ])
            ->setRowAttr([
                'color' => 'red',
            ])
            ->addColumn('permission', function ($user) {
                $rp = new self;
                $x = '';
                foreach ($user->getPermissionNames() as $key => $value) {
                    $crot = ucfirst($value);
                    $permission = Crypt::encryptString($value);
                    $id = Crypt::encryptString($user->id);
                    $x .= "<a href='/system/user_manager/role_permission/{$rp->permission}/{$permission}/{$id}' class='btn btn-danger btn-outline btn-sm'>{$crot}</a>";
                }
                return $x;
            })
            ->addColumn('menu',function($user){
                if($user->menu)
                {
                    $men = '';
                    $m = Menu::whereIn('id',$user->menu)->get();
                    foreach($m as $mm)
                    {
                        $men .= "<label class='label label-primary'>".$mm->title."</label> ";
                    }
                    return $men;
                }else{
                    return "<label class='label label-success'>Tidak Ada Menu</label>";
                }
            })
            ->addColumn('modul',function($user){
                if($user->modul){
                    $modMod = '';
                    $mod = MenuModule::whereIn('id',$user->modul);
                    foreach($mod->get() as $mid)
                    {
                        $modMod .= "<label class='label label-primary'>".$mid->name."</label> ";
                    }
                    return $modMod;   
                }else{
                    return "<label class='label label-success'>Tidak Ada Modul</label>";
                }
            })
            ->addColumn('role', function ($user) {
                $rp = new self;
                $x = '';
                foreach ($user->getRoleNames() as $key => $value) {
                    $crot = ucfirst($value);
                    $permission = Crypt::encryptString($value);
                    $id = Crypt::encryptString($user->id);
                    $x .= "<a href='/system/user_manager/role_permission/{$rp->role}/{$permission}/{$id}' class='btn btn-success btn-outline btn-sm'>{$crot}</a>";
                }
                return $x;
            })
            ->addColumn('action', function ($user) {
                return '<div class="btn-group">
                <a href="'.route('user_manager.edit',$user->id).'" class="btn btn-sm btn-primary">
                <i class="fa fa-edit"></i> Edit</a>
                <a href="'.route('user_manager.delete',Crypt::encryptString($user->id)).'" class="btn btn-sm btn-danger">
                <i class="fa fa-trash-o"></i> Delete</a>
                </div>';
            })
            ->rawColumns(['permission','action','role','menu','modul'])
            ->make(true);
    }
    public function edit_user($id)
    {
        $user = User::with(['roles','permissions'])->find($id);
        $roles = Role::all();
        $permissions = Permission::all();
        return view('backend.system.user_edit',compact('user','roles','permissions'));

    }
    public function delete_user($id)
    {
        $id = Crypt::decryptString($id);
        $res = User::where('id',$id)->delete();
        return back();
        
    }
    public function create_user()
    {
        $depo = Depo::all();
        return view('backend.system.user_create',compact('depo'));
    }
    public function save_user($id, Request $request)
    {

        $user = User::with(['roles','permissions'])->find($id);
        $user->email = $request->email;
        $user->name = $request->name;
        $user->syncPermissions($request->permission);
        $user->syncRoles($request->role);
        // $user->givePermissionTo($request->permission);
        // $user->assignRole($request->role);
        return $user->save();
    }
    public function insert(Request $request)
    {
        

         User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'depo_id'=>$request->depo_id,
            'menu'=> json_encode($request->menu),
            'modul'=>json_encode($request->modul),
        ]);
        return back();
    }

    public function role_permission($stat,$permission,$id)
    {
        $rp = Crypt::decryptString($stat);
        $permission = Crypt::decryptString($permission);
        $id = Crypt::decryptString($id);
    }
   	public function grid(Request $request)
   	{
   		$length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
        $query = User::eloquentQuery($sortBy, $orderBy, $searchValue);
        $data = $query->paginate($length);

        return new DataTableCollectionResource($data);
    }
    public function update(Request $request)
    {   
        // dump($request->params);
        $user = User::find($request->params['id']);
        $user->name = $request->params['name'];
        $user->email = $request->params['email'];
        $user->save();
        return $user->id;
        // return $this->input->all();
    }
    
    public function get_permission()
    {
        $role = Role::all();
        $permission = Permission::all();
        return compact('role','permission');
    }
    public function assign_user_role($id,Request $request)
    {
        $role = Role::all();
        $permission = Permission::all();
        if ($request->isMethod('post')) {
            if($id == 0)
            {
                $role = Role::findByName($request->role);
                return $role->permissions()->get();
            }else{
                $request->validate([
                    'async_role' => 'required|max:255',
                    'async_permission' => 'required',
                ]);
                $user = User::find($id);
                $user->assignRole($request->async_role);
                if(count($request->async_permission) != 0)
                {
                    foreach ($request->async_permission as $key => $value) {
                        $user->givePermissionTo($value);
                    }
                }
                return back();
            }
        }
        $user = User::with(['roles','permissions'])->find($id);
        $module = MenuModule::all();
        $menu = Menu::all();
        $depo = Depo::all();
        return view('backend.system.user_role',compact('user','role','permission','menu','module','depo'));
    }
    public function menu_module($id,Request $request)
    {
        $user = User::find($id);
        $user->modul = $request->module;
        $user->menu = $request->menu;
        $user->depo_id = $request->depo;
        $user->save();
        return back();
    }
}
