<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SampleController extends Controller
{
    public function index(Request $request)
    {
        return view('finance.sample.index');
    }
    public function get_data(Request $request)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        $sj_clean = implode(',',explode(',',str_replace(array("\r","\n",'\r','\n'," "), '', strtoupper($request->sj))));
                $client = new \GuzzleHttp\Client();
                $response = $client->request('GET', '192.168.0.201:9090/SjSample/get?sj='.$sj_clean);
                // echo $response->getStatusCode(); // 200
                // echo $response->getHeaderLine('content-type'); // 'application/json; charset=utf8'
                return $response->getBody(); // '{"id": 1420053, "name": "guzzle", ...}'        
    }
    public function post_data(Request $request)
    {
        $data = [
            'sj'=>$request->all()[1],
            'item'=>$request->all()[2],
            'sample'=>$request->all()[3]
        ];
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => '192.168.0.201:9090/SjSample/post_sample',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>json_encode($data),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo $response;     

    //    return $request->all();
    }
}
