<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\MenuModule;
use App\Models\Admin\Menu;
use App\Models\Marketing\NotifSurvey;
use App\Models\Marketing\Motif;
use App\Models\Marketing\DepoSurvey;
use App\Models\Marketing\ImageSurvey;
use App\User;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if($user->jabatan == 'kadep')
        {
            $status = [8];
            $data = NotifSurvey::whereIn('status_id',$status)
            ->where('depo',$user->depo_id)->get();
            return view('backend._report.approve_kadep.index')->with('user',$user)->with('data',$data);
        }
        if($user->jabatan == 'rm')
        {
            $status = [6];
            $data = NotifSurvey::whereIn('status_id',$status)
            ->where('depo',$user->depo_id)->get();
            return view('backend._report.approve_kadep.index')->with('user',$user)->with('data',$data);
        }
        if(in_array('super-admin',$user->getRoleNames()->toArray()))
        {
            return view('backend.dashboard',['modul'=>MenuModule::where('void',1)->get(),'menu'=>[],'d'=>['satu'=>'satu']]);
        }
        if(!empty($user->modul))
        {
            return view('backend.dashboard',['modul'=>MenuModule::whereIn('id',$user->modul)->get(),'menu'=>[],'d'=>['satu'=>'satu']]);
        }
        
    }
    public function get_menu($id,Request $r)
    {
        $user = Auth::user();
        $r->session()->forget('menu');
        if(in_array('super-admin',$user->getRoleNames()->toArray()))
        {
            $m = Menu::where('menu_modules_id',$id)->where('active',1)->get();
        }else{
            $m = Menu::whereIn('id',$user->menu)->where('active',1)->get();
        }
        $m = $this->buildTree($m->toArray(), 'parent_id', 'id');
        if($m)
        {
            $r->session()->put('menu',\json_encode($m));
        }
        
        return \back();
    }
    private function buildTree($flatStructure, $pidKey, $idKey = null)
    {
        if(\count($flatStructure) <= 0)
        {
            return false;
        }

        $parents = array();
        foreach ($flatStructure as $item){
            $parents[$item[$pidKey]][] = $item;
        }

        $fnBuilder = function($items, $parents, $idKey) use (&$fnBuilder) {
            foreach ($items as $position => $item) {
                $id = $item[$idKey];
                if(isset($parents[$id])) { // kumpulin paren nya
                    $item['sub_menu'] = $fnBuilder($parents[$id], $parents, $idKey); //masukin child
                }

                //reset the value as children have changed
                $items[$position] = $item;
            }

            //return the item
            return $items;
        };

        return $fnBuilder($parents[0], $parents, $idKey);
    }
}
