<?php

namespace App\Http\Middleware;

use Closure;

class CrotMid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->segment(3) == 'status_grid')
        {
            return redirect()->route('login');
        }
         $next($request);
    }
}
