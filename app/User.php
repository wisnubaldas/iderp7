<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable  
{
    use Notifiable;
    use LaravelVueDatatableTrait;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','depo_id','menu','modul','jabatan',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_at','updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getMenuAttribute($value)
    {
        return json_decode($value);
    }
    public function getModulAttribute($value)
    {
        return json_decode($value);
    }

    public function module()
    {
        return $this->belongsToMany('App\Models\Admin\MenuModule');
    }
}
