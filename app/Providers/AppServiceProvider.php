<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use Jenssegers\Agent\Agent;

// use App\Helpers\UploadFile;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind('UploadFile',UploadFile::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //  
        $agent = new Agent();
        View::share('agent', $agent);
    }
}
