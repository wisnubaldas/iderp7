<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Report\Lshjadlh;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;
class LamaHutangExcel implements FromQuery, WithHeadings, WithStyles, ShouldAutoSize, WithMapping
{
    use Exportable;

    public function __construct(string $ids)
    {
        $this->ids = $ids;
    }

    public function query()
    {
        return Lshjadlh::query()->select(['id','kd_barang','motif','harga_pabrik','discount','harga_normal','bunga_1','bunga_2','bunga_3','bunga_4','bunga_5','keterangan'])
        ->whereIn('id',explode(',',$this->ids));
        // return Invoice::query()->whereYear('created_at', $this->ids);
    }
    public function map($ht): array
    {
        return [
            $ht->id,
            strtoupper($ht->kd_barang),
            strtoupper($ht->motif),
            number_format($ht->harga_pabrik),
            number_format($ht->discount),
            number_format($ht->harga_normal),
            number_format($ht->bunga_1),
            number_format($ht->bunga_2),
            number_format($ht->bunga_3),
            number_format($ht->bunga_4),
            number_format($ht->bunga_5),
            $ht->keterangan,
        ];
    }

    public function headings(): array
    {
        return [
           ['NO','KODE BARANG','NAMA MOTIF','HARGA PABRIK','LAMA PELUNASAN HUTANG','','','','','','','KETERANGAN'],
           [
               '','','','',
               'Cash Disc -3% 0-10 HR',
               'Harga Normal 11-60 HR',
               'Interest +1% 61-90 HR',
               'Interest +2% 91-120 HR',
               'Interest +3% 121-150 HR',
               'Interest +4% 151-180 HR',
               'Interest +5% 181-210 HR'
           ]
        ];
    }
    public function styles(Worksheet $sheet)
    {
        $sheet->mergeCells('A1:A2');
        $sheet->mergeCells('B1:B2');
        $sheet->mergeCells('C1:C2');
        $sheet->mergeCells('D1:D2');
        $sheet->mergeCells('L1:L2');
        $sheet->mergeCells('E1:K1');
        return [
            // Style the first row as bold text.
            1 => [
                    'font' => ['bold' => true,'color'=>array('rgb' => '0066cc')],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                    // 'fill' => [
                    //     'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                    //     'rotation' => 90,
                    //     'startColor' => [
                    //         'argb' => '0066cc',
                    //     ],
                    //     'endColor' => [
                    //         'argb' => '0d61c2',
                    //     ],
                    // ],
                ],
            2 => [
                    'font' => ['bold' => true],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ],
            // // Styling a specific cell by coordinate.
            // 'B2' => ['font' => ['italic' => true]],

            // // Styling an entire column.
            // 'C'  => ['font' => ['size' => 16]],
        ];
    }
}
