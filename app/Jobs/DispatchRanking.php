<?php

namespace App\Jobs;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use App\Models\Acura\Brand;
use App\Models\Acura\BarangPenjualan;
use App\Models\Acura\BarangRetur;
use App\Models\Acura\Depo;
use App\Models\Acura\ItemUmur;
use App\Models\Acura\BarangTransaksi;
use App\Models\Acura\BarangUmur;
use App\Models\Acura\ItemStatus;
use App\Models\Acura\TmpRppb;
use App\Models\Acura\TmpStok;
use App\Models\Acura\ItemStok;
use App\Models\ReportRanking;
use App\Models\LeadTime;

class DispatchRanking implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $tglParsing = '2019-04-01';
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // echo "::::::: start Job ::::::::: ".PHP_EOL;
        // $begin = memory_get_usage();
        // $this->parsing_data_ranking();
        // echo 'Total memory usage : '.(memory_get_usage() - $begin).PHP_EOL;
        // echo "::::::: End Job ::::::::: ".PHP_EOL;
    }
   protected function parsing_data_ranking()
    {
        TmpRppb::truncate();
        TmpStok::truncate();

        $date = $this->tglParsing;
        $rStart = Carbon::parse($date,'Asia/Jakarta')->sub('6 month')->toDateTimeString();
        $rEnd = Carbon::parse($rStart,'Asia/Jakarta')->add('5 month')->toDateTimeString();
       
        // penjualan 6 bulan kebelakang
        $penjualan = BarangPenjualan::select(['id_depo','kode_barang','kw_barang','jumlah_kirim','tgl_jual','kode_sj'])
                                    ->whereBetween('tgl_jual',[$rStart,$rEnd])
                                    ->orderBy('jumlah_kirim','desc');
        if($penjualan)
        {
            try {
                $pj = $this->proses_penjualan($penjualan);
                if($pj)
                {
                    $x = $this->prosess_stok();
                    if($x)
                    {
                        $this->create_r_ranking();
                    }
                }
            } catch (Exception $e) {
                // report($e);
                dd($e->getMessages);
                // echo "Error";
            }
        }else{
            dd('data kosong tidak ada');
        }
        
        
    }

    private function create_r_ranking($date)
    {
            $G = []; $P = []; $H = []; $M = []; $Q = []; $X =[]; $O =[]; $K =[]; $T = [];
            foreach(TmpStok::get() as $f)
            {
                echo substr($f->kode_barang,0,1).';';
                switch (substr($f->kode_barang,0,1)) {
                    case 'P':
                        $x = array_merge($f->toArray(),['brand'=>'Picasso']);
                        array_push($P,$x);
                        break;
                    case 'G':
                        $x = array_merge($f->toArray(),['brand'=>'Grand Atena']);
                        array_push($G,$x);
                        break;
                    case 'H':
                        $x = array_merge($f->toArray(),['brand'=>'Harmoni']);
                        array_push($H,$x);
                        break;
                    case 'M':
                        $x = array_merge($f->toArray(),['brand'=>'Mustika']);
                        array_push($M,$x);
                        break;
                    case 'Q':
                        $x = array_merge($f->toArray(),['brand'=>'Granit I']);
                        array_push($Q,$x);
                        break;
                    case 'X':
                            $x = array_merge($f->toArray(),['brand'=>'Granit II']);
                            array_push($X,$x);
                            break;
                    case 'O':
                            $x = array_merge($f->toArray(),['brand'=>'Massimo']);
                            array_push($O,$x);
                            break;
                    case 'K':
                            $x = array_merge($f->toArray(),['brand'=>'Kita']);
                            array_push($K,$x);
                            break;
                    case 'T':
                            $x = array_merge($f->toArray(),['brand'=>'Atena']);
                            array_push($T,$x);
                            break;
                    
                }
            }
            // $fileName = date('YmdHis',strtotime('now'));
            // $dJson = compact('G','P','H','M','Q','X','O','K','T');
            // $save = new ReportRanking;
            // $save->tanggal = $this->tglParsing;
            // $save->data_ranking = json_encode($dJson);
            // $save->url = '/public/data_ranking/data_rppb/'.$fileName.'.json';
            // $save->save();

            // $this->save_to_file($fileName,$dJson,'data_rppb');
    }

    private function prosess_stok()
    {
        $q = "id_depo, kode_barang, sum(net_jual) as jual, umur, tgl_lahir, sum(stk_depo) as stk_depo, discontinue";
        $rppbs = TmpRppb::selectRaw($q)
                        ->where('discontinue', '=', 'Y')
                        ->where('tgl_lahir','!=','1972-01-01')
			            ->groupBy('umur')
                        ->groupBy('kode_barang')
                        ->groupBy('id_depo');

        return $rppbs->chunk(200,function($d){
                foreach($d as $i => $x)
                {
                    echo 'stokjual: '.$x->stk_depo.'=>'.$x->jual.';';
                    // langsung grouping by depo dan kode barang
                    $kode_barang = $this->replace_char($x->kode_barang);

                    $stok = ItemStok::where('kode_item','=',$kode_barang)->first(); // table item stok

                    $item = TmpStok::firstOrNew(array(
                                                'id_depo' => $x->id_depo,
                                                'kode_barang'=>$kode_barang
                                            ));
                    $rppb = $this->cari_rppb($x->umur,$x->jual);
                    $item->id_depo = $x->id_depo;
                    $item->kode_barang = $kode_barang;
                    $item->umur = $x->umur;
                    $item->tgl_lahir = $x->tgl_lahir;
                    $item->kw = 1;
                    $item->stok = $stok?$stok->jumlah_stok:0;
                    $item->stok_depo = (int)$item->stok_depo + (int)$x->stk_depo;
                    $item->jual = (int)$item->jual + (int)$x->jual;
                    $item->rppb = (int)$item->rppb + (int)$rppb;
                    $item->lama_kirim + $this->cari_lama_kirim($x->jual,$rppb,$x->id_depo);
                    $item->save();
                }; // en loop
        }); // end chunk
    }

    private function proses_penjualan($penjualan)
    {
        return $penjualan->chunk(200,function($pen){
                foreach ($pen as $jual) {
                    echo $jual->id_depo.'<->'.$jual->kode_barang;
                    // cari retur, kurangin jual dengan retur
                    $ret = BarangRetur::selectRaw('jumlah_retur')->where([
                        'id_depo'=>$jual->id_depo,
                        'kode_sj'=>$jual->kode_sj,
                        'kode_barang'=>$jual->kode_barang,
                    ])->first();

                    $stk_depo = BarangTransaksi::selectRaw('sum(jumlah_trans) as stk_depo')
                    ->where([
                        'id_depo'=>$jual->id_depo,
                        'kode_barang'=>$jual->kode_barang,
                    ])->get()->toArray();
                    
                    if(!$stk_depo)
                    {
                        $stk_depo = [['stk_depo'=>00]];
                    }

                    if($ret)
                    {
                        $jual->jumlah_kirim = $jual->jumlah_kirim - $ret->jumlah_retur; 
                    }

                    // cari umur
                    $umur = ItemUmur::where([
                        'kode_item'=>$this->replace_char($jual->kode_barang),
                    ])->first();
                    if(!$umur)
                    {
                         $umur = (object)['masuk_awal'=>'1972-01-01'];
                        //  dump($umur->masuk_awal);
                    }
                    $discon = 'ngga ada data discontinue';
                    $statDiscontinue = ItemStatus::where('kode_item','=',$this->replace_char($jual->kode_barang))->first();
                    if($statDiscontinue)
                    {
                        $discon = $statDiscontinue->status_item;
                    }

                    $save = new TmpRppb;
                        $save->id_depo = $jual->id_depo;
                        $save->kode_barang = $jual->kode_barang;
                        $save->kode_sj = $jual->kode_sj;
                        $save->kw = $jual->kw_barang;
                        $save->tgl_jual = Carbon::parse($jual->tgl_jual);
                        $save->umur = $this->cari_umur($umur->masuk_awal); // umur bukan detail!!!
                        $save->tgl_lahir = $umur->masuk_awal;
                        $save->net_jual = $jual->jumlah_kirim;
                        $save->rppb = 0;
                        $save->discontinue = $discon;
                        $save->stk_depo = $stk_depo[0]["stk_depo"];
                        $save->save();
                }
                // $fileParse = collect($fileParse)->sort()->reverse()->take(50);
                // $this->save_to_file('tesparsing-'.date('Ymdhis',strtotime('now')),$fileParse,'json');
            });
    }
    
    private function replace_char(string $str)
    {
        $search =  '!"#$%&/()=?*+\'-,;:_><^' ;
        $search = str_split($search);
        return \substr(str_replace($search, ".", $str),0,10);
    }

    private function save_to_file(string $name,array $json,string $foldername)
    {
         Storage::disk('local')->put('public/data_ranking/'.$foldername.'/'.$name.'.json',json_encode($json,true));
    }
    private function cari_umur($awal)
    {
       return Carbon::parse('now','Asia/Jakarta')->floatDiffInMonths($awal);
    }
    private function cari_rppb($umur,$jual)
    {
            // rppb = pejualan total 6 bulan/bulan efective
            // cari rppb 
            if($umur >= 6 )
            {
                $rppb = $jual/6;
            }else{
                $rppb = $jual/$umur;
            }
            return $rppb;
    }
    private function cari_lama_kirim($jual,$rppb,$depo)
    {
        if($jual == 0.0 && $rppb == 0.0)
        {
            return 0;
        }

        $bln = LeadTime::where(['depo'=>$depo])->first();
        if($bln)
        {
            return ($jual/$rppb)*$bln->date_limit;
        }else{
            return ($jual/$rppb)*0;
        }
        
    }

}
