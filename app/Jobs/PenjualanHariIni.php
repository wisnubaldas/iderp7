<?php

namespace App\Jobs;

use App\Helpers\MyHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Carbon\Carbon;
use App\Models\Report\BarangPenjualan;
use App\Models\Report\PenjualanPerhari;

class PenjualanHariIni implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->get_penjualan();
    }
    private function get_penjualan()
    {
        $tgl = Carbon::now('Asia/Jakarta');
        $s = $tgl->add(-1,'day')->format('Y-m-d');
        $l = BarangPenjualan::where('tgl_jual',$s)->get();
        $penjualan = [];
        foreach ($l as $v) {
            $id_depo = $v->id_depo;
            $kode_barang = $v->kode_barang;
            $kw = $v->kw_barang;
            $jual = $v->jumlah_kirim;
            array_push($penjualan,compact('id_depo','kode_barang','kw','jual'));
        }
        $penjualan = collect($penjualan);
        $save = new PenjualanPerhari;
        $save->tgl = $s;
        $save->total = $penjualan->sum('jual');
        $save->data = $penjualan->toJson();
        $save->created_at = $tgl;
        $save->updated_at = $tgl;
        $save->save();
    }
    public function get_pen()
    {
        $x = PenjualanPerhari::first();
        dump(json_decode($x->data));
    }
}
