<?php

namespace App\View\Components\Survey;

use Illuminate\View\Component;

class ReportForm extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $dataInclude;
    public function __construct($dataInclude)
    {
        $this->dataInclude = $dataInclude;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.survey.report-form');
    }
}
