<?php

namespace App\View\Components\Survey;

use Illuminate\View\Component;

class ModalWarna extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $warna_customer;
    public $warnanya;
    public function __construct($customer,$warna)
    {
        $this->warna_customer = $customer;
        $this->warnanya = $warna;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.survey.modal-warna');
    }
}
