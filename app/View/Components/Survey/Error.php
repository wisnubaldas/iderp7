<?php

namespace App\View\Components\Survey;

use Illuminate\View\Component;

class Error extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $err;
    public function __construct($err)
    {
        $this->err = $err;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return <<<'blade'
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Error...!</h4>
            <ul class="list-inline">
                @foreach ($err as $error)
                    <li class="list-inline-item"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                     {{ $error }};</li>
                @endforeach
            </ul>
        </div>
        @push('script')
            <script>
                jQuery(function(){
                    setTimeout(function(){
                        $('.alert-danger').fadeOut(1000)
                    },4000)
                })
            </script>
        @endpush
blade;
    }
}
