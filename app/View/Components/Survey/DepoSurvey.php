<?php

namespace App\View\Components\Survey;

use Illuminate\View\Component;
use App\Helpers\Survey as SurveyHelper;
class DepoSurvey extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $data_form;
    public $warna;
    public function __construct($data,$warna)
    {
        $this->get_warna($warna);
        $this->get_data($data);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.survey.depo-survey');
    }

    public function get_data($data)
    {
        $result = [];
       foreach($data as $d)
       {
            $form_id = $d['form_id'];
            $sales = $d['sales'];
            $harga_depo = $d['harga_depo'];
            $cust = [];
            foreach($d['customer'] as $cus)
            {
                $cus_cus = SurveyHelper::toko($cus['id']);
                $id_cus = $cus_cus->id;
                $name_cus = $cus_cus->nama;
                array_push($cust,compact('id_cus','name_cus'));
            }
            array_push($result,compact('form_id','sales','harga_depo','cust'));
       }
       $this->data_form = collect($result);
    }
    protected function get_warna($warna)
    {
        $x = explode(',',$warna);
        switch (count($x)) {
            case 1:
                array_push($x,'','','');
              break;
            case 2:
                array_push($x,'','');
              break;
            case 3:
                array_push($x,'','');
              break;
                $x = ['','','','',''];
            default:
          } 
        $this->warna = $x;
    }
}
