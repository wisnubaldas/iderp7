<?php

namespace App\View\Components\Survey;

use Illuminate\View\Component;

class SummaryToko extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $hasil;
    public function __construct($hasil)
    {
        $this->hasil = $hasil;
        // $this->make_tabel($dataSurvey);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function make_tabel($data)
    {
        // foreach ($data as $key => $value) {
        //         print_r($value);
        // }
    }
    public function render()
    {

        return view('components.survey.summary-toko');
    }
}
