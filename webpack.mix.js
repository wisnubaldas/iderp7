const mix = require('laravel-mix');
require('laravel-mix-js-partial');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('resources/assets','public/')
mix.js('resources/js/app.js', 'public/js')
   // .sass('resources/sass/app.scss', 'public/css');
mix.disableNotifications();

// blog asset
mix.less('resources/blog_asset/less/blog/style.less','public/blog/style_modif.css');
mix.styles([
   'resources/blog_asset/plugins/bootstrap3/css/bootstrap.css',
   'resources/blog_asset/plugins/font-awesome/css/font-awesome.css',
   'resources/blog_asset/plugins/animate/animate.css',
   // 'resources/blog_asset/css/blog/style.css',
   // 'resources/blog_asset/css/blog/style-responsive.min.css',
],'public/blog/style.css');
mix.copy('resources/blog_asset/plugins/pace/pace.min.js','public/blog/js/pace.js');
mix.copy('resources/blog_asset/plugins/jquery/jquery-3.2.1.min.js','public/blog/js/jquery-3.2.1.min.js');
mix.copy('resources/blog_asset/plugins/bootstrap3/js/bootstrap.min.js','public/blog/js/bootstrap.min.js');
mix.copy('resources/blog_asset/plugins/js-cookie/js.cookie.js','public/blog/js/js.cookie.js');
mix.copy('resources/blog_asset/js/blog/apps.min.js','public/blog/js/main.js');
// copy image blog
mix.copy('resources/blog_asset/img','public/blog/img');
mix.copy('resources/blog_asset/plugins/font-awesome/fonts','public/blog/fonts');
mix.copy('resources/blog_asset/plugins/bootstrap3/css/bootstrap.css.map','public/blog/bootstrap.css.map');
mix.copy('resources/blog_asset/plugins','public/blog/plugins');

mix.setPublicPath('public/resources').jsPartial('resources/views/parsial_script/survey.js', 'resources/views/parsial_script/survey.blade.php');
mix.setPublicPath('public/resources').jsPartial('resources/views/parsial_script/system.js', 'resources/views/parsial_script/system.blade.php');

// mix.browserSync('192.168.10.10:8080');