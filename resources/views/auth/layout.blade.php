<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'IDERP') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}" />
    <link href="{!! asset('font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('css/handsontable.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/animate.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/style.css') !!}" />
</head>

<body class="gray-bg">
    <div id="app">
        <div class="loginColumns animated fadeInDown">
            <div class="row">
                @if ($agent->isDesktop())
                    <div class="col-md-6">
                        <h2 class="font-bold">Welcome to CJFI</h2>
                        <p>
                            Menjadi perusahaan yang baik, terpercaya, dan berkembang.
                        </p>
                        <p>
                            Selalu berkomitmen meningkatkan kualitas serta desain produk - produk yang inovatif.
                            Secara konsisten melakukan pengendalian standar spesifikasi yang didukung oleh Sumber Daya
                            Manusia bermutu, serta perangkat Teknologi Informasi modern.
                        </p>
                        <p>
                            Meningkatkan dan menciptakan kesejahteraan serta lingkungan kerja yang sehat, aman dan harmonis.
                            Menerapkan sistem ISO 9001 secara menyeluruh untuk meningkatkan kualitas dan kuantitas produk
                            secara konsisten dan berkesinambungan untuk mencapai kepuasan pelanggan.
                        </p>
                    </div>
                @endif
                <div class="col-md-6">
                    <div class="ibox-content">
                        @yield('content')
                        <p class="m-t">
                            <small>IDERP report application and data process &copy; 2019</small>
                        </p>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-6">
                    PT. Chang Jui Fang Indonesia (CJFI)
                </div>
                <div class="col-md-6 text-right">
                    <small>© 2014-2019</small>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
