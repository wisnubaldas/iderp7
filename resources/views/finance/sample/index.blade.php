@extends('backend.app')

@section('content')
<div class="col-lg-12">
    <div class="ibox ">
        <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-3 b-r">
                            <div class="form-group">
                                <label>Cari SJ (gunakan tanda koma untuk pemisah)</label>
                                <span class="input-group-append"> 
                                    <button type="button" class="btn btn-primary btn-block" id="submit">Go!</button> 
                                </span>
                                <div class="input-group">
                                    <textarea type="text"  class="form-control text-uppercase" name="sj" id="sj" placeholder="SJ-21030347,SJ-21030348,SJ-21030349,....."></textarea> 
                                </div>
                                
                            </div>
                    </div>
                    <div class="col-sm-9">
                        <div id="kontent"></div>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection

@push('before-script')
    
@endpush
@push('script')
<script>
    function escapeHtml(text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };
        
        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }
    let SJ = {
        postData:function(data)
        {
            const dataSet = data.dataset.sample.split(',')
            axios.post('/finance/sample/post_data', dataSet)
                .then(function (response) {
                    console.log(response.data);
                })
                .catch(function (error) {
                    console.log(error);
                });
                
        },
        table:function(tbody){
            
            return `<div class="ibox ">
                    <div class="ibox-title">
                        <h5>Striped Table </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <table class="table table-striped" id="tbl">
                            <thead>
                            <tr>
                                <th>Nomer SJ</th>
                                <th>Kode Barang</th>
                                <th>Price</th>
                                <th>QTY</th>
                                <th>Agen</th>
                                <th>Official/Sample</th>
                            </tr>
                            </thead>
                            <tbody>
                                ${tbody.join('')}
                            </tbody>
                        </table>
                    </div>
            </div>`;
        },
        tpl:function(data)
        {
            return data.map(function(a) {
                const id = Date.now()+a.id
                let x;
                if(a.status == 'Y')
                {
                    x = `<div class="switch">
                            <div class="onoffswitch">
                                <input type="checkbox" onChange="handleChange(this)" checked class="onoffswitch-checkbox" id="${id}" data-sample="${a.id},${a.sj},${a.kd_barang},${a.status}">
                                <label class="onoffswitch-label" for="${id}">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>`;
                }else{
                    x = `<div class="switch">
                            <div class="onoffswitch">
                                <input type="checkbox" onChange="handleChange(this)" class="onoffswitch-checkbox" id="${id}" data-sample="${a.id},${a.sj},${a.kd_barang},${a.status}">
                                <label class="onoffswitch-label" for="${id}">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>`;
                }
                return `<tr id="${a.id}"><td>${a.sj}</td><td>${a.kd_barang}</td><td>${a.price}</td><td>${a.qty}</td><td>${a.agen}</td><td>${x}</td></tr>`;                
            })
        },
        parse:function(data){
            // console.log(data)
            const dataSJ = data.map(function(a){
                const r = {
                    id:Math.floor(Math.random() * 1000) + 1,
                    sj:a.JHDH_C.replace(/ /g,''),
                    kd_barang:escapeHtml(a.LPDH_C.replace(/ /g,'')),
                    status:a.KDDM_C.replace(/ /g,''),
                    price:a.JHJE_N.replace(/ /g,''),
                    qty:a.JHSL_N.replace(/ /g,''),
                    agen:a.KHDH_C.replace(/ /g,'')
                }
               return r;
            })
            return SJ.table(SJ.tpl(dataSJ))
        }
    }
    $('#submit').on('click',function(a){
        a.preventDefault();
        const sj = $('#sj').val();

        axios.get('http://192.168.0.201:9090/SjSample/get?sj='+sj)
                .then(function(response){
                    $('#kontent').html(SJ.parse(response.data));
                })
                .catch(function(error){
                    console.error(error);
                })
    })

    function handleChange(a)
    {
        SJ.postData(a)
    }
</script>
@endpush
@push('css')

@endpush