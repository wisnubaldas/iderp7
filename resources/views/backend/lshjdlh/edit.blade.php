@extends('backend.app')
@section('content')
<div class="row">
    <div class="col-sm-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5 class="text-navy">Laporan Simulasi Harga Jual Dengan Lama Hutang</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {{-- @dump($editData->id) --}}
                    <form action="{{url('report/lshjadlh/update',$editData->id)}}" method="POST" class="form">
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label>Kode Barang</label>
                                <input type="text" class="form-control form-control-sm text-uppercase" value="{{$editData->kd_barang}}"  name="kd_barang" id="kd_barang">
                                <label>Motif</label>
                                <input type="text" class="form-control form-control-sm text-uppercase" value="{{$editData->motif}}" name="motif" id="motif">
                                <label>Harga Pabrik</label>
                                <input type="text" class="form-control form-control-sm text-uppercase" value="{{$editData->harga_pabrik}}" name="harga_pabrik" id="harga_pabrik">
                            </div>
                            <div class="col-lg-4">
                                <label>Harga Normal</label>
                                <input type="text" class="form-control form-control-sm text-uppercase" oninput="get_value(this,'harga_normal')" value="{{$editData->harga_normal}}" name="harga_normal" id="harga_normal">
                                <label>Keterangan</label>
                                <input type="text" class="form-control form-control-sm text-uppercase" value="{{$editData->keterangan}}" name="keterangan" id="keterangan">
                           <br>
                                <input type="text" hidden class="form-control form-control-sm text-uppercase" name="disc" id="disc">
                            </div>
                            <div class="col-lg-8 text-right">
                                <br>
                                <button class="btn btn-outline btn-primary" type="submit" >
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    Save
                                </button>
                            </div>
                        </div>
                        @csrf
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    function get_value(input,id)
    {
        // console.log(input.value)
        // var x = document.getElementById("myInput").value;
        document.getElementById(id).value = input.value;
        if(id === 'harga_normal')
        {
            rumus_potongan(input.value)
        }
    }
    
    function rumus_potongan(harga)
    {
        const htg = parseInt(harga)
        if(htg)
        {
            let pt = {}
            pt.disc3 = function(htg)
            {
                return (htg-(htg*3/100));
            }
            pt.bunga1 = function(htg)
            {
                return (htg+(htg*1/100));
            }
            pt.bunga2 = function(htg)
            {
                return (htg+(htg*2/100));
            }
            pt.bunga3 = function(htg)
            {
                return (htg+(htg*3/100));
            }
            pt.bunga4 = function(htg)
            {
                return (htg+(htg*4/100));
            }
            pt.bunga5 = function(htg)
            {
                return (htg+(htg*5/100));
            }
            pt.init = function(htg)
            {
                return {
                    discount:pt.disc3(htg),
                    bunga_1:pt.bunga1(htg),
                    bunga_2:pt.bunga2(htg),
                    bunga_3:pt.bunga3(htg),
                    bunga_4:pt.bunga4(htg),
                    bunga_5:pt.bunga5(htg)
                }
            }
            // console.log(pt.init(htg))
            const hargaDiscount = pt.init(htg);
            document.getElementById('disc').value = JSON.stringify(hargaDiscount);
            console.log(hargaDiscount)
        }
        
    }
</script>
@endpush
@push('css')

@endpush
