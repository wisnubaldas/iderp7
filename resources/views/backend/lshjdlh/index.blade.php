@extends('backend.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5 class="text-navy">Laporan Simulasi Harga Jual Dengan Lama Hutang</h5>
                    <div class="ibox-tools">
                        
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="btn-group">
                        <button id="select-all" class="btn btn-info" >Select All</button>
                        <button id="print" class="btn btn-primary btn-outline" >Expot Excel</button>
                        <button id="print" class="btn btn-danger btn-outline" >Expot PDF</button>
                        <div class="custom-file">
                            <input id="logo" type="file" class="custom-file-input">
                            <label for="logo" class="custom-file-label">Choose file...</label>
                        </div> 
                    </div>
                    
                    <table class="table table-hover table-bordered" id="tbl-harga">
                        <thead class="text-center align-middle">
                            <tr>
                                <th rowspan="2">#</th>
                                <th rowspan="2" class="align-middle">Kode Barang</th>
                                <th rowspan="2" class="align-middle">Nama Motif</th>
                                <th rowspan="2" class="align-middle">Harga Pabrik</th>
                                <th colspan="7">Lama Pelunasan Hutang</th>
                                <th rowspan="2" class="align-middle">Keterangan</th>
                                <th rowspan="2" class="align-middle">#</th>
                            </tr>
                            <tr>
                                <th>Cash Disc -3%<br>0-10 HR</th>
                                <th>Harga Normal<br>11-60 HR</th>
                                <th>Interest +1%<br>61-90 HR</th>
                                <th>Interest +2%<br>91-120 HR</th>
                                <th>Interest +3%<br>121-150 HR</th>
                                <th>Interest +4%<br>151-180 HR</th>
                                <th>Interest +5%<br>181-210 HR</th>
                            </tr>
                            <tr>
                                <td>
                                    
                                </td>
                                <td><div class="form-group has-success"><input class="form-control form-control-sm text-uppercase" oninput="get_value(this,'kd_barang')"></div></td>
                                <td><div class="form-group has-success"><input class="form-control form-control-sm text-uppercase" oninput="get_value(this,'motif')"></div></td>
                                <td><div class="form-group has-success"><input class="form-control form-control-sm" oninput="get_value(this,'harga_pabrik')"></div></td>
                                <td id="discount">0</td>
                                <td><div class="form-group has-success"><input class="form-control form-control-sm" oninput="get_value(this,'harga_normal')"></div></td>
                                <td id="bunga_1">0</td>
                                <td id="bunga_2">0</td>
                                <td id="bunga_3">0</td>
                                <td id="bunga_4">0</td>
                                <td id="bunga_5">0</td>
                                <td>
                                        <div class="form-group has-success">
                                            <textarea class="form-control form-control-sm" oninput="get_value(this,'keterangan')" type='text'></textarea>
                                        </div>
                                   
                                </td>
                                <td>
                                    <form action="{{url('report/lshjadlh/save')}}" method="POST" class="form">
                                        @csrf
                                        
                                        <input type="text" hidden name="kd_barang" id="kd_barang">
                                        <input type="text" hidden name="motif" id="motif">
                                        <input type="text" hidden name="harga_pabrik" id="harga_pabrik">
                                        <input type="text" hidden name="harga_normal" id="harga_normal">
                                        <input type="text" hidden name="keterangan" id="keterangan">
                                        <input type="text" hidden name="disc" id="disc">
                                        <button class="btn btn-outline btn-primary" type="submit" >
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                                <th style="padding: 15px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                                
                        </tbody>
                    </table>

                </div>
        </div>
    </div>
</div>
{{-- laert error --}}


@endsection

@push('before-script')
{{-- <script src="{{asset('js/plugins/pdfjs/pdf.js')}}"></script> --}}
<script src="{{asset('js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>


<script>
    function get_value(input,id)
    {
        // console.log(input.value)
        // var x = document.getElementById("myInput").value;
        document.getElementById(id).value = input.value;
        if(id === 'harga_normal')
        {
            rumus_potongan(input.value)
        }
    }
    
    function rumus_potongan(harga)
    {
        const htg = parseInt(harga)
        if(htg)
        {
            let pt = {}
            pt.disc3 = function(htg)
            {
                return (htg-(htg*3/100));
            }
            pt.bunga1 = function(htg)
            {
                return (htg+(htg*1/100));
            }
            pt.bunga2 = function(htg)
            {
                return (htg+(htg*2/100));
            }
            pt.bunga3 = function(htg)
            {
                return (htg+(htg*3/100));
            }
            pt.bunga4 = function(htg)
            {
                return (htg+(htg*4/100));
            }
            pt.bunga5 = function(htg)
            {
                return (htg+(htg*5/100));
            }
            pt.init = function(htg)
            {
                return {
                    discount:pt.disc3(htg),
                    bunga_1:pt.bunga1(htg),
                    bunga_2:pt.bunga2(htg),
                    bunga_3:pt.bunga3(htg),
                    bunga_4:pt.bunga4(htg),
                    bunga_5:pt.bunga5(htg)
                }
            }
            // console.log(pt.init(htg))
            const hargaDiscount = pt.init(htg);
            document.getElementById('disc').value = JSON.stringify(hargaDiscount);
            ['discount','bunga_1','bunga_2','bunga_3','bunga_4','bunga_5'].map(function(a){
                
                if(hargaDiscount[a])
                {
                    document.getElementById(a).innerHTML = hargaDiscount[a]
                }
            })
        }
        
    }

    jQuery(function(){
            // upload file
            $('.custom-file-input:file').on('change', function (a) {
                        // console.log(a)
                        var fileName = $(this).val().split('\\').pop();
                        $(this).next('.custom-file-label').addClass("selected").html(fileName);
                        var formData = new FormData();
                        var imagefile = document.querySelector('#logo');
                        formData.append("file", imagefile.files[0]);
                        
                        axios.post('/report/lshjadlh/upload', formData, {
                            headers: {
                            'Content-Type': 'multipart/form-data'
                            }
                        }).then(function (response) {
                            console.log(response)
                            alert('data sukses di upload')
                        })["catch"](function (error) {
                            alert('gagal upload file')
                            // console.log(error);
                        });
                        
                    });

            let table = $('#tbl-harga').DataTable({
                    ajax: '/report/lshjadlh/grid',
                    columns: [
                        { data: 'id',name:'id'},
                        { data: 'kd_barang',name:'kd_barang'},
                        { data: 'motif',name:'motif'},
                        { data: 'harga_pabrik',name: 'harga_pabrik' },
                        { data: 'discount',name: 'discount' },
                        { data: 'harga_normal',name: 'harga_normal' },
                        { data: 'bunga_1',name: 'bunga_1' },
                        { data: 'bunga_2',name: 'bunga_2' },
                        { data: 'bunga_3',name: 'bunga_3' },
                        { data: 'bunga_4',name: 'bunga_4' },
                        { data: 'bunga_5',name: 'bunga_5' },
                        { data: 'keterangan',name: 'keterangan' },
                        { data: 'action',name: 'action' },
                    ],
                    "columnDefs": [
                        { className: "text-right", "targets": [3,4,5,6,7,8,9,10] }
                    ],
                    processing: true,
                    serverSide: true,
                    pageLength: 100,
                    // paging: false,
                    lengthChange: false,
                    searching:true,
                    responsive: true,
                    select: true
            });

            // $('#tbl-harga tbody').on( 'click', 'tr', function () {
            //     if ( $(this).hasClass('bg-primary') ) {
            //         $(this).removeClass('bg-primary');
            //     }
            //     else {
            //         table.$('tr.bg-primary').removeClass('bg-primary');
            //         $(this).addClass('bg-primary');
            //     }
            // } );

            $('#tbl-harga tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('bg-primary');
            } );
            
            $('#select-all').on( 'click', function () {
                $('#tbl-harga tbody tr').toggleClass('bg-primary');
            } );
            
            $('#print').click( function () {
                var ids = $.map(table.rows('.bg-primary').data(), function (item) {
                    // console.log(item)
                    return item.id
                });
                // console.log(ids)
                if(ids.length == 0)
                {
                    alert('select dulu row nya yang mau di esport');
                }else{
                        let formData = new FormData();
                        formData.append("ids", ids);
                        axios.post('/report/lshjadlh/export', formData, {
                            // headers: {
                            // 'Content-Type': 'multipart/form-data'
                            // }
                        }).then(function (response) {
                            console.log(response.data)
                            return window.open(response.data.link, "_blank"); 
                        })["catch"](function (error) {
                            // alert('gagal upload file')
                            console.log(error);
                        });
                }
                // alert(table.rows('.bg-primary').data().length + ' row(s) selected');
            } );
            
        })
    
</script>


@endpush
@push('css')
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endpush
