@extends('backend.app')

@section('content')
    <merek-component></merek-component>
@endsection

@push('script')
<script>

</script>
<!-- Input Mask-->
<script src="{{asset('js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>
@endpush
@push('css')
<link href="{{asset('css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
@endpush