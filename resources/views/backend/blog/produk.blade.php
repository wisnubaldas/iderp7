@extends('backend.app')

@section('content')
    <produk-component/>
@endsection

@push('script')

<!-- Input Mask-->
<script src="{{asset('js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/select2/select2.full.min.js')}}"></script>
<script>
    jQuery(function() {
  $(".custom-file-input").on("change", function() {
    let fileName = $(this)
      .val()
      .split("\\")
      .pop();
    $(this)
      .next(".custom-file-label")
      .addClass("selected")
      .html(fileName);
  });
    let arr = [];
    let data = @json($brand);
        data = data.map((a)=>{
            x = new Object;
            x.id = a.id;
            x.text = a.nama;
            const z = `<option value="${a.id}">${a.nama}</option>`;
            arr.push(z);
        })
    
  $('.brands').append(arr.join(''));      
//   $(".select2_demo_1").select2({data:arr});
});
</script>
@endpush
@push('css')
<link href="{{asset('css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/select2/select2.min.css')}}" rel="stylesheet">
@endpush