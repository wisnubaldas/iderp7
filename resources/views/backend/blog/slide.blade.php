@extends('backend.app')

@section('content')
    <div class="row">
        <div class="col-md-6">
          <div class="ibox">
            <div class="ibox-title">
              <h5>Create Slide</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('slide.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Title Indonesia</label>
                                <input type="text" class="form-control" name="slide_text_id"/>
                                <span class="form-text m-b-none text-danger"></span>
                            </div>
                            <div class="form-group">
                                <label>Keterangan Indonesia</label>
                                <textarea type="text" class="form-control" name="slide_desc_id"></textarea>
                                <span class="form-text m-b-none text-danger"></span>
                            </div>
                            <div class="form-group">
                                <label>Image</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input id="inputGroupFile01" type="file" class="custom-file-input" name="slide_img">
                                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Title English</label>
                                <input type="text" class="form-control" name="slide_text_en"/>
                                <span class="form-text m-b-none text-danger"></span>
                            </div>
                            <div class="form-group">
                                <label>Description English</label>
                                <textarea type="text" class="form-control" name="slide_desc_en"></textarea>
                                <span class="form-text m-b-none text-danger"></span>
                            </div>
                            <div class="form-group">    
                                <label>Link</label>
                                <input type="text" class="form-control" name="slide_link" value="#"/>
                                <span class="form-text m-b-none text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                        </div>
                    </div>
                    
                </form>
                
            </div>
          </div>
        </div> <!-- end col-8 -->
        <div class="col-md-6">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <h3>Error...;</h3>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="ibox-content">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($slide->sort() as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->slide_text_en}}</td>
                                <td>{{$item->slide_img}}</td>
                                <td>
                                    <a id="" class="btn btn-danger btn-sm deleteSlide" href="{{route('slide.edit',$item->id)}}" >Delete</a>
                                </td>
                            </tr>
                        @endforeach                        
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
@endsection

@push('script')
<script>
    $('.deleteSlide').click(function (a) {
        a.preventDefault()
        const url = $(this).attr('href');
    swal({
        title: "Hapus data?",
        text: "Hati-hati data akan di hapus permanen....",
        type: "warning",
        showCancelButton: true,
        cancelButtonColor:"#DD6199",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        document.location = url
        swal("Deleted!", url, "success");
    });
});
</script>
<script src="{{asset('js/plugins/sweetalert/sweetalert.min.js')}}"></script>
@endpush
@push('css')
<link href="{{asset('css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
@endpush