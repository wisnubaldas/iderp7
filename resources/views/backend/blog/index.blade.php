@extends('backend.app')

@section('content')
    <blog-component></blog-component>
@endsection

@push('script')
<!-- Input Mask-->
<script src="{{asset('js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>
@endpush
@push('css')
<link href="{{asset('css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
@endpush