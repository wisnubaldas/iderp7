@extends('backend.app')

@section('content')
    <barang-component/>
    
@endsection

@push('before-script')
    <script>
      const connate = @json($connate);
    </script>
@endpush
@push('script')

<!-- Input Mask-->
<script src="{{asset('js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/select2/select2.full.min.js')}}"></script>
<script>
    jQuery(function() {
      // $(".custom-file-input").on("change", function() {
      //   let fileName = $(this)
      //     .val()
      //     .split("\\")
      //     .pop();
      //   $(this)
      //     .next(".custom-file-label")
      //     .addClass("selected")
      //     .html(fileName);
      // });
        
      //   const brand = _.map(connate.brand, 
      //                         function(b) {  
      //                                       return `<option vlaue=${b.id}>${b.nama.toUpperCase()}</option>`; 
      //                                     });
      //   const produk = _.map(connate.produk, 
      //                         function(b) {  
      //                                       return `<option vlaue=${b.id}>${b.title.toUpperCase()}</option>`; 
      //                                     });
      //   const type = _.map(connate.type, 
      //                         function(b) {  
      //                                       return `<option vlaue=${b.id}>${b.title.toUpperCase()} (${b.code})</option>`; 
      //                                     });  
      //   const ukuran = _.map(connate.ukuran, 
      //                         function(b) {  
      //                                       return `<option vlaue=${b.id}>${b.title.toUpperCase()}</option>`; 
      //                                     });                                          
      // $('.brands').append(brand.join(''));      
      // $('.produk').append(produk.join(''));      
      // $('.type').append(type.join(''));      
      // $('.ukuran').append(ukuran.join(''));      
//   $(".select2_demo_1").select2({data:arr});
});
</script>
@endpush
@push('css')
<link href="{{asset('css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/select2/select2.min.css')}}" rel="stylesheet">
@endpush