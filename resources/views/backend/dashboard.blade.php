@extends('backend.app')

@section('content')
<div class="col-lg-12">
    <div class="row">
        @foreach ($modul as $item)
            <div class="col-3">
                <a href="{{url('menu',$item->id)}}">
                    <div class="widget style1 navy-bg">
                        <div class="row">
                            <div class="col-4 d-none d-lg-block">
                                <i class="{{$item->icon}}"></i>
                            </div>
                            <div class="col-8 text-right">
                                <span class="d-none d-lg-block"> {{$item->ket}} </span>
                            <h2 class="font-bold">{{$item->name}}</h2>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
</div>
@endsection
@push('css')
@endpush
@push('script')

    {{-- <script>
         localforage.removeItem('menu').then(function() {
                // Run this code once the key has been removed.
                    console.log('Menu is cleared!');
                }).catch(function(err) {
                    // This code runs if there were any errors
                    console.log(err);
                });
    </script> --}}
    {{-- @if (session('menu'))
            <script>
                let mainMenu = @json(Session::get('menu'));
                localforage.setItem('menu', JSON.parse(mainMenu)).then(function (value) {
                    // Do other things once the value has been saved.
                        console.log('dasbord generate menu',value);
                    }).catch(function(err) {
                        // This code runs if there were any errors
                        console.log(err);
                    });
            </script>
    @endif --}}
@endpush