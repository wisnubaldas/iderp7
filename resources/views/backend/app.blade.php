<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('backend.layouts.head')

<body class="md-skin">
    <div id="app">
        <div id="wrapper">
            @if (!isset($apprf_data))
                @include('backend.layouts.nav')
            @endif
            
            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    @if (!isset($apprf_data))
                    @include('backend.layouts.header')
                    @endif
                </div>
                @if (isset($breadcrumb) == true)
                @include('backend.layouts.breadcrumb')
                @endif
                <div class="wrapper wrapper-content animated fadeInRight">
                    @yield('content')
                </div>
                @include('backend.layouts.footer')
            </div>
        </div>
    </div>
    @include('backend.layouts.scripts')
</body>

</html>