@extends('backend.app')

@section('content')
    <ranking-component></ranking-component>
@endsection

@push('script')
<script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/js/bootstrap-datetimepicker.min.js"></script> --}}
@endpush
@push('css')
<link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
{{-- <link href="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> --}}
<link href="{{asset('css/handsontable.full.css')}}" rel="stylesheet">
@endpush