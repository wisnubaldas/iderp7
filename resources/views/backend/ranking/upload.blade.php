@extends('layouts.template.main')

@section('title','Upload Ranking')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox ">
        <div class="ibox-content">
            <form action="{{route('store_file_ranking')}}" class="form">
                <div class="form-group">
                    <div class="custom-file">
                        <input id="logo" type="file" class="custom-file-input">
                        <label for="logo" class="custom-file-label">Choose file...</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Date Priode</label>
                    <input type="text" class="form-control " name="priode" />
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" id="" class="form-control" cols="30" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-lg btn-outline-primary" type="submit">Save Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{!! asset('js/plugins/datapicker/bootstrap-datepicker.js') !!}"></script>
<script src="{!! asset('js/plugins/chosen/chosen.jquery.js') !!}"></script>
<script src="{!! asset('js/plugins/iCheck/icheck.min.js') !!}"></script>
<script>
    jQuery(function(){
        $('.custom-file-input').on('change', function() {
            let fileName = $(this).val().split('\\').pop();
            $(this).next('.custom-file-label').addClass("selected").html(fileName);
        }); 
    });
</script>
@endsection

@section('css')
<link rel="stylesheet" href="{!! asset('css/plugins/chosen/bootstrap-chosen.css') !!}" />
<link rel="stylesheet" href="{!! asset('css/plugins/datapicker/datepicker3.css') !!}" />
<link href="{!! asset('css/plugins/iCheck/custom.css') !!}" rel="stylesheet">
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.3.200/pdf.min.js" integrity="sha256-J4Z8Fhj2MITUakMQatkqOVdtqodUlwHtQ/ey6fSsudE=" crossorigin="anonymous"></script> --}}

@endsection
