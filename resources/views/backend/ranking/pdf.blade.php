@php
    function mewarnai($rppb,$stok)
    {
        if ($stok > (3*$rppb)) {
            return 'w3-win8-lebih-banyak';
        }
        
        if (in_array($stok,range((1*$rppb),(2*$rppb)))) {
            return 'w3-win8-lebih-sedikit';
        }
        if($stok < (0.8*$rppb))
        {
            return 'w3-win8-kurang-banyak';
        }
        if($stok < (0.8*$rppb))
        {
            return 'w3-win8-cobalt';
        }
        if ($stok == 0) {
            return 'w3-win8-stok';
        }
        // ada di kolom rppb
        // if ($rppb == 0) {
        //     return 'w3-win8-rppb';
        // }
        return 'bg-white';
    }
@endphp

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="/var/www/html/iderp2/public/pdf/color-min.css" />
    
    <style type="text/css">
            body  { font-family: 'Ubuntu', sans-serif; }
            table { 
                    page-break-inside:auto; 
            }
            tr    { page-break-inside:avoid; page-break-after:auto; }
            td    { page-break-inside:avoid; page-break-after:auto; }
            thead { display:table-header-group; }
            tfoot { display:table-footer-group; }
            div.page {
                        /* overflow: hidden; */
                        page-break-after: always;
                    }
            table {
                    border-collapse: collapse;
                    width: 100%;
                    border: 1px solid black;
                }
            th, td {
                    text-align: left;
                    padding: 8px;
                    border: 1px solid black;
            }
            /* tr:nth-child(even) {background-color: #f2f2f2;} */
    </style>    

</head>
    <body class="bg-white">
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-12">
                    @for ($i = 0; $i < count($header); $i++)
                        <div class="page">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                    <tr class="success">
                                        @foreach ($header[$i] as $item)
                                            <th class="bg-black"></th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data_tabel[$i] as $ii)
                                        @if ($loop->even)
                                            <tr class="warning">
                                        @else
                                            <tr>
                                        @endif

                                        @if ($loop->index === 0)
                                            @foreach ($ii as $iii)
                                                @if ($loop->index === 1 or $loop->index === 0)
                                                    <td>{{$iii}}</td>
                                                @endif
                                                @if ($iii !== null and $loop->index > 1)
                                                    <td colspan="2">{{$iii}}</td>
                                                @endif
                                            @endforeach
                                        @endif

                                        @if ($loop->index === 1)
                                            @foreach ($ii as $iii)
                                                @if ($loop->index === 1 or $loop->index === 0)
                                                    <td>{{$iii}}</td>
                                                @endif
                                                @if ($iii !== null and $loop->index > 1)
                                                    <td colspan="2">{{$iii}}</td>
                                                @endif
                                            @endforeach
                                        @endif

                                        @if ($loop->index === 2)
                                            @foreach ($ii as $iii)
                                                @if ($iii !== null)
                                                    <td colspan="2" class="text-center">{{$iii}}</td>
                                                @endif
                                            @endforeach
                                        @endif

                                        @if ($loop->index === 3)
                                            @foreach ($ii as $iii)
                                                @if ($iii !== null)
                                                    <td colspan="2" class="text-center">{{$iii}}</td>
                                                @endif
                                            @endforeach
                                        @endif
                                        
                                        @if ($loop->index === 4 || $loop->index === 5|| $loop->index === 6 || $loop->index === 7)
                                            @foreach ($ii as $iii)
                                                @if($loop->index < 2)
                                                    <td class="bg-green">{{$iii}}</td>
                                                @else
                                                    <td class="bg-green">{{$iii}}</td>
                                                @endif
                                            @endforeach
                                        @endif
                                        
                                        {{-- belajar mewarnai --}}
                                        @if ($loop->index > 7)
                                        @php
                                         $even = null;   
                                        @endphp
                                            @foreach ($ii as $iii)
                                                    @if($loop->index < 2)
                                                        <td class="bg-silver">{{$iii}}</td>
                                                    @else
                                                        @if($loop->odd)
                                                            @php
                                                                $rppb = $iii;
                                                            @endphp
                                                            @if ($iii == 0)
                                                                <td class="w3-win8-rppb">{{$iii}}</td>
                                                            @else
                                                                <td>{{$iii}}</td>
                                                            @endif
                                                        @endif

                                                        @if($loop->even)
                                                            @php
                                                                $warna = mewarnai($rppb,$iii);
                                                            @endphp
                                                            <td class="{{$warna}}">{{$iii}}</td>
                                                        @endif
                                                    @endif
                                            @endforeach
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endfor
                </div>
            </div>
        </div>
    </body>
</html>
