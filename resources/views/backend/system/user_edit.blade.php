@extends('backend.app')

@section('content')
@php
$permissionName = $user->permissions->pluck('name');
$rolesName =  $user->roles->pluck('name');
@endphp
{{-- <user-manager></user-manager> --}}
<div class="card">
    <div class="card-header">
        <span style="font-size:18px;"> User Manager</span>
        <div class="float-right btn-group">
            <a href="{{route('user_manager')}}" class="btn btn-warning btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
            <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add User</a>
        </div>
    </div>
    <div class="card-body">
        <form class="" role="form" id="formUser" method="POST">
            @csrf
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" placeholder="Email" name="email" class="form-control form-control-sm"
                        value="{{$user->email}}">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" placeholder="Name" name="name" class="form-control form-control-sm"
                        value="{{$user->name}}">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="">Permissions</label>
                        <select name="permission[]" id="permission" class="form-control select2" multiple="multiple">
                            @foreach ($permissions as $item)
                                @if (in_array($item->name,$permissionName->toArray()))
                                    <option value="{{$item->name}}" selected>{{$item->name}}</option>
                                @else
                                    <option value="{{$item->name}}">{{$item->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Roles</label>
                        <select name="role[]" id="role" class="form-control select2" multiple="multiple">
                            @foreach ($roles as $item)
                                @if (in_array($item->name,$rolesName->toArray()))
                                    <option value="{{$item->name}}" selected>{{$item->name}}</option>
                                @else
                                    <option value="{{$item->name}}">{{$item->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group p-xs">
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>
@endsection
@push('before-script')
@include('parsial_script.system')
@endpush
@push('script')
<script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert/sweetalert.min.js')}}"></script>

{{-- <script src="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/js/bootstrap-datetimepicker.min.js"></script> --}}
<script>
    const postEdit = "/system/user_manager/save_user/{{$user->id}}";

    jQuery(function () {
        $('#formUser').on('submit', function (a) {
            a.preventDefault();
            $.ajax({
                    method: 'POST',
                    url: postEdit,
                    data: $(this).serialize(),
                }).done(function () {
                    swal("Sukses Update User {{$user->name}}", "", "warning")
                })
                .fail(function () {
                    swal("Server Errorr.....d:", "", "warning")
                })
                .always(function () {
                    // alert( "complete" );
                });
        })
    })

</script>
@endpush
@push('css')
<link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
{{-- <link href="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> --}}
{{-- <link href="{{asset('css/handsontable.full.css')}}" rel="stylesheet"> --}}
@endpush
