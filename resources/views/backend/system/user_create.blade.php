@extends('backend.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>
                <div class="card-body">
                    <form method="POST" action="/system/user_manager/insert">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="depo" class="col-md-4 col-form-label text-md-right">Jabatan</label>
                            <div class="col-md-6">
                                <select id="jabatan" type="text" class="form-control" name="jabatan" required >
                                    <option value="staff">Staff Kantor Pusat</option>
                                    <option value="operator depo">Operator Depo</option>
                                    <option value="kadep">Kepala Depo</option>
                                    <option value="rm">Manager Regional</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="depo" class="col-md-4 col-form-label text-md-right">Posisi</label>
                            <div class="col-md-6">
                                <select id="depo_id" type="text" class="form-control" name="depo_id" required autocomplete="depo_id">
                                    <option value="office">Kantor Pusat</option>
                                    <option value="pabrik">Pabrik</option>
                                    @foreach ($depo as $item)
                                        <option value="{{$item->id_depo}}">{{$item->nama_depo}}</option>
                                    @endforeach
                                </select>
                                @error('depo_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="menu" class="col-md-4 col-form-label text-md-right ">{{ __('Set Menu') }}</label>

                            <div class="col-md-6">
                                <select id="menu" type="text" class="form-control select2" name="menu[]" multiple="multiple" >
                                    @inject('menu', 'App\Models\Admin\Menu')
                                    @foreach ($menu::all() as $item)
                                        <option value="{{$item->id}}">
                                            @if ($item->parent_id == 0)
                                                <i>#</i>
                                            @endif
                                            {{$item->title}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="menu" class="col-md-4 col-form-label text-md-right ">{{ __('Set Modul') }}</label>
                            <div class="col-md-6">
                                <select id="modul" type="text" class="form-control select2" name="modul[]" multiple="multiple" >
                                    @inject('modul', 'App\Models\Admin\MenuModule')
                                    @foreach ($modul::all() as $item)
                                        <option value="{{$item->id}}">
                                            {{$item->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script src="{{asset('js/plugins/select2/select2.full.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
@endpush
@push('css')
<link href="{{asset('css/plugins/select2/select2.min.css')}}" rel="stylesheet">
@endpush