@extends('backend.app')

@section('content')
{{-- <user-manager></user-manager> --}}
    <div class="card">
        <div class="card-header">
            <span style="font-size:18px;"> User Manager</span>
            <div class="float-right">
                <a href="/system/user_manager/create_user" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add User</a>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="table-responsive">
                    <table id="tes" class="table compact table-hover">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Depo</th>
                                <th>Menu</th>
                                <th>Modul</th>
                                <th>Permission</th>
                                <th>Role</th>
                                <th>#</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/js/bootstrap-datetimepicker.min.js"></script> --}}
    <script>
        const userIndex = "{{route('user_manager')}}";
        
        jQuery(function(){
            $('#tes').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            searching: false,
            pageLength: 20,
            ajax: {
                method:'get',
                url:userIndex,
            },
            columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'depo_id', name: 'depo_id' },
                    { data: 'menu', name: 'menu'},
                    { data: 'modul', name: 'modul'},
                    { data: 'permission', name: 'permission' },
                    { data: 'role', name: 'role' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            })
        })
    </script>
@endpush
@push('css')
    <link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
    {{-- <link href="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> --}}
    {{-- <link href="{{asset('css/handsontable.full.css')}}" rel="stylesheet"> --}}
@endpush
