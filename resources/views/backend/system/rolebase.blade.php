@extends('backend.app')

@section('content')
@role('super-admin')
<div class="ibox">
    <div class="ibox-title">
        <h2>FS002 <small>Role and Permission</small></h2>
    </div>
    
        <div class="ibox-content">
            @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Error...!</h4>
                    <ul class="list-inline">
                        @foreach ($errors->all() as $error)
                            <li class="list-inline-item">{{ $error }};</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-md-3 bg-secondary p-md b-r-md">
                <form id="form" action="{{route('create.role','role')}}" method="POST">
                        @csrf
                    <div class="form-group">
                        <label class="white">Role</label>
                        <input name="role" class="form-control required">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4 btn-group">
                            <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                </form>
                <form id="form" action="{{route('create.role','permission')}}" method="POST">
                        @csrf
                    <div class="form-group">
                        <label class="white">Permission</label>
                        <input name="permission" type="text" class="form-control required" name="permission">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4 btn-group">
                            <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                </form>
                <form id="form" action="{{route('create.role','async')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label class="white">Async Role and Permission</label>
                        <select class="form-control required select2" name="async_role[]" multiple="multiple">
                            @foreach ($role as $item)
                                <option>{{$item->name}}</option>
                            @endforeach
                        </select>
                        <div class="p-xs"></div>
                        <select class="form-control required select2" name="async_permission[]" multiple="multiple">
                            @foreach ($permission as $item)
                                <option>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4 btn-group">
                            <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                </form>
                <form id="form" action="{{route('create.role','revoke')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label class="white">Delete Role or Permission</label>
                        <select class="form-control required select2" name="async_role">
                            @foreach ($role as $item)
                                <option>{{$item->name}}</option>
                            @endforeach
                        </select>
                        <div class="p-xs"></div>
                        <select class="form-control required select2" name="async_permission" >
                            @foreach ($permission as $item)
                                <option>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4 btn-group">
                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                </form>
                </div>
                <div class="col-md-9">
                    <table id="tes" class="table table-hover ">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Guard</th>
                                <th>Permission</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            
        </div>
</div>
@else
    <div class="alert alert-danger">
        <h1>Anda tidak mempunyai akses halaman ini</h1>
    </div>
@endrole

@endsection

@push('before-script')
@include('parsial_script.system')
@endpush
@push('script')
{{-- <script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/js/bootstrap-datetimepicker.min.js"></script> --}}
@endpush
@push('css')
{{-- <link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet"> --}}
{{-- <link href="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> --}}
{{-- <link href="{{asset('css/handsontable.full.css')}}" rel="stylesheet"> --}}
@endpush