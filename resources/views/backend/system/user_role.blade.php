@extends('backend.app')

@section('content')

<div class="wrapper wrapper-content animated fadeInRight">


    <div class="row m-b-lg m-t-lg">
        <div class="col-md-6">
            <div class="profile-image">
                <img src="{{asset('img/a4.jpg')}}" class="rounded-circle circle-border m-b-md" alt="profile">
            </div>
            <div class="profile-info">
                <div class="">
                    <div>
                        <h2 class="no-margins">
                            {{$user->name}}
                        </h2>
                        <h4>{{$user->email}}</h4>
                        <small>
                            
                        </small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <table class="table small m-b-xs">
                <tbody>
                    <tr>
                        <td>
                            <strong>142</strong> Projects
                        </td>
                        <td>
                            <strong>22</strong> Followers
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <strong>61</strong> Comments
                        </td>
                        <td>
                            <strong>54</strong> Articles
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>154</strong> Tags
                        </td>
                        <td>
                            <strong>32</strong> Friends
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox">
                <div class="ibox-content">
                    <h3>User Roles</h3>
                    {{$user->depo_id}}
                    <ul class="list-unstyled file-list">
                        @foreach ($user->roles as $item)
                        <li>
                            <a href=""><i class="fa fa-file"></i> {{$item->name}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="ibox">
                <div class="ibox-content">
                    <h3>User Permission</h3>
                    <ul class="list-unstyled file-list">
                        @foreach ($user->permissions as $item)
                        <li>
                            <a href=""><i class="fa fa-file"></i> {{$item->name}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="ibox">
                <div class="ibox-content">
                    <h3>User Menu and Modul</h3>
                    {!! $user->menu !!}
                </div>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="ibox">
                <div class="ibox-content">
                    <form id="form" action="{{route('post.userroles',$user->id)}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Async Role and Permission</label>
                            <select class="form-control select-role" name="async_role">
                                <option value="" selected>## Select Role ##</option>
                                @foreach ($role as $item)
                                <option value="{{$item->name}}" >{{$item->name}}</option>
                                @endforeach
                            </select>
                            <div class="p-xs"></div>
                            <select class="form-control select-permission" name="async_permission[]" multiple="multiple">
                                
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4 btn-group">
                                <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>

        <div class="col-lg-4">
            <div class="ibox">
                <div class="ibox-content">
                    <form id="form" action="{{route('post.menumodule',$user->id)}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Depo</label>
                            <select class="form-control select-role" name="depo" >
                                <option selected >Office</option>
                                @foreach ($depo as $item)
                                    <option value="{{$item->id_depo}}" >{{$item->id_depo}}-{{$item->nama_depo}}</option>
                                @endforeach
                            </select>

                            <label>Modul</label>
                            <select class="form-control select-role" name="module[]" multiple="multiple">
                                @foreach ($module as $item)
                                <option value="{{$item->id}}" >{{$item->name}}-{{$item->ket}}</option>
                                @endforeach
                            </select>
                            <label>Menu</label>
                            <select class="form-control select-permission" name="menu[]" multiple="multiple">
                                @foreach ($menu as $item)
                                    @if($item->url == '#')
                                        <option value="{{$item->id}}"># {{$item->title}}</option>
                                    @else
                                        <option value="{{$item->id}}" >{{$item->title}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4 btn-group">
                                <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    </div>

</div>
@endsection

@push('before-script')
<script>
    const postUserRolesUrl = "{{route('post.userroles',0)}}"
</script>
@include('parsial_script.system')   
@endpush
@push('script')
{{-- <script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/js/bootstrap-datetimepicker.min.js"></script> --}}
@endpush

@push('css')
{{-- <link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet"> --}}
{{-- <link href="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> --}}
{{-- <link href="{{asset('css/handsontable.full.css')}}" rel="stylesheet"> --}}
@endpush
