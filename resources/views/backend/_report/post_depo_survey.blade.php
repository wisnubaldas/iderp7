@extends('backend.app')
@section('content')


<div class="card">
    <div class="card-header">
        <h4>No Survey <span class="text-navy">{{$ds->survey_no}}</span></h4>
        <div class="ibox-tools" >
            <a href="/report/survey/depo_survey" class="btn btn-primary btn-sm" >Back to Survey</a>
        </div>
    </div>
    @if ($errors->any())
        <x-survey.error :err="$errors->all()" />
    @endif
    <form id="frmPostSurvey" method="POST" action="/report/survey/post_depo_survey">
        @csrf
        <input type="text" hidden name="survey_no" value="{{$ds->survey_no}}">
    <div class="card-body">
        <div class="row">
            <div class="col-md-9">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Depo</label> 
                                <input type="text" value="{{$ds->depo_detail->nama_depo}}" readonly class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Merek</label> 
                                <input type="text" value="{{$ds->brand}}" readonly class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Ukuran</label> 
                                <input type="text" value="{{$ds->ukuran->title}}" readonly class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Motif</label> 
                                <input type="text" value="{{$ds->motif}}" readonly class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="text-danger"><strong>Upload Dokument Survey*</strong> </label> 
                            <div class="custom-file">
                                <input id="file" type="file" class="custom-file-input form-control-sm" >
                                <input type="text" id="image" name="image" hidden>
                                <label for="logo" class="custom-file-label">Choose file...</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                        <x-survey.depo-survey :data="$ds->depo_survey->customer['form_respon']" :warna="$ds->warna"/>
                </div>
            </div>
            <div class="col-md-3">
                <div id="fileName"></div>
            </div>
        </div>
    </div>

        <div class="card-footer">
            <button class="btn btn-primary btn-bg" type="submit" >Kirim Data Survey <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
        </div>
        </form>
</div>
<br />
@endsection
@push('script')
<script>
    function order_vote(stat, id)
    {
        if(stat == 'yes')
        {
            $('#order-'+id).show();
        }else{
            $('#order-'+id).hide();
        }
    }     

$('.crott').hide(); // hide dulu baru show

$('.i-checks').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
});

$('.i-checks-blue').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
});

let idImage = [];
$('.custom-file-input').on('change', function(event) {
    let fileName = $(this).val().split('\\').pop();
    $(this).next('.custom-file-label').addClass("selected").html(fileName);

    // upload image survey
    let f = event.target.files;
    let allowedExtensions = /(\.jpg|\.jpeg|\.png\.pdf)$/i; 
        if (!allowedExtensions.exec(f[0].name)) { 
            alert('file image harus .jpeg, .jpg'); 
            return false; 
        }else{
            var formData = new FormData();
            var imagefile = document.querySelector('#file');
            formData.append("image", imagefile.files[0]);
            console.log(formData)
            axios.post('/report/survey/add_image/0', formData, {
                            headers:{
                                        'Content-Type': 'multipart/form-data'
                                    }
            }).then(function(response){
                const img = response.data;
                // collect idnya
                idImage.push(img.id)
                $('#image').val(idImage)
                const tpl = `<div class="text-center" id="image-${img.id}">
                                <a href="${img.image_link}"  target="_blank" >
                                    <img src="${img.image_link}" alt="${img.image_name}" class="img-thumbnail border border-primary ">
                                </a>
                                <button type="button" class="btn btn-danger btn-outline btn-block m-sm apus-image" onclick="delImage('${img.id}','${img.image_name}')" >Apus Image</button>
                            </div>`;
                $('#fileName').append(tpl);
            }).catch(function(error){
                alert('file gagal di upload')
                console.error(error);
            })
        }
});

function delImage(id,name) {
    axios.post('/report/survey/delete_image/'+id+'/'+name)
            .then(function(response){
                // console.log(response.data)
                if(response.data)
                {
                    $('#image-'+id).remove();
                }
            })
            .catch(function(error){
                console.error(error);
            })
}

$('#form-respon').DataTable({
        pageLength: 5,
        // paging: false,
        // processing: true,
        lengthChange: false,
        searching:false,
        responsive: true,
});

</script>
@endpush
