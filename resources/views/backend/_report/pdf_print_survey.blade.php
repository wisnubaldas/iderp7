<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>survey Survey</title>
    <link rel="stylesheet" href="{{url('/font-awesome/css/font-awesome.css')}}" >
    <link rel="stylesheet" href="{{url('/css/pdf_sumarry_survey.css')}}" media="screen,print">
</head>

<body>
    <table style="width: 100%; height=100%;" >
        <tbody>
            <tr>
                <td colspan="6">
                    <h3 class="title">SUMMARY RESPON PRODUK BARU</h3> 
                </td>
            </tr>
            <tr>
                <td>
                    <span class="a">Depo</span>  <span class="b">:</span>  <span class="c font-bold">{{$survey['depo']}}</span>
                    <br>
                    <span class="a">Merek</span>  <span class="b">:</span>  <span class="c font-bold">{{$survey['merek']}}</span>
                    <br>
                    <span class="a">Ukuran</span>  <span class="b">:</span>  <span class="c font-bold">{{$survey['ukuran']}}</span>
                </td>
                <td>
                    <span class="a">Tgl Produksi</span>  <span class="b">:</span>  <span class="c font-bold">{{$survey['tgl_produksi']}}</span>
                    <br>
                    <span class="a">Nama Motif</span>  <span class="b">:</span>  <span class="c font-bold">{{$survey['nama_motif']}}</span>
                    <br>
                    <span class="a">Harga Depo</span>  <span class="b">:</span>  <span class="c font-bold">{{number_format($survey['harga_depo'])}}</span>
                </td>
                <td style="text-align: right; vertical-align: bottom;">
                    <span class="a">No Survey</span>  <span class="b">:</span>  <span class="c font-bold">{{$survey['no_survey']}}</span>
                    <br>
                    <span class="a">Durasi </span>  <span class="b">:</span>  <span class="c font-bold">{{$survey['durasi']}}</span>
                    <br>
                    <span class="a">{{ucfirst(Auth::user()->name)}},</span>  <span class="b"> </span>  <span class="c font-bold">{{\Carbon\Carbon::now()->format('d F Y')}}</span>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <table style="width: 100%;" class="with-border ">
                        <thead class="text-center">
                            <tr class="font-bold">
                                <td  rowspan="7" style="width: 5%">No</td>
                                <td rowspan="7" style="width: 25%">Nama Toko</td>
                                <td colspan="5">Komentar Barang (berikan tanda check di bawah ini)</td>
                                <td colspan="2" >Apakan Anda Mau Order</td>
                                <td colspan="4" >Perkiraan Quantity Order / Item Setiap Bulan (BOX)</td>
                                <td rowspan="7">
                                    Penjualan Sampai Tgl<br>
                                    {{  \Carbon\Carbon::now()->subDay()->format('d F Y') }}
                                </td>
                            </tr>
                            <tr class="font-bold" >
                                <td rowspan="6" class="panjang-vote" style="width: 5%">SANGAT SUKA</td>
                                <td rowspan="6" class="panjang-vote" style="width: 5%">SUKA</td>
                                <td rowspan="6" class="panjang-vote" style="width: 5%">CUKUP SUKA</td>
                                <td rowspan="6" class="panjang-vote" style="width: 5%">TIDAK SUKA</td>
                                <td rowspan="6" class="panjang-vote" style="width: 5%">SANGAT TIDAK SUKA</td>
                                <td rowspan="6" class="order-vote" style="width: 5%">YA</td>
                                <td rowspan="6" class="order-vote" style="width: 5%">TIDAK</td>
                                <td rowspan="4" colspan="2">{{$survey['merek']}}</td>
                                <td colspan="2">Sangat Suka > 100 Box</td>
                            </tr>
                            <tr class="font-bold">
                                <td colspan="2">Suka 50 - 99 Box</td>
                            </tr>
                            <tr class="font-bold">
                                <td colspan="2">Cukup Suka 1 - 49 Box</td>
                            </tr>
                            <tr class="font-bold">
                                <td colspan="2">Tidak Suka 0 B Box</td>
                            </tr>
                            <tr class="font-bold">
                                <td colspan="4">Jika Ya mau order harus di isi dengan quantity order</td>
                            </tr>
                            <tr class="font-bold">
                                @foreach ($survey['warna'] as $item)
                                    <td style="width: 8%">{{$item}}</td>
                                @endforeach
                            </tr>
                            
                        </thead>
                        <tbody>
                            @php($total = 0)
                            @foreach ($survey['vote_order'] as $item)
                                <tr class="tbl-core">
                                    <td class="text-center" style="background-color: #62ff3b94;">{{$loop->index + 1}}</td>
                                    <td>{{$item['name']}}</td>
                                    @foreach ($item['vote'] as $v)
                                        @if ($v)
                                            <td class="text-center"><i class="fa fa-check" aria-hidden="true"></i></td>
                                        @else
                                            <td></td>
                                        @endif
                                    @endforeach
                                    @if (array_sum($item['order']) != 0)
                                        <td class="text-center"><i class="fa fa-check" aria-hidden="true"></i></td>
                                        <td></td>
                                    @else
                                        <td></td>
                                        <td class="text-center"><i class="fa fa-check" aria-hidden="true"></i></td>
                                    @endif
                                    @foreach ($survey['warna'] as $wr)
                                        @if (isset($item['order'][$wr]))
                                            <td class="panjang-vote text-center">{{$item['order'][$wr]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                    @endforeach
                                    @if (isset($survey['jual'][$item['customer_id']]))
                                        @php($total += $survey['jual'][$item['customer_id']])
                                        <td class="panjang-vote text-center">{{$survey['jual'][$item['customer_id']]}}</td>
                                    @else
                                        <td></td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                
                        <tfoot class="font-bold tbl-total text-center font-bold">
                            <tr>
                                <td colspan="2">Total</td>
                                <td>{{$survey['tot_vote']['1']}}</td>
                                <td>{{$survey['tot_vote']['2']}}</td>
                                <td>{{$survey['tot_vote']['3']}}</td>
                                <td>{{$survey['tot_vote']['4']}}</td>
                                <td>{{$survey['tot_vote']['5']}}</td>
                                <td></td>
                                <td></td>
                                @foreach ($survey['warna'] as $wr)
                                    @if (isset($survey['tot_order'][$wr]))
                                        <td class="panjang-vote text-center">{{array_sum($survey['tot_order'][$wr])}}</td>
                                    @else
                                        <td></td>
                                    @endif
                                @endforeach
                                <td>{{$total}}</td>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="f-10">
        <tr>
            <td>
                Note:
                <ul>
                    <li>Survey Harus Sebanyak 20 Toko</li>
                    <li>Survey ke toko yang sudah menjadi pelanggan tetap Depo & toko tersebut juga 
                        banyak menjual merek kompetitor yang setara/sekelas dengan merek 
                        produk baru yang di survey ini.
                    </li>
                    <li>Nama toko harus sesuai dengan database accura (data pelanggan depo)</li>
                    <li>Yang dimaksud item adalah per warna</li>
                    <li>Kepala Depo wajib menuliskan perkiraan harga jual / box di form ini sebelum salesman melakukan 
                        survey produk baru ini
                    </li>
                    <li>Form respon produk baru ini harus di isi lengkap</li>
                </ul>
            </td>
            @if (in_array($survey['status'],[7,6,9]))
            <td class="text-center">
                Diketahui<br>
                Regional Manager<br>
                    <img src="{{url('img/approved.png')}}" alt="approved" width="200px">
                (...................................)
            </td>
            @else
            <td class="text-center">
                Diketahui<br>
                Regional Manager<br>
                    <img src="{{url('img/approved.png')}}" alt="approved" width="200px">
                (...................................)
            </td>
            @endif
            @if (in_array($survey['status'],[6,9,7]))
            <td class="text-center">
                Dibuat<br>
                Kepala Depo<br>
                    <img src="{{url('img/approved.png')}}" alt="approved" width="200px">
                (...................................)
            </td>
            @else
            <td class="text-center">
                Dibuat<br>
                Kepala Depo<br>
                    <img src="{{url('img/approved.png')}}" alt="approved" width="200px">
                (...................................)
            </td>
            @endif
            
        </tr>
    </table>
    @foreach ($survey['image'] as $item)
            <div class="page-break"></div>
            <img src="{{asset($item)}}" width="100%" height="910px" >
    @endforeach
</body>
</html>
