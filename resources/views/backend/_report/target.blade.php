@extends('backend.app')

@section('content')
	<default-component :target="true"></default-component>
@endsection

@push('script')
<script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/js/bootstrap-datetimepicker.min.js"></script> --}}
@endpush
@push('css')
<link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
{{-- <link href="https://cdn.jsdelivr.net/npm/pc-bootstrap4-datetimepicker@4.17/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> --}}
<link href="{{asset('css/handsontable.full.css')}}" rel="stylesheet">
@endpush

<body>
    <h3>SUMMARY RESPON PRODUK BARU</h3>
    <div class="header">
        <table class="c" >
            <colgroup>
                <col>
            </colgroup>
            <tr>
                <td>Merek</td>
                <td>: {{$head_tpl[0]['brand']}}</td>
            </tr>
            <tr>
                <td width="100px">Nama Motif</td>
                <td>: {{$head_tpl[0]['motif']}}</td>
            </tr>
            <tr>
                <td>Ukuran</td>
                <td>: {{$head_tpl[0]['ukuran']}} <span style="padding-left:45em;">{{Auth::user()->name}}-{{today()}}</span></td>
            </tr>
        </table>

    </div>
    <div class="content">
        <table class="tbl-r">
            <colgroup>
                <col style="width:90px;" class="text-center">
                <col style="width:160px;">
                <col style="width:25px;">
                <col style="width:50px;">
                <col style="width:25px;">
                <col style="width:50px;">
                <col style="width:25px;">
                <col style="width:50px;">
                <col style="width:25px;">
                <col style="width:50px;">
                <col style="width:25px;">
                <col style="width:50px;">
                <col style="width:25px;">
                <col style="width:50px;">
                <col style="width:50px; text-align:right;">
                <col style="width:50px; text-align:right;">
                <col style="width:50px; text-align:right;">
                <col style="width:50px; text-align:right;">
            </colgroup>
            <thead class="thead">
                <tr class="">
                    <td rowspan="7" class="br-tbl-abs">REGIONAL</td>
                    <td rowspan="7" class="br-tbl-abs">NAMA DEPO</td>
                    <td colspan="12" class="br-tbl-a">KOMENTAR BARANG</td>
                    <td colspan="4" class="br-tbl-a br-tbl-kn">PERKIRAAN QUANTITY ORDER PER ITEM SETIAP BULAN (BOX)</td>
                </tr>
                <tr>
                    <td rowspan="6" colspan="2" class="br-tbl-b">SANGAT SUKA</td>
                    <td rowspan="6" colspan="2" class="br-tbl-b">SUKA</td>
                    <td rowspan="6" colspan="2" class="br-tbl-b">CUKUP SUKA</td>
                    <td rowspan="6" colspan="2" class="br-tbl-b">TIDAK SUKA</td>
                    <td rowspan="6" colspan="2" class="br-tbl-b">SANGAT TIDAK SUKA</td>
                    <td rowspan="6" colspan="2" class="br-tbl-b">%</td>
                    <td rowspan="4" colspan="2" class=" ">{{$head_tpl[0]['brand']}}</td>
                    <td colspan="2" class="br-tbl-kn kecil">Sangat Suka: >= 200 Box</td>
                </tr>
                <tr>
                    <td colspan="2" class="br-tbl-kn kecil">Suka: 100 - 199 Box</td>
                </tr>
                <tr>
                    <td colspan="2" class="br-tbl-kn kecil" >Cukup Suka: 50 - 99 Box</td>
                </tr>
                <tr>
                    <td colspan="2" class="br-tbl-kn kecil" >Tidak Suka: 0 - 49 Box</td>
                </tr>
                <tr>
                    <td colspan="4" class="br-tbl-kn" >JIKA "YA" MAU ORDER, HARUS DIISI DENGAN QUANTITY ORDER</td>
                </tr>
                <tr>
                    {!! $h->head_warna($head_tpl[0]['warna']) !!}
                </tr>
            </thead>
            <tbody>
                @php($tot_footer = [])
                @php($tot_order = [null,null,null,null])
                @foreach ($h->main_data($summary_region) as $key => $item)
                    <tr>
                        <td rowspan="{{count($item)+4}}" class="br-tbl-kr br-tbl-kn text-center br-tbl-b br-tbl-a">{{$key}}</td>
                        {{-- Subtotal order variable --}}
                        @php($sub_tot_order = [null,null,null,null])
                        @foreach ($item as $depo)
                            <tr>
                                <td class="br-tbl-kn br-tbl-b-dot">{{$depo['nama_depo']}}</td>
                                {!! $h->voting($depo['survey']['vote'],$loop->last) !!}
                               
                                @foreach($h->order($depo['survey']['order']) as $order)
                                    @php($class_order = '')                                    
                                    @switch($loop->index)
                                        @case(0)
                                            @php($sub_tot_order[0] += $order) 
                                            @break
                                        @case(1)
                                            @php($sub_tot_order[1] += $order) 
                                            @break
                                        @case(2)
                                            @php($sub_tot_order[2] += $order) 
                                            @break
                                        @case(3)
                                            @php($class_order = 'br-tbl-kn')
                                            @php($sub_tot_order[3] += $order)  
                                        @break
                                    @endswitch
                                    <td class="text-right {{$class_order}}">{{$order}}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tr>
                    <tr>
                        <td class="br-tbl-kn text-right">Sub Total</td>
                        @php($tot_sub = 0) @php($per_sub = 0) @php($persub_arr = [])
                        @foreach ($h->sub_total_voting($item) as $it)
                            @if ($loop->index%2 == 0)
                                @php($tot_sub = $tot_sub+$it)
                                <td class="text-center">{{$it}}</td>
                            @else
                                @php($per_sub = $per_sub+$it)   
                                <td class="text-center">{{$h->n($it)}}%</td>
                                @php(array_push($persub_arr,$it))
                                @php(array_push($tot_footer,$it))
                            @endif
                        @endforeach
                        {{-- persentase total --}}
                        <td class="text-center">{{$tot_sub}}</td>
                        <td class="text-center br-tbl-kn">{{$h->n($per_sub)}}%</td>
                        
                        @foreach ($sub_tot_order as $sub_order)
                            
                            @switch($loop->index)
                                @case(0)
                                    @php($tot_order[0] += $sub_order)
                                    @break
                                @case(1)
                                @php($tot_order[1] += $sub_order)
                                    @break
                                @case(2)
                                @php($tot_order[2] += $sub_order)
                                    @break
                                @case(3)
                                @php($tot_order[3] += $sub_order)
                                    @break
                            @endswitch
                            <td class="text-right br-tbl-kn">{{$sub_order}}</td>
                        @endforeach
                    </tr>
                    <tr>
                        <td class="br-tbl-kn text-right bg-grey">Sub Total X Nilai Skor</td>
                        @php($idx = 5)
                        @php($nil_motif = 0)
                        @foreach ($persub_arr as $item)
                        @php($x = round(($item*($idx--))/100,2,PHP_ROUND_HALF_UP))
                            <td colspan="2" class="text-center bg-grey">{{$h->n($x)}}</td>
                            @php($nil_motif = $nil_motif + $x)                            
                        @endforeach
                            <td colspan="2" class="text-center br-tbl-kn bg-grey"> - </td>
                            <td colspan="4" class="text-right br-tbl-kn">{{$h->n(array_sum($sub_tot_order),0)}}</td>
                    </tr>
                    <tr>
                        <td class="br-tbl-kn text-right br-tbl-b bg-grey">Nilai Motif</td>
                        <td colspan="12" class="text-center br-tbl-kn br-tbl-b bg-grey">{{$nil_motif}}</td>
                        <td colspan="4" class="br-tbl-b br-tbl-kn">-</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot style="font-weight: bold">
                @php($chart_vote = []) @php($chart_order = [])
                <tr>
                    <td colspan="2" class="text-right">Total</td>
                    @foreach ($h->total_footer($tot_footer) as $item)
                        @php($tot = round($item/7,2))
                        <td colspan="2" class="text-center">{{$h->n($tot)}}%</td>
                        @php(array_push($chart_vote,$tot))
                    @endforeach
                    <td colspan="2" class="text-center">100%</td>

                    @foreach ($tot_order as $tot_ord)
                        <td class="text-right">{{$tot_ord}}</td>
                        @php(array_push($chart_order,$tot_ord))
                    @endforeach
                </tr>
                <tr>
                    <td colspan="2" class="text-right">Nilai Skor</td>
                    <td colspan="2" class="text-center">X 5</td>
                    <td colspan="2" class="text-center">X 4</td>
                    <td colspan="2" class="text-center">X 3</td>
                    <td colspan="2" class="text-center">X 2</td>
                    <td colspan="2" class="text-center">X 1</td>
                    <td colspan="2" class="text-center">-</td>
                    <td colspan="3" rowspan="3" class="text-center">Jumlah Quantity order/box</td>
                    <td rowspan="3" class="text-right">{{number_format(array_sum($tot_order))}}</td>
                </tr>
                <tr>
                    <td colspan="2" class="bg-grey text-right">Total X Nilai Skor</td>
                    @php($it = 5)
                    @php($grand_tot = 0)
                    @foreach ($h->total_footer($tot_footer) as $item)
                        @php($xx = round(($item/7)*$it--/100,2))
                        <td colspan="2" class="bg-grey text-center">{{$h->n($xx)}}</td>
                        @php($grand_tot = $grand_tot + $xx)
                    @endforeach
                    <td colspan="2" class="bg-grey text-center"> - </td>
                </tr>
                <tr>
                    <td colspan="2" class="bg-grey text-right">Grand Total (Skala 5)</td>
                    <td colspan="12" class="bg-grey text-center"><span style="color: red">{{$grand_tot}}</span></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <br>
    <br>
    <br >
    {{-- <div class="page-break"></div> --}}
   
    {{-- ChartJS --}}
    <table width="100%" class="page-break" style="padding-top:500px;">
        <tr style="text-align: center">
            <td>Komentar Barang</td>
            <td>Quantity Order/Barang</td>
        </tr>
        <tr>
            <td><canvas id="voting" width="500" height="300"></canvas></td>
            <td><canvas id="order" width="500" height="300"></canvas></td>
        </tr>
        <tr>
            <td colspan="2" style="padding-top: 200px;">
                <p>
                    <strong>Kesimpulan:</strong> 
                    {!! $kesimpulan !!}
                </p>
            </td>
        </tr>
    </table>
    <hr />

    <table width="100%">
        <tr>
            <td class="text-center">Disetujui,</td>
            <td colspan="3" class="text-center">Diketahui,</td>
            <td colspan="3" class="text-center">Diajukan,</td>
        </tr>
        <tr>
            <td class="text-center-p">(Lin Chien Ping)</td>
            <td class="text-center-p">(Sudarsa)</td>
            <td class="text-center-p">(Suhartono)</td>
            <td class="text-center-p">({{$diajukan ?? '.............'}})</td>
            <td class="text-center-p">(Suripto)</td>
            <td class="text-center-p">(Rengga)</td>
            <td class="text-center-p">(Erikbal)</td>
        </tr>
    </table>

    <script>
        // extend 
        Chart.types.Bar.extend({
            name: 'BarOverlay',
            draw: function (ease) {

                // First draw the main chart
                Chart.types.Bar.prototype.draw.apply(this);
                var ctx = this.chart.ctx;

                ctx.closePath();
            }
        });
        
        const warna = @json($h->head_warna);
        const dataChart = @json($chart_vote);
        const dataOrder = @json($chart_order);
        var barData = {
                    labels : ["Sangat Suka","Suka","Cukup Suka","Tidak Suka","Sangat Tidak Suka"],
                    datasets : [
                            {
                                label: "My Second dataset",
                                fillColor: "rgba(255, 0, 0,0.5)",
                                strokeColor: "rgba(255, 0, 0,0.8)",
                                highlightFill: "rgba(255, 0, 0,0.75)",
                                highlightStroke: "rgba(255, 0, 0,1)",
                                data : dataChart
                            }
                        ]
                    }
                    
        // get bar chart canvas
        var income1 = document.getElementById("voting").getContext("2d");
        // draw bar chart
        new Chart(income1).BarOverlay(barData);
        
                    

        var orderData = {   
                    labels : warna,
                    datasets : [
                            {
                                label: "My Second dataset",
                                fillColor: "rgba(0, 0, 254,0.5)",
                                strokeColor: "rgba(0, 0, 254,0.8)",
                                highlightFill: "rgba(0, 0, 254,0.75)",
                                highlightStroke: "rgba(0, 0, 254,1)",
                                data : dataOrder
                            }
                        ]
                    }
        // option
        const option = {
            scaleBeginAtZero : true,
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.05)",
        }
        // get bar chart canvas
        var income2 = document.getElementById("order").getContext("2d");
        // draw bar chart
        new Chart(income2).Bar(orderData,option);


                    // var ctx = document.getElementById("order").getContext("2d");
                    // var myBarChart = new Chart(ctx, {
                    //     type: "bar",
                    //     data: orderData,
                    // });
    </script>
    
</body>