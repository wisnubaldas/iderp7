@extends('backend.app')
@section('content')
@if($errors->any())
    {{-- <div class="alert alert-danger">
        <h4>{{$errors->first()}}</h4>
    </div> --}}
    <x-survey.error :err="$errors->all()" />
@endif

<div class="panel panel-primary">
    <div class="panel-heading">
        ID {{Auth::user()->depo_id}} To Progress
    </div>
    <div class="panel-body">
        <table id="tes" class="table table-hover">
                <thead>
                    <tr>
                        <th>Status</th>
                        <th>Nomer Survey</th>
                        <th>Tanggal Survey</th>
                        <th>Brand</th>
                        <th>Depo</th>
                        <th>Motif</th>
                        <th>Ukuran</th>
                        <th>#</th>
                    </tr>
                </thead>
            </table>
    </div>
</div>
   
@endsection
@push('script')
    <script>
        jQuery(function(){
            let table = $('#tes').DataTable({
                    ajax: '/report/survey/depo_survey/proses_survey',
                    columns: [
                        { data: 'status',name:'status'},
                        { data: 'survey_no',name:'survey_no'},
                        { data: 'tgl',name: 'tgl' },
                        { data: 'brand',name: 'brand' },
                        { data: 'depo_detail.nama_depo',name: 'depo' },
                        { data: 'motif',name: 'motif' },
                        { data: 'ukuran.title',name: 'ukuran' },
                        { data: 'action',name: 'action' },
                    ],
                    processing: true,
                    serverSide: true,
                    pageLength: 10,
                    // paging: false,
                    lengthChange: false,
                    searching:false,
                    responsive: true,
            });
        })
    </script>
@endpush