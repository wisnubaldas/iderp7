@extends('backend.app')

@section('content')
<div class="ibox">
    <div class="ibox-title">
        <h3 class="border-bottom">FS002 <small>Upload Data Penjualan</small></h3>
    </div>
    <div class="ibox-content">
        <div class="raw">
        <form action="{{url('report/survey/upload_penjualan')}}" method="POST" enctype="multipart/form-data">
            @csrf
                <div class="col-6">
                    <div class="custom-file">
                        <input id="logo" type="file" class="custom-file-input" name="file">
                        <label for="logo" class="custom-file-label">Choose file...</label>
                    </div>  
                </div>
                <div class="col-6">
                    <button class="btn btn-outline btn-primary" type="submit">Save</button>
                </div>
        </form> 
    </div>
    </div>
</div>
@endsection

@push('before-script')
    @include('parsial_script.survey')
@endpush

@push('script')
<!-- Select2 -->
<script src="{{asset('js/plugins/sweetalert/sweetalert.min.js')}}"></script>
<!-- Tags Input -->
<script>
    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    }); 
</script>
@endpush
@push('css')
<link href="{{asset('css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
<style>
    .swal-overlay {
        background-color: rgba(43, 165, 137, 0.45);
    }
</style>
@endpush
