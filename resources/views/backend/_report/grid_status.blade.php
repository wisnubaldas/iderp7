@extends('backend.app')
@section('content')
<div class="row">
<div class="col-12">
    @if($errors->any())
        <div class="alert alert-danger">
            <h4>{{$errors->first()}}</h4>
        </div>
    @endif
</div>
    <div class="card col-lg-12">
        
        <div class="card-body table-responsive">
            <table id="tes" class="table table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th>Status</th>
                        <th>No Survey</th>
                        <th>Tanggal Survey</th>
                        <th>Brand</th>
                        <th>Depo</th>
                        <th>Motif</th>
                        <th>Ukuran</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection
@push('script')
<script>
    $(function () {
        function tblFormat(id,d,depo){
        $('#' + id).DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            searching: false,
            pageLength: 5,
            ajax: {
                method:'get',
                url:'/report/survey/get_customer',
                data:{id:d,depo:depo}
            },
            columns: [
                    { data: 'id_toko', name: 'id_toko' },
                    { data: 'nama_toko', name: 'nama_toko' }
                ]
            })
        }

        let table = $('#tes').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/report/survey/get_status',
            columns: [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "searchable":      false,
                    "data":           null,
                    "defaultContent": ''
                },
                { data: 'status',name: 'status.status',"orderable":false,},
                { data: 'survey_no',name: 'survey_no' },
                { data: 'tgl',name: 'tgl' },
                { data: 'brand',name: 'brand' },
                { data: 'depo_detail.nama_depo',name: 'depo' },
                { data: 'motif',name: 'motif' },
                { data: 'ukuran.title',name: 'ukuran.title', "orderable":false,"searchable":false},
                { data: 'action',name: 'action', "orderable":false,"searchable":false},

            ],
            pageLength: 10,
            // paging: false,
            processing: true,
            lengthChange: false,
            responsive: true,
           
            // dom: '<"html5buttons"B>lTfgitp',
            // buttons: [{extend: 'copy'},
            //     {extend: 'csv'},
            //     {extend: 'excel',title: 'ExampleFile'},
            //     {extend: 'pdf',title: 'ExampleFile'},
            //     {
            //         extend: 'print',
            //         customize: function (win) {
            //             $(win.document.body).addClass('white-bg');
            //             $(win.document.body).css('font-size', '10px');
            //             $(win.document.body).find('table')
            //                 .addClass('compact')
            //                 .css('font-size', 'inherit');
            //         }
            //     }
            // ]
        });

        $('#tes tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            let r = (d)=> {console.log(d)
                        return `<table class="table table-hover" style="width:100%" id=${row.data()['id']}>
                        <thead><tr><th>ID Customer</th><th>Nama Customer</th></tr></thead>
                        </table>`}
            row.child(r(row.data())).show();
            tblFormat(row.data()['id'],row.data()['customer'],row.data()['depo_detail']['id_depo']);
            tr.addClass('shown');
            tr.next().find('td').addClass('no-padding');

        }
    });

    });// end func

    
</script>
@endpush
