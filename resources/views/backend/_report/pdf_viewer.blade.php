@extends('backend.layouts.pdf_layout')
@section('content')
@php
    // dump($m[0]->depo_survey->vote);
    $brand = $m[0]->brand;
    $motif = $m[0]->motif;
    $ukuran = $m[0]->ukuran->title;
    $user = Auth::user()->name.' / '.date('d-m-Y',strtotime('now'));
    $tod_data = $m->count();
@endphp
<table class="body-wrap" border="0">
    <tbody>
        <tr>
            <td class="container">
                <div class="content">
                    <table class="main" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td class="content-wrap text-center">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td class="content-block ">
                                                    <h2 class="font-bold">SUMMARY RESPON PRODUK BARU</h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="content-block">
                                                    <table class="text-left" border="0" width="100%" id="tbl-status">
                                                        <tbody>
                                                            <tr>
                                                                <td class="font-bold">
                                                                    Merek : <span class="text-success" >{{$brand}}</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="font-bold">Nama Motif : <span class="text-success">{{$motif}}</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td class="font-bold">Ukuran : <span class="text-success">{{ $ukuran }}</span></td>
                                                            </tr>
                                                            <tr class="text-right text-primary font-bold">
                                                            <td>{{strtoupper($user)}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width='100%' class="invoice-items" cellspacing="0"
                                                                        cellpadding="0" border="0">
                                                                        <thead class="text-center text-primary font-bold">
                                                                            <tr>
                                                                                <td rowspan="7" class="bg-primary">REGIONAL</td>
                                                                                <td rowspan="7" class="bg-primary">NAMA DEPO</td>
                                                                                <td colspan="12" class="bg-warning" >KOMENTAR BARANG</td>
                                                                                <td colspan="4" class="bg-warning" >PERKIRAAN QUANTITY ORDER PER ITEM SETIAP BULAN (BOX)</td>
                                                                            </tr>
                                                                            <tr >
                                                                                <td rowspan="6" colspan="2" class="bg-primary">SANGAT SUKA</td>
                                                                                <td rowspan="6" colspan="2" class="bg-primary">SUKA</td>
                                                                                <td rowspan="6" colspan="2" class="bg-primary">CUKUP SUKA</td>
                                                                                <td rowspan="6" colspan="2" class="bg-primary">TIDAK SUKA</td>
                                                                                <td rowspan="6" colspan="2" class="bg-primary">SANGAT TIDAK SUKA</td>
                                                                                <td rowspan="6" colspan="2" class="bg-primary">%</td>
                                                                                <td rowspan="4" colspan="2" class="bg-success">PICASSO</td>
                                                                                <td colspan="2" class="bg-success text-left">Sangat Suka: >= 200 Box</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" class="bg-success text-left">Suka: 100 - 199 Box</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" class="bg-success text-left">Cukup Suka: 50 - 99 Box</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" class="bg-success text-left">Tidak Suka: 0 - 49 Box</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" class="bg-success ">Jika Ya, Mau order harus di isi dengan quantity order</td>
                                                                            </tr>
                                                                            <tr class="bg-info">
                                                                                <td>LCR</td>
                                                                                <td>LBR</td>
                                                                                <td>LCR</td>
                                                                                <td>MBR</td>
                                                                            </tr>
                                                                        </thead>
                                                                        @for ($i = 0; $i < $tod_data; $i++)
                                                                            @php
                                                                                $s = collect(json_decode($m[$i]->depo_survey->vote));
                                                                                $ll = $s->countBy()->toArray();
                                                                                $tot = collect($ll)->sum();
                                                                                $order = collect(json_decode($m[$i]->depo_survey->order))->values();
                                                                                // $order->sum(function ($product) {
                                                                                //     $x = collect($product)->values();
                                                                                //     dump(intval($x));
                                                                                // });
                                                                                // dump($order);
                                                                            @endphp
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Jabotabek</td>
                                                                                <td>{{$m[$i]->depo_detail->nama_depo}}</td>
                                                                                <td>{{isset($ll['Sangat Suka'])?$ll['Sangat Suka']:0}}</td>
                                                                                <td>{{isset($ll['Sangat Suka'])?($ll['Sangat Suka']*5/100):0}} %</td>
                                                                                <td>{{isset($ll['Suka'])?$ll['Suka']:0}}</td>
                                                                                <td>{{isset($ll['Suka'])?($ll['Suka']*5/100):0}} %</td>
                                                                                <td>{{isset($ll['Cukup Suka'])?$ll['Cukup Suka']:0}}</td>
                                                                                <td>{{isset($ll['Cukup Suka'])?($ll['Cukup Suka']*5/100):0}} %</td>
                                                                                <td>{{isset($ll['Tidak Suka'])?$ll['Tidak Suka']:0}}</td>
                                                                                <td>{{isset($ll['Tidak Suka'])?$ll['Tidak Suka']*5/100:0}} %</td>
                                                                                <td>{{isset($ll['Sangat Tidak Suka'])?$ll['Sangat Tidak Suka']:0}}</td>
                                                                                <td>{{isset($ll['Sangat Tidak Suka'])?$ll['Sangat Tidak Suka']*5/100:0}} %</td>
                                                                                <td>{{$tot}}</td>
                                                                                <td>{{$tot*5/100}} %</td>
                                                                                <td>300</td>
                                                                                <td>1700</td>
                                                                                <td>1800</td>
                                                                                <td>1800</td>
                                                                            </tr>
                                                                            
                                                                            <tr class="bg-info">
                                                                                <td colspan="2" >Sub total Nilai Skor</td>
                                                                                <td colspan="2">0</td>
                                                                                <td colspan="2">0</td>
                                                                                <td colspan="2">0</td>
                                                                                <td colspan="2">0</td>
                                                                                <td colspan="2">0</td>
                                                                                <td colspan="2">0</td>
                                                                                <td colspan="4">0</td>
                                                                            </tr>
                                                                            <tr class="bg-info">
                                                                                <td colspan="2">Nilai Motif</td>
                                                                                <td colspan="12">0</td>
                                                                                <td colspan="4">0</td>
                                                                            </tr>
                                                                        </tbody>
                                                                        @endfor
                                                                        
                                                                        <tfoot>
                                                                            <tr>
                                                                                <td colspan="2">Totalal</td>
                                                                                <td colspan="2">0,00%</td>
                                                                                <td colspan="2">30,00%</td>
                                                                                <td colspan="2">70.00%</td>
                                                                                <td colspan="2">10.00%</td>
                                                                                <td colspan="2">12.00%</td>
                                                                                <td colspan="2">%%##</td>
                                                                                <td>LCR</td>
                                                                                <td>LBR</td>
                                                                                <td>LCR</td>
                                                                                <td>MBR</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">Nilai Skor</td>
                                                                                <td colspan="2">0,00%</td>
                                                                                <td colspan="2">30,00%</td>
                                                                                <td colspan="2">70.00%</td>
                                                                                <td colspan="2">10.00%</td>
                                                                                <td colspan="2">12.00%</td>
                                                                                <td colspan="2">%%##</td>
                                                                                <td colspan="3" rowspan="3">Jumlah quantity order per box</td>
                                                                                <td colspan="3" rowspan="3">Jumlah @XXX</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">Total X Nilai Skor</td>
                                                                                <td colspan="2">0,00%</td>
                                                                                <td colspan="2">30,00%</td>
                                                                                <td colspan="2">70.00%</td>
                                                                                <td colspan="2">10.00%</td>
                                                                                <td colspan="2">12.00%</td>
                                                                                <td colspan="2">%%##</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">Grand Total</td>
                                                                                <td colspan="12">grand</td>
                                                                            </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="content-block">
                                                    <a href="#">View in browser</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="content-block">
                                                    Company Inc. 123 Van Ness, San Francisco 94102
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="footer">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td class="aligncenter content-block">Questions? Email <a
                                            href="mailto:">support@company.inc</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </td>
            <td></td>
        </tr>
    </tbody>
</table>
@endsection
