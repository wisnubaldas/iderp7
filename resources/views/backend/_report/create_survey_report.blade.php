@extends('backend.app')
@section('content')
<div class="row">
    <div class="card col-lg-12">
        <div class="card-body table-responsive">
            <div id="report_sumary">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3>SUMMARY RESPON PRODUK BARU</h3>
                    </div>
                    <div class="col-12">
                        <div id="xspreadsheet"></div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="https://unpkg.com/x-data-spreadsheet@1.1.5/dist/xspreadsheet.css">   
@endpush
@push('script')
<script src="https://unpkg.com/x-data-spreadsheet@1.1.5/dist/xspreadsheet.js"></script>
<script>
    const dataNya = @json($data);
    console.log(dataNya);
    
    const rows = {
        0: {
          cells: {
            0: { text: 'Merek:', style:0,merge: [0, 2] },
          },
        },
        1: {
          cells: {
            0: { text: 'Nama Motif:',style:0,merge: [0, 2]},
          }
        },
        2: {
          cells: {
            0: { text: 'Ukuran:',style:0,merge: [0, 2]},
          }
        },
      };

    // console.log(rows);

    let xs = x_spreadsheet('#xspreadsheet',{
    showToolbar: false, 
    showGrid: true,
    mode:'read',
    row: {
        len: 30,
        height: 20,
    },
    col: {
        len: 10,
        width: 50,
        indexWidth: 60,
        minWidth: 60,
    },
}) .loadData(
                {
                    name:'sheet1',
                    styles: [
                            {
                                textwrap: true,
                                font:{
                                    bold:true,
                                }
                            },
                    ],
                    merges: [
                        'A1:C1',
                    ],
                    cols: {
                        0: { width: 50 },
                    },
                    rows:rows
                }
            ) // load data
                .change(data => {
                    // save data to db
                    console.log(data)
                });
 </script>
 
@endpush