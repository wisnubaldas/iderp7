@extends('backend._report.approve_kadep')
@section('header')
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">{{$survey->survey_no}}</div>
        <div class="right"></div>
@endsection

@section('content')
@php($vote = json_decode($survey->depo_survey->vote, true))
@php($order = json_decode($survey->depo_survey->order, true))
<div class="section-title">
    {{$survey->depo_survey->customer['brand']}}/{{$survey->depo_survey->customer['ukuran']}}/{{$survey->depo_survey->customer['motif']}}
</div>
<div class="wide-block pt-2 pb-2">
    <div class="table-responsive">
        <table class="table text-center">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Toko</th>
                    <th>Vote</th>
                    <th>Order</th>
                </tr>
            </thead>
            <tbody>
                @php($idxx = 1)
                @foreach ($customer as $item)
                    <tr>
                        {{-- @dump($item) --}}
                        <td>{{$idxx++}}</td>
                        <td class="text-left">
                            {{$item->nama_toko}}
                        </td>
                        <td>
                            {{SurveyHelper::status_vote($vote[$item->id_toko])}}
                        </td>
                        <td>
                            <button type="button" class="btn btn-secondary btn-sm"  data-toggle="modal" data-target="#{{$item->id_toko}}">Total {{array_sum($order[$item->id_toko])}}</button>
                            <!-- Modal Listview -->
                            <div class="modal fade modalbox" id="{{$item->id_toko}}" data-backdrop="static" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{$item->nama_toko}}</h5>
                                            <a href="javascript:;" data-dismiss="modal" class="btn btn-warning">Close</a>
                                        </div>
                                        <div class="modal-body p-0">
                                            <ul class="listview image-listview flush mb-2">
                                                @foreach ($order[$item->id_toko] as $idx => $ord)
                                                    <li>
                                                        <div class="item h1">
                                                            {{$idx}}: 
                                                            <div class="in text-success">
                                                                <div>{{($ord)?$ord:0}}</div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- * Modal Listview -->
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection