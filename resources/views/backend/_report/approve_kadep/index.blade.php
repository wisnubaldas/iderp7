@extends('backend._report.approve_kadep')
@section('header')
        <div class="left">
            {{-- <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a> --}}
        </div>
        <div class="pageTitle">PT Chang Jui Fang Indonesia</div>
        <div class="right"></div>
@endsection

@section('content')
<div class="section-title">Data Approval Survey</div>
<div class="wide-block pt-2 pb-2">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Approve</th>
                    <th>Survey</th>
                    <th>Tgl</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $item)
                    <tr>
                        <td>
                            <a href="{{url('report/survey/kadep_approval/confirm',$item->id)}}" class="btn btn-icon btn-sm btn-primary">
                                <ion-icon name="checkmark-circle" ios="ios-checkmark-circle"></ion-icon>
                            </a>
                        </td>
                        <td>
                            <a href="{{url('report/survey/kadep_approval/detail_survey',$item->survey_no)}}" class="btn btn-text-primary btn-sm">
                                {{$item->survey_no}}
                            </a>
                        </td>
                        <td>{{date('Y/m/d',strtotime($item->tgl))}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection