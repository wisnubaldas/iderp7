@php
    class Kacrut {
        public $head_warna;
        public function order($data)
        {
            // get all order
            $order = collect($data)->values();
            
            $order->transform(function($a,$k){
                switch (count($a)) {
                    case 1:
                        return array_values(array_merge($a,[null,null,null]));
                        break;
                    case 2:
                        return array_values(array_merge($a,[null,null]));
                        break;
                    case 3:
                        return array_values(array_merge($a,[null]));
                        break;
                    case 4:
                        return array_values($a);
                        break;
                    default:
                        return [null,null,null,null];
                        break;
                }
            });
            return array($order->sum(0),$order->sum(1),$order->sum(2),$order->sum(3));
        }
        public function total_footer($data)
        {
            $res = [];
            $x = collect($data)->chunk(5)->values();
            foreach ($x->all() as $key => $value) {
                array_push($res,$value->values());
            }
            $x = collect($res);
            return [
                $x->sum(0),
                $x->sum(1),
                $x->sum(2),
                $x->sum(3),
                $x->sum(4)
            ];
        }
        public function sub_total_voting($data)
        {
            $cange_data = [];
            foreach ($data as $key => $value) {
                if(count($value['survey']) == 0)
                {
                    array_push($cange_data,["ss" => 0,"s" => 0,"cs" => 0,"ts" => 0,"sts" => 0]); // harus set default
                    unset($data[$key]);
                }else{
                    $xx = $this->cek_komentar(self::flipAndGroup(array_values($value['survey']['vote'])));
                    $cus = count($value['survey']['vote']);
                    array_push($cange_data,$xx);
                }
            }

            // dump(json_encode($cange_data));

            $col = collect($cange_data);
            $ss = $col->sum('ss');
            $ssp = ($ss == 0)?0:round(($ss/20)*100/count($data),2,PHP_ROUND_HALF_UP);
            $s = $col->sum('s');
            $sp = ($s == 0)?0:round(($s/20)*100/count($data),2,PHP_ROUND_HALF_UP);
            $cs = $col->sum('cs');
            $csp = ($cs == 0)?0:round(($cs/20)*100/count($data),2,PHP_ROUND_HALF_UP);
            $ts = $col->sum('ts');
            $tsp = ($ts == 0)?0:round(($ts/20)*100/count($data),2,PHP_ROUND_HALF_UP);
            $sts = $col->sum('sts');
            $stsp = ($sts == 0)?0:round(($sts/20)*100/count($data),2,PHP_ROUND_HALF_UP);
            $xxx = compact('ss','ssp','s','sp','cs','csp','ts','tsp','sts','stsp');
            return $xxx;
        }
        public function voting($data,$last = false)
        {
            $jml_customer = count($data);
            $ss = self::flipAndGroup(array_values($data));
            $all_vote = $this->cek_komentar($ss);
            
            return $this->vote_to_table($all_vote,$jml_customer,$last);
        }
        public function vote_to_table($all_vote,$jml_customer,$cus)
        {
            $r = '';
            $i = 0;
            $x = '';
                $border_dot = 'bd-b b-r-1x';
            if($cus){
                $border_dot = '';
            }
            $total_per = 0;
            
            
            foreach ($all_vote as $key => $value) {
                if($value == 0)
                {
                    $per = 0;
                }else {
                    $per = ($value/$jml_customer)*100;
                }
               
                $total_per = ($total_per + $per);

                $r .= "<td class='{$border_dot} bd-r' align='center'>{$value}</td>";
                $r .= "<td class='{$border_dot} b-r-1x' align='right'>{$this->n($per)}%</td>";
                
            }
            $r .= "<td class='{$border_dot} bd-r'  align='center'>{$jml_customer}</td>";
            $r .= "<td class='b-r-2x bd-b'>{$this->n($total_per)}%</td>";
            return $r;
        }
        protected function cek_komentar($dakom)
        {
            $ret = [];
            (isset($dakom['1']))?
                $ret['ss'] = count($dakom['1']):
                $ret['ss'] = 0;
            
            (isset($dakom['2']))?
                $ret['s'] = count($dakom['2']):
                $ret['s'] = 0;
            (isset($dakom['3']))?
                $ret['cs'] = count($dakom['3']):
                $ret['cs'] = 0;
            (isset($dakom['4']))?
                $ret['ts'] = count($dakom['4']):
                $ret['ts'] = 0;
            (isset($dakom['5']))?
                $ret['sts'] = count($dakom['5']):
                $ret['sts'] = 0;
            return $ret;
        }
        public function head_warna($warna){
            $w = explode(',',$warna);
            
            $wr = '';
            switch (count($w)) {
                case 1:
                    $this->head_warna = array_merge($w,[0,0,0]);
                    return "<td class=\"b-b-2x b-r-1x\"  width='50'>{$w[0]}</td>
                            <td class=\"b-b-2x b-r-1x\"  width='50'></td>
                            <td class=\"b-b-2x b-r-1x\" width='50'></td>
                            <td class=\"b-b-2x b-r-2x\"  width='50'></td>";
                    break;
                case 2:
                    $this->head_warna = array_merge($w,[0,0]);
                    return "<td class=\"b-b-2x b-r-1x\">{$w[0]}</td>
                            <td class=\"b-b-2x b-r-1x\">{$w[1]}</td>
                            <td class=\"b-b-2x b-r-1x\"></td>
                            <td class=\"b-b-2x b-r-2x\"></td>";
                    break;
                case 3:
                    $this->head_warna = array_merge($w,[0]);
                    return "<td class=\"b-b-2x b-r-1x\">{$w[0]}</td>
                            <td class=\"b-b-2x b-r-1x\">{$w[1]}</td>
                            <td class=\"b-b-2x b-r-1x\">{$w[2]}</td>
                            <td class=\"b-b-2x b-r-2x\"></td>";
                    break;
                case 4:
                    $this->head_warna = $w;
                    return "<td class=\"b-b-2x b-r-1x\">{$w[0]}</td>
                            <td class=\"b-b-2x b-r-1x\">{$w[1]}</td>
                            <td class=\"b-b-2x b-r-1x\">{$w[2]}</td>
                            <td class=\"b-b-2x b-r-2x\">{$w[3]}</td>";
                    break;
                default:
                    return "<td></td><td></td><td></td><td></td>";
                    break;
            }
        }
        public function main_data($data)
        {
            
            return self::group_by('nama_reg',$data);
           
        }
        protected static function group_by($key, $data) {
            $result = array();

            foreach($data as $val) {
                if(array_key_exists($key, $val)){
                    $result[$val[$key]][] = $val;
                }else{
                    $result[""][] = $val;
                }
            }

            return $result;
        }
        protected static function flipAndGroup($input) {
            $outArr = array();
            array_walk($input, function($value, $key) use (&$outArr) {
                if(!isset($outArr[$value]) || !is_array($outArr[$value])) {
                    $outArr[$value] = [];
                }
                $outArr[$value][] = $key;
            });
            return $outArr;
        }
        public static function n($text,$d = 2)
        {
            return number_format($text,$d);
        }
    }
    $h = new Kacrut;
@endphp
<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>PDF REPOT</title>
	<meta name="author" content="adi"/>
	<meta name="created" content="2021-02-01T07:17:44"/>
	<meta name="changedby" content="adi"/>
	<meta name="changed" content="2021-02-01T07:23:35"/>
	<meta name="AppVersion" content="14.0300"/>
	<meta name="DocSecurity" content="0"/>
	<meta name="HyperlinksChanged" content="false"/>
	<meta name="LinksUpToDate" content="false"/>
	<meta name="ScaleCrop" content="false"/>
    <meta name="ShareDoc" content="false"/>
    <link href="{{asset('css/pdf_report_survey.css')}}" rel="stylesheet">
    <script src='{{asset('plugins/Chart.js-1.1.1/Chart.min.js')}}' type="text/javascript"></script>
</head>
<body>
    <h3>SUMMARY RESPON PRODUK BARU</h3>
    <div class="user">
        {{strtoupper(Auth::user()->name)}}/{{date('d-m-Y',strtotime('now'))}}
    </div>
    <table cellspacing="0" border="0" width="100%">
        <colgroup width="80"></colgroup>
        <tr>
            <td>Merek</td>
            <td>: {{$head_tpl[0]['brand']}}</td>
        </tr>
        <tr>
            <td>Nama Motif</td>
            <td>: {{$head_tpl[0]['motif']}}</td>
        </tr>
        <tr>
            <td>Ukuran</td>
            <td>: {{$head_tpl[0]['ukuran']}}  </td>
        </tr>
    </table>
    
    @for ($i = 0; $i < 2; $i++)
        @if ($i == 1)
        <h3>SUMMARY RESPON PRODUK BARU DENGAN DATA PENJUALAN</h3>
        @endif
    <table cellspacing="0" border="0">
        <colgroup width="100"></colgroup>
        <colgroup width="145"></colgroup>
        <colgroup span="12" width="25"></colgroup>
        <colgroup span="5" width="70"></colgroup>
        <tr class="t-b">
            <td class="b-b-2x b-t-2x b-r-2x b-l-2x" rowspan=7 height="146" align="center" valign=middle>REGIONAL</td>
            <td class="b-b-2x b-t-2x b-r-2x b-l-2x" rowspan=7 align="center" valign=middle>NAMA DEPO</td>
            <td class="b-t-2x b-l-2x b-b-1x b-r-2x" colspan=12 rowspan=4 align="center" valign=middle>KOMENTAR BARANG</td>
            <td class="b-t-2x b-r-2x b-b-1x" colspan=4 align="center" valign=middle>PERKIRAAN QUANTITY ORDER PER ITEM SETIAP BULAN (BOX)</td>
            @if ($i == 1)
                <th rowspan=7 class="b-b-2x b-t-2x b-r-2x b-l-2x"><div><span>Konversi Data Penjualan</span></div></th>
            @endif
        </tr>
        <tr>
            <td class="t-b b-r-1x b-b-1x" colspan=2 rowspan=4 align="center" valign=middle>{{$head_tpl[0]['brand']}}</td>
            <td class="b-b-1x f-xs" align="left" valign=middle>Sangat Suka</td>
            <td class="b-b-1x b-r-2x f-xs" align="left" valign=middle>: &ge; 200 Box</td>
        </tr>
        <tr>
            <td class="b-b-1x f-xs" align="left" valign=middle>Suka</td>
            <td class="b-b-1x b-r-2x f-xs" align="left" valign=middle>:100-199 Box</td>
        </tr>
        <tr>
            <td class="b-b-1x f-xs" align="left" valign=middle>Cukup Suka</td>
            <td class="b-b-1x b-r-2x f-xs" align="left" valign=middle>:50-99 Box</td>
        </tr>
        <tr>
            <td class="t-b b-r-1x  b-b-2x" colspan=2 rowspan=3 align="center" valign=middle>SANGAT SUKA</td>
            <td class="t-b b-b-2x b-r-1x" colspan=2 rowspan=3 align="center" valign=middle>SUKA</td>
            <td  class="t-b b-b-2x b-r-1x" colspan=2 rowspan=3 align="center" valign=middle>CUKUP SUKA</td>
            <td class="t-b b-b-2x b-r-1x" colspan=2 rowspan=3 align="center" valign=middle>TIDAK SUKA</td>
            <td class="t-b b-b-2x b-r-1x" colspan=2 rowspan=3 align="center" valign=middle>SANGAT TIDAK SUKA</td>
            <td class="t-b b-b-2x b-r-2x" colspan=2 rowspan=3 align="center" valign=middle>%</td>
            <td class="b-b-1x f-xs" align="left" valign=middle>Tidak Suka</td>
            <td class="b-b-1x b-r-2x f-xs" align="left" valign=middle>:0-49 Box</td>
        </tr>
        <tr>
            <td class="b-b-1x b-r-2x" colspan=4 align="center" valign=middle>JIKA &quot;YA&quot; MAU ORDER, HARUS DIISI DENGAN QUANTITY ORDER</td>
        </tr>
        <tr align="center">
            {!! $h->head_warna($head_tpl[0]['warna']) !!}
        </tr>
        @php($tot_footer = [])
        @php($gran_penjualan = [])
                @php($tot_order = [null,null,null,null])
                @foreach ($h->main_data($summary_region) as $key => $item)
                    <tr>
                        <td rowspan="{{count($item)+4}}" class="b-l-2x b-b-2x b-r-2x t-c">{{$key}}</td>
                        {{-- Subtotal order variable --}}
                        @php($sub_tot_order = [null,null,null,null])
                        @php($sub_tot_penjualan = [])
                        @foreach ($item as $depo)
                            <tr>
                                <td class="b-r-2x bd-b">{{$depo['nama_depo']}}</td>
                                    @if (count($depo['survey']) == 0)
                                        <td colspan="16" class="b-t-1x b-r-2x b-b-1x" align="center" valign=middle style="background-color: honeydew">0</td>
                                    @else
                                {!! $h->voting($depo['survey']['vote'],$loop->last) !!}
                                @foreach($h->order($depo['survey']['order']) as $order)
                                    {{-- @php($class_order = '')                                     --}}
                                    @switch($loop->index)
                                        @case(0)
                                            @php($sub_tot_order[0] += $order) 
                                            @break
                                        @case(1)
                                            @php($sub_tot_order[1] += $order) 
                                            @break
                                        @case(2)
                                            @php($sub_tot_order[2] += $order) 
                                            @break
                                        @case(3)
                                            @php($sub_tot_order[3] += $order)  
                                        @break
                                    @endswitch
                                    {{-- nilai order --}}
                                    @if ($loop->index == 3)
                                        <td align="center" valign=middle sdval="0" sdnum="1033;" class="bd-b b-r-2x">{{$order}}</td>
                                    @else
                                        <td align="center" valign=middle sdval="0" sdnum="1033;" class="bd-b b-r-1x">{{$order}}</td>
                                    @endif
                                    
                                @endforeach
                                {{-- data penjualan --}}
                                    @if ($i == 1)
                                        @isset($penjualan[$key])
                                            @php(array_push($sub_tot_penjualan,$penjualan[$key][$depo['nama_depo']]))
                                            <td align="right" valign=middle class="b-t-1x b-r-2x b-b-1x">{{number_format($penjualan[$key][$depo['nama_depo']])}}</td>
                                        @endisset
                                        @empty($penjualan[$key])
                                            @php(array_push($sub_tot_penjualan,0))
                                            <td align="right" valign=middle class="b-t-1x b-r-2x b-b-1x">0</td>
                                        @endempty
                                    @endif
                                @endif
                            </tr>
                        @endforeach
                    </tr>
                    <tr>
                        <td class="b-t-1x b-r-2x" align="right">Sub Total</td>
                        @php($tot_sub = 0) @php($per_sub = 0) @php($persub_arr = [])
                        @foreach ($h->sub_total_voting($item) as $it)
                            @if ($loop->index%2 == 0)
                                @php($tot_sub = $tot_sub+$it)
                                <td class="bd-r b-t-1x"  align='center'>{{$it}}</td>
                            @else
                                @php($per_sub = $per_sub+$it)   
                                <td class="b-r-1x b-t-1x" align='right'>{{$h->n($it)}}%</td>
                                @php(array_push($persub_arr,$it))
                                @php(array_push($tot_footer,$it))
                            @endif
                            
                        @endforeach
                        
                        {{-- persentase total --}}
                        <td class="bd-r b-t-1x"  align='center'>{{$tot_sub}}</td>
                        <td class="b-r-2x b-t-1x">{{$h->n($per_sub)}}%</td>
                        
                        @foreach ($sub_tot_order as $sub_order)
                            
                            @switch($loop->index)
                                @case(0)
                                    @php($tot_order[0] += $sub_order)
                                    @break
                                @case(1)
                                @php($tot_order[1] += $sub_order)
                                    @break
                                @case(2)
                                @php($tot_order[2] += $sub_order)
                                    @break
                                @case(3)
                                @php($tot_order[3] += $sub_order)
                                    @break
                            @endswitch
                            @if ($loop->index == 3)
                            <td class="b-t-1x b-r-2x" align="center">{{$sub_order}}</td>
                            @else
                            <td class="b-t-1x b-r-1x " align="center">{{$sub_order}}</td>
                            @endif
                            
                        @endforeach
                        @if ($i == 1)
                            {{-- sub total penjualan --}}
                            @php(array_push($gran_penjualan,array_sum($sub_tot_penjualan)))
                            <td rowspan="3" class="b-r-2x bg-tot t-b" align="center">{{number_format(array_sum($sub_tot_penjualan))}}</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="bg-tot b-r-2x b-t-1x" align="right">Sub Total X Nilai Skor</td>
                        @php($idx = 5)
                        @php($nil_motif = 0)
                        @foreach ($persub_arr as $item)
                        @php($x = round(($item*($idx--))/100,2,PHP_ROUND_HALF_UP))
                            <td colspan="2" class="t-b bg-tot b-t-1x b-r-1x" align="center">{{$h->n($x)}}</td>
                            @php($nil_motif = $nil_motif + $x)                            
                        @endforeach
                            <td colspan="2" class="bg-tot b-t-1x b-r-2x" align="center"> - </td>
                            <td colspan="4" class="b-t-1x b-r-2x" align="center">{{$h->n(array_sum($sub_tot_order),0)}}</td>
                    </tr>
                    <tr>
                        <td class="bg-tot b-r-2x b-t-1x b-b-2x" align="right">Nilai Motif</td>
                        <td colspan="12" class="bg-tot b-r-2x b-t-1x b-b-2x" align="center">{{$nil_motif}}</td>
                        <td colspan="4" class="b-t-1x b-r-2x b-b-2x" align="center">-</td>
                    </tr>
                @endforeach
                <tfoot style="font-weight: bold">
                    
                    @php($chart_vote = []) @php($chart_order = [])
                    <tr>
                        <td colspan="2" class="b-t-2x b-r-2x b-l-2x" align="right">Total</td>
                        @foreach ($h->total_footer($tot_footer) as $item)
                            @php($tot = round($item/7,2))
                            <td colspan="2" class="b-r-1x" align="center">{{$h->n($tot)}}%</td>
                            @php(array_push($chart_vote,$tot))
                        @endforeach
                        <td colspan="2" class="b-r-2x" align="center">100%</td>
    
                        @foreach ($tot_order as $tot_ord)
                            @if($loop->index == 3)
                            <td class="b-r-2x" align="center">{{$tot_ord}}</td>
                            @else
                            <td class="b-r-1x" align="center">{{$tot_ord}}</td>
                            @endif
                            
                            @php(array_push($chart_order,$tot_ord))
                        @endforeach
                        {{-- gran total penjualan --}}
                        @if($i == 1)
                        <td align="center" rowspan="4" class="b-r-2x b-b-2x b-t-2x">{{number_format(array_sum($gran_penjualan))}}</td>
                        @endif
                    </tr>
                    <tr>
                        <td colspan="2" class="b-r-2x b-t-1x b-l-2x" align="right">Nilai Skor</td>
                        <td colspan="2" class="b-t-1x b-r-1x" align="center">X 5</td>
                        <td colspan="2" class="b-t-1x b-r-1x" align="center">X 4</td>
                        <td colspan="2" class="b-t-1x b-r-1x" align="center">X 3</td>
                        <td colspan="2" class="b-t-1x b-r-1x" align="center">X 2</td>
                        <td colspan="2" class="b-t-1x b-r-1x" align="center">X 1</td>
                        <td colspan="2" class="b-t-1x b-r-2x" align="center">-</td>
                        <td colspan="3" rowspan="3" class="b-t-1x b-r-1x b-l-2x b-b-2x" align="center">Jumlah Quantity order/box</td>
                        <td rowspan="3" class="b-t-1x b-r-2x b-b-2x" align="center">{{number_format(array_sum($tot_order))}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="bg-tot b-r-2x b-l-2x b-t-1x" align="right">Total X Nilai Skor</td>
                        @php($it = 5)
                        @php($grand_tot = 0)
                        @foreach ($h->total_footer($tot_footer) as $item)
                            @php($xx = round(($item/7)*$it--/100,2))
                            <td colspan="2" class="bg-tot b-t-1x b-r-1x" align="center">{{$h->n($xx)}}</td>
                            @php($grand_tot = $grand_tot + $xx)
                        @endforeach
                        <td colspan="2" class="bg-tot b-t-1x" align="center"> - </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="bg-tot b-r-2x b-l-2x b-b-2x b-t-1x" align="right">Grand Total (Skala 5)</td>
                        <td colspan="12" class="bg-tot b-b-2x b-t-1x" align="center"><span style="color: red">{{$grand_tot}}</span></td>
                    </tr>
                </tfoot>
    </table>
    <!-- ************************************************************************** -->
    <p style="page-break-after: always;" />
    @endfor

    <div>
        <table width="100%" class="page-break" style="padding-top:500px;">
            <tr style="text-align: center">
                <td class="t-b">Komentar Barang</td>
                <td class="t-b">Quantity Order/Barang</td>
            </tr>
            <tr>
                <td class="b-t-1x b-l-1x b-b-1x b-r-1x" style="padding: 5px;"><canvas id="voting" width="500" height="300"></canvas></td>
                <td class="b-t-1x b-l-1x b-b-1x b-r-1x" style="padding: 5px;"><canvas id="order" width="500" height="300"></canvas></td>
            </tr>
            <tr>
                <td colspan="2" class="b-t-1x b-l-1x b-b-1x b-r-1x">
                    <p>
                        <strong>Kesimpulan:</strong> 
                        {!! $kesimpulan !!}
                    </p>
                </td>
            </tr>
        </table>
        <br />
        <table width="100%">
            <tr>
                <td class="t-c">Disetujui,</td>
                <td colspan="3" class="t-c">Diketahui,</td>
                <td colspan="3" class="t-c">Diajukan,</td>
            </tr>
            <tr class="t-b">
                <td class="t-c-pt-100">(Lin Chien Ping)</td>
                <td class="t-c-pt-100">(Sudarsa)</td>
                <td class="t-c-pt-100">(Suhartono)</td>
                <td class="t-c-pt-100">({{$diajukan ?? '.............'}})</td>
                <td class="t-c-pt-100">(Suripto)</td>
                <td class="t-c-pt-100">(Rengga)</td>
                <td class="t-c-pt-100">(Erikbal)</td>
            </tr>
        </table>
    </div>
    <script>
        // extend 
        Chart.types.Bar.extend({
            name: 'BarOverlay',
            draw: function (ease) {

                // First draw the main chart
                Chart.types.Bar.prototype.draw.apply(this);
                var ctx = this.chart.ctx;

                ctx.closePath();
            }
        });
        
        const warna = @json($h->head_warna);
        const dataChart = @json($chart_vote);
        const dataOrder = @json($chart_order);
        var barData = {
                    labels : ["Sangat Suka","Suka","Cukup Suka","Tidak Suka","Sangat Tidak Suka"],
                    datasets : [
                            {
                                label: "My Second dataset",
                                fillColor: "rgba(255, 0, 0,0.5)",
                                strokeColor: "rgba(255, 0, 0,0.8)",
                                highlightFill: "rgba(255, 0, 0,0.75)",
                                highlightStroke: "rgba(255, 0, 0,1)",
                                data : dataChart
                            }
                        ]
                    }
                    
        // get bar chart canvas
        var income1 = document.getElementById("voting").getContext("2d");
        // draw bar chart
        new Chart(income1).BarOverlay(barData);
        
                    

        var orderData = {   
                    labels : warna,
                    datasets : [
                            {
                                label: "My Second dataset",
                                fillColor: "rgba(0, 0, 254,0.5)",
                                strokeColor: "rgba(0, 0, 254,0.8)",
                                highlightFill: "rgba(0, 0, 254,0.75)",
                                highlightStroke: "rgba(0, 0, 254,1)",
                                data : dataOrder
                            }
                        ]
                    }
        // option
        const option = {
            scaleBeginAtZero : true,
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.05)",
        }
        // get bar chart canvas
        var income2 = document.getElementById("order").getContext("2d");
        // draw bar chart
        new Chart(income2).Bar(orderData,option);


                    // var ctx = document.getElementById("order").getContext("2d");
                    // var myBarChart = new Chart(ctx, {
                    //     type: "bar",
                    //     data: orderData,
                    // });
    </script>
</body>
</html>

{{-- @dump($head_tpl[0]['warna']) --}}