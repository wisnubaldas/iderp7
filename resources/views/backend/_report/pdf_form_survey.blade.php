<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Survey</title>
    <style>
body {
    font-size: 10px;
}
table.tbl_form {
  font-family: "Arial", Helvetica, sans-serif;
  text-align: center;
}
.tbl_form, table.tbl_form td, table.tbl_form th {
    border: 2px solid #555555;
    padding: 2px 10px;
    border-collapse: collapse;
}
table.tbl_form tbody td {
  font-size: 10px;
  font-weight: bold;
  color: #000000;
}
table tbody td span {
  font-weight: bold;
}
.page-break {
    page-break-after: always;
}
.panjang-vote{
    width: 50px;
}
.order-vote{
    width: 50px;
}
.ttd-vote{
    text-align: center;
    width: 50%;
}
.col-ttd{
    table-layout: fixed ;
    width: 100% ;
    
}
.col-ttd > td{
    width: 30% ;
    vertical-align: 'bottom'; 
    color: blue; 
    background-color: lightyellow;
    height: 20em;
    vertical-align: top;
}

.col-ttd .ttd-center{
    text-align: center;
    /* height: 20em; */
    vertical-align: top;
}
.ttd-center p span{
    display: block;
    /* line-height: 28px; */
    /* margin: 50px 0; */
    /* padding-bottom: 100px; */
    /* line-height: 20em; */
    /* position: relative; */
    /* top: 0; */
}
    </style>
</head>

<body>
    @php
      $form = $m->depo_survey->customer;
        $tables = [];
        $survey_no = $form['survey_no'];
        $depo = $form['nama_depo'];
        $brand = $form['brand'];
        $ukuran = $form['ukuran'];
        $motif = $form['motif'];
        $warna = explode(',',$form['warna']);  
        foreach ($form['form_respon'] as $v) {
            $sales = $v['sales'];
            $form_id = $v['form_id'];
            $customer = collect($v['customer'])->chunk(5);
            foreach ($customer as $cus) {
                $cus = $cus->values()->toArray();
                array_push($tables,compact('survey_no','depo','brand','ukuran','motif','warna','sales','form_id','cus'));
            }
        }
    @endphp
    @foreach ($tables as $item)
    @if (!$loop->last)
    <div class="page-break">
    @else
    <div>
    @endif
        <table style="width: 100%; height=100%;" >
            <tbody>
                <tr>
                    <td colspan="6">
                        <span style="font-weight: bold">FORM RESPON PRODUK BARU - {{ $item['form_id']}}
                            <span style="padding-left:91em" >
                                {{\Carbon\Carbon::now()->format('d F Y')}}
                            </span>
                            <hr>
                            Survey No : {{$item['survey_no']}}
                        </span> 
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">Depo: <span>{{$item['depo']}}</span></td>
                    <td style="width: 100px">Merek: <span>{{$item['brand']}}</span></td>
                    <td style="width: 100px">Nama Motif/Kode Barang: <span>{{$item['motif']}}</span></td>
                </tr>
                <tr>
                    <td>Nama Sales: <span>{{ucwords($item['sales'])}}</span></td>
                    <td>Ukuran: <span>{{$item['ukuran']}}</span></td>
                    <td style="border-bottom: 1px solid black;">Perkiraan Harga Jual: <span>Rp. {{number_format($m->harga)}}</span></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table style="width: 100%;" class="tbl_form">
                            <thead>
                                <tr>
                                    <td rowspan="7" >No</td>
                                    <td rowspan="7">Nama Toko</td>
                                    <td colspan="5">Komentar Barang (berikan tanda check di bawah ini)</td>
                                    <td colspan="2" >Apakan Anda Mau Order</td>
                                    <td colspan="4" >Perkiraan Quantity Order / Item Setiap Bulan (BOX)</td>
                                    <td rowspan="7">Saran Dan Stempel Toko</td>
                                </tr>
                                <tr>
                                    <td rowspan="6" class="panjang-vote">SANGAT SUKA</td>
                                    <td rowspan="6" class="panjang-vote">SUKA</td>
                                    <td rowspan="6" class="panjang-vote">CUKUP SUKA</td>
                                    <td rowspan="6" class="panjang-vote">TIDAK SUKA</td>
                                    <td rowspan="6" class="panjang-vote">SANGAT TIDAK SUKA</td>
                                    <td rowspan="6" class="order-vote">YA</td>
                                    <td rowspan="6" class="order-vote">TIDAK</td>
                                    <td rowspan="4" colspan="2">{{$item['brand']}}</td>
                                    <td colspan="2">Sangat Suka > 100 Box</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Suka 50 - 99 Box</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Cukup Suka 1 - 49 Box</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Tidak Suka 0 B Box</td>
                                </tr>
                                <tr>
                                    <td colspan="4">Jika Ya mau order harus di isi dengan quantity order</td>
                                </tr>
                                <tr>
                                    @foreach ($item['warna'] as $color)
                                        <td class="panjang-vote">{{$color}}</td>
                                    @endforeach
                                </tr>
                                
                            </thead>
                            <tbody>
                                @foreach ($item['cus'] as $customer)
                                    <tr style="height: 10em; text-align:left">
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$customer['id']}}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="col-ttd">
            <tr>
                <td>
                    Note:
                    <ul>
                        <li>Survey Harus Sebanyak 20 Toko</li>
                        <li>Survey ke toko yang sudah menjadi pelanggan tetap Depo & toko tersebut juga 
                            banyak menjual merek kompetitor yang setara/sekelas dengan merek 
                            produk baru yang di survey ini.
                        </li>
                        <li>Nama toko harus sesuai dengan database accura (data pelanggan depo)</li>
                        <li>Yang dimaksud item adalah per warna</li>
                        <li>Kepala Depo wajib menuliskan perkiraan harga jual / box di form ini sebelum salesman melakukan 
                            survey produk baru ini
                        </li>
                        <li>Form respon produk baru ini harus di isi lengkap</li>
                    </ul>
                </td>
                <td class="ttd-center">
                    <p><span>Di susun oleh:<br></span>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    <span>( {{strtoupper($item['sales'])}} )</span></p>
                </td>
            </tr>
        </table>
    </div>
    @endforeach
</body>
</html>
