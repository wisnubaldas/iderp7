@extends('backend.app')

@section('content')
    <x-survey.report-form id="report" class="row" :data-include="$data_include">
        <x-slot name="error_message">
            <div id="error-message">

            </div>
        </x-slot>
    </x-survey.report-form >
   

    
@endsection
@push('css')
<link href="{{asset('css/plugins/summernote/summernote-bs4.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
<link href="{{url('css/plugins/daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/footable/footable.core.css')}}" rel="stylesheet">
@endpush

@push('script')
<!-- Date range use moment.js same as full calendar plugin -->
<script src="{{asset('js/plugins/fullcalendar/moment.min.js')}}"></script>

<!-- Date range picker -->
<script src="{{asset('js/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('js/plugins/summernote/summernote-bs4.js')}}"></script>
<script src="{{asset('js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
<script src="{{asset('js/plugins/dataTables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/plugins/footable/footable.all.min.js')}}"></script>
{{-- <script src="{{asset('js/plugins/chartJs/Chart.min.js')}}"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>

    <script>
         $(document).ready(function(){
            $('.footable').footable();
            $('.footable2').footable();
        });

        Chart.plugins.unregister(ChartDataLabels); // disable dulu plugin chart js nya

        var l = $('.ladda-button' ).ladda();
        // declar variable
        let fn = {}; fn.v = {}; fn.c = {};
        // setup request
        fn.getRequest = function(p,result,always)
        {
            axios.get(p.url,{params:p.data})
            .then(function(response){
                return result(response.data);
            })
            .catch(function(error){
                console.error(error);
            })
            .then(function(a){
                return always(a);
            })
        }
        // event handler dom
        fn.eh = function(el,event,result)
        {
            $(el).on(event,function(a){
                return result(a)
            })
        }
        
        // jquery app
        jQuery(function(){
            fn.eh('#cari','click',function(a){
                l.ladda( 'start' );
                // handle form
                const form = {}
                      form.motif = $('input[name="motif"]').val();
                      form.kode_barang =  $('input[name="kode_barang"]').val();
                      form.created_at = $('input[name="daterange"]').val();

                const req = {
                    url:'/report/survey/report/get_motif',
                    data:form
                }
                // call request 
                fn.getRequest(req,function(a){
                    if(typeof a === 'string')
                    {
                        $('#error-message').html(a);
                    }
                    if(typeof a === 'object')
                    {
                        $('#summary').removeClass('invisible').addClass('visible');

                        let idx = 1;
                        for (const key in a) {
                            if (a.hasOwnProperty(key)) {
                                const element = a[key];
                                const id = '#tab-'+idx++;
                                $(id).html(element)
                            }
                        }
                    }
                },function(b){
                    l.ladda('stop');
                    $('#report-form')[0].reset()
                })
            });
        });

    </script>
    <script>
        jQuery(function(a){
            $("#cari").mouseenter(function() {
                $(this).find('i').addClass('text-primary')
            }).mouseleave(function() {
                $(this).find('i').removeClass('text-primary')
            });
        })
    
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format(
            'MMMM D, YYYY'));
        $('#reportrange').daterangepicker({
            format: 'DD/MM/YYYY',
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            dateLimit: {
                days: 60
            },
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-warning',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                    'October', 'November', 'December'
                ],
                firstDay: 1
            }
        }, function (start, end, label) {
            // console.log(start, end, label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        });
    </script>
@endpush

@push('css')
<style>
    .bg-navy {
        background-color:#7979FF
    .navy-pastel {
        color:#7979FF
    }
    .border--navy-pastel {
        border-color:#7979FF
    }
    .fill-navy-pastel {
        fill:#7979FF
    }
    .stroke-navy-pastel {
        stroke:#7979FF
    }
</style>
@endpush