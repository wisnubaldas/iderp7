@extends('backend.app')
@section('content')
<div class="ibox">
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-b-md">
                    <a href="/report/survey/status_grid" class="btn btn-primary btn-sm float-right">Back</a>
                    <h2><span class="font-bold text-primary">{{$data->survey_no}}</span></h2>
                    <hr class="border-bottom border-size-lg" />
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Status:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1">
                            <span class="label label-primary">{{$data->status->status}}</span> <br>
                            <span class="font-italic text-danger">{{$data->status->desc}}</span>
                        </dd>
                    </div>
                </dl>
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Created by:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1">{{ $data->sales_name }}</dd>
                    </div>
                </dl>
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Tanggal Survey:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1"> {{ $data->tgl->format('d F Y') }}</dd>
                    </div>
                </dl>
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Depo:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1"><a href="#" class="text-navy"> {{ $data->depo_detail->id_depo }} /
                                {{ $data->depo_detail->nama_depo }}</a></dd>
                    </div>
                </dl>
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Ceramic:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1">
                            Brand: <span class="text-navy">{{$data->brand}}</span>/
                            Motif: <span class="text-navy">{{$data->motif}}</span>/
                            Ukuran: <span class="text-navy">{{$data->ukuran->title}}</span>
                        </dd>
                    </div>
                </dl>

            </div>
            <div class="col-lg-6" id="cluster_info">

                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Last Updated:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1">{{$data->updated_at}}</dd>
                    </div>
                </dl>
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Created:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1"> {{$data->created_at}}</dd>
                    </div>
                </dl>
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Note:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1"> 
                            <p>{{ $data->depo_survey->note }}</p>
                        </dd>
                    </div>
                </dl>
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Documents:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1"> 
                        <div class="lightBoxGallery">
                            @foreach($data->image as $i)
                                <a href="{{ $i->image }}" title="{{ $i->image_name }}" data-gallery="">
                                    <img class="img-thumbnail" src="{{ $i->image }}" alt="{{ $i->image_name }}" />
                                </a>
                            @endforeach
                            <div id="blueimp-gallery" class="blueimp-gallery">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>
                        </div>
                            
                        </dd>
                    </div>
                </dl>

            </div>
        </div>

        <div class="row m-t-sm">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li><a class="nav-link active" data-toggle="tab" href="#tab-3">
                                <i class="fa fa-laptop"></i>
                                Customer
                            </a>
                        </li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-4">
                            <i class="fa fa-desktop"></i>
                            Customer Depo
                            </a>
                        </li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-5">
                            <i class="fa fa-database"></i>
                            Customer Survey
                        </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-3" class="tab-pane active">
                            <div class="panel-body">
                                <table class="table table-hover datatables">
                                    <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        {{-- @foreach($customer as $i)
                                        <tr>
                                            <td>{{$i->id_toko}}</td>
                                            <td>{{$i->nama_toko}}</td>
                                        </tr>
                                        @endforeach --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <table class="table table-hover datatables">
                                    <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        {{-- @foreach($customer_depo as $i)
                                        <tr>
                                            <td>{{$i->id_toko}}</td>
                                            <td>{{$i->nama_toko}}</td>
                                        </tr>
                                        @endforeach --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="tab-5" class="tab-pane">
                            <div class="panel-body">
                                <table id="customer-depo" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Status Survey</th>
                                            <th>Order</th>
                                            <th>Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- @foreach($customer_survey as $i)
                                        <tr>
                                            <td>{{$i['id_toko']}}</td>
                                            <td>{{$i['nama_toko']}}</td>
                                            <td>{{$i['survey']}}</td>
                                            <td>{{$i['order']}}</td>
                                            <td>{{$i['qty']}}</td>
                                        </tr>
                                        @endforeach --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link href="{{asset('css/plugins/blueimp/css/blueimp-gallery.min.css')}}" rel="stylesheet">
@endpush
@push('script')
<!-- blueimp gallery -->
<script src="{{asset('js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>
<script>

$('.datatables').DataTable({
        pageLength: 7,
        // // paging: false,
        // processing: true,
        lengthChange: false,
        searching:false,
        responsive: true,
});
</script>
@endpush
