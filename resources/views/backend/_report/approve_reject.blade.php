@extends('backend.app')
@section('content')
<div class="card">
    <div class="card-header">
    <h3>{{$data['title']}} {{$data['survey']->survey_no}}</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6">
                @if($data['status'] == 'a')
                    <form role="form" action="/report/survey/approve/{{$data['survey']->id}}?id={{Str::random(40)}}" method="POST">
                        @csrf
                            <div class="form-group">
                                <label>Kode Barang</label>
                                <input type="text" name="kode_barang"  class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Perkiraan Harga</label>
                                <input type="text" name="p_harga"  class="form-control">
                            </div>
                            <div class="btn-group" >
                                <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit">
                                    <strong>Save</strong></button>
                                <a class="btn btn-sm btn-info float-right m-t-n-xs" href="{{url('report/survey/status_grid')}}">
                                        <strong>Back</strong></a>
                            </div>
                    </form>
                @endif
                @if($data['status'] == 'r')
                    <form role="form" action="/report/survey/approve/{{$data['survey']->id}}?id={{Str::random(50)}}" method="POST">
                        @csrf
                            <div class="form-group">
                                <label>Catatan</label>
                                <textarea type="text" name="note"  class="form-control"></textarea>
                            </div>
                            <div class="btn-group" >
                                <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit">
                                    <strong>Save</strong></button>
                                <a class="btn btn-sm btn-info float-right m-t-n-xs" href="{{url('report/survey/status_grid')}}">
                                        <strong>Back</strong></a>
                            </div>
                    </form>
                @endif
            </div>
            <div class="col-lg-6">
                <h3>Legend</h3>
                <div class="row">
                    <div class="widget style1 navy-bg col-6">
                        <h3>Brand:
                            <small>{{$data['survey']->brand}}</small>
                        </h3>
                        <h3>Depo:
                            <small>{!! $data['survey']->depo_detail->nama_depo !!}</small>
                        </h3>
                        <h3>Motif:
                            <small>{!! $data['survey']->motif !!}</small>
                        </h3>
                    </div>
                    <div class="widget style1 navy-bg col-6">
                        <h3>Perkiraan Harga:
                            <small>{{$data['survey']->p_harga}}</small>
                        </h3>
                        <h3>Kode Barang:
                            <small>{!! $data['survey']->kode_barang !!}</small>
                        </h3>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection
