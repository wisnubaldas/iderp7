<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Chart.js demo</title>

{{-- <script src='http://netdna.webdesignerdepot.com/uploads7/easily-create-stunning-animated-charts-with-chart-js/Chart.min.js' type="text/javascript"></script> --}}
{{-- <script src='{{asset('plugins/chart-pdf/chartJs.min.js')}}' type="text/javascript"></script> --}}
{{-- <script src='{{asset('plugins/chart-pdf/chartjs-plugin-datalabels.js')}}' type="text/javascript"></script> --}}
<script src="http://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
<script src="http://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
</head>
<body>
<h1>Chart Ooooy...</h1>
<canvas id="buyers" width="600" height="400"></canvas>

<script type="text/javascript">
 
        let barChartData = {
            labels:['dasd','dasda','asdadas','asdasd'],
            datasets:[
                
                {
                    label: 'Komentar Barang',
                    backgroundColor:'rgba(255,0,51,0.7)',
                    data: [1,2,3,4],
                    datalabels: {
                                    formatter: function(value, context) {
                                        return value+ '%';
                                    },
                                    color: '#FFFF00',
                                    font:{
                                        weight:'bold',
                                        size:'15'

                                    }
                                }
                }
            ]
        }
        let ctz = document.getElementById('buyers').getContext("2d");
        let myLineChart = new Chart(ctz, {
                    type: 'bar',
                    data: barChartData,
                    plugins: [ChartDataLabels],
                    options: {
                        title: {
                            display: true,
                            text: 'sdasdasdasdasd'
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                        responsive: true
                    }
        });
    
</script>
</html>
