@extends('backend.app')

@section('content')
<div class="ibox">
    <div class="ibox-title">
        <h2>FS001 <small>Survey Produk Baru</small></h2>
    </div>
    <section id="survey-form">
        <form id="form" action="/report/survey/submit" method="POST">
            @csrf
            <div class="ibox-content">
                @if ($errors->any())
                    <x-survey.error :err="$errors->all()" />
                @endif
               
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Pilih Tanggal</label>
                            <input id="datepicker" name="tgl" class="form-control required" placeholder="Date survey"
                                data-provide="datepicker" data-date-autoclose="true" type="text">
                        </div>
                        <div class="form-group">
                            <label>Pilih Tanggal Produksi</label>
                            <input id="datepicker" name="tgl_produksi" class="form-control required" placeholder="Tanggal Produksi"
                                data-provide="datepicker" data-date-autoclose="true" type="text">
                        </div>
                        <div class="form-group">
                            <label>Motif</label>
                            <input id="motif" name="motif" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Tipe Warna</label>
                            <br>
                            <input id="tag-input" name="warna" class="form-control " type="text" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Brand</label>
                            <select id="brand" name="brand" type="text" class="form-control required">
                                <option value="" selected>### Select Brand ###</option>
                                @foreach ($brand as $item)
                                    <option value="{{strtoupper($item->nama_brand)}}">{{strtoupper($item->nama_brand)}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Depo *</label>
                            <select name="depo" id="depo" type="text" class="form-control required"
                                onchange="selectDepo(this)">
                                <option selected value="">### Select Depo ###</option>
                                @foreach ($depo as $i)
                                    <optgroup label="{{strtoupper($i->nama_reg)}}">
                                        @foreach ($i->depo_regional as $item)
                                            <option value="{{$item->id_depo}}">{{ $item->nama_depo }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Ukuran</label>
                            <select id="ukuran" name="ukuran" type="text" class="form-control required">
                                <option selected value="">### Select Ukuran ###</option>
                                @foreach ($ukuran as $i)
                                <option value="{{$i->id}}">{{ $i->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Harga</label>
                            <input id="harga" name="harga" type="text" class="form-control required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kode Barang</label>
                            <input id="kode_barang" name="kode_barang" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Upload Customer</label>
                            <div class="custom-file">
                                <input id="customer_file" type="file" class="custom-file-input" name="customer_file">
                                <label for="customer_file" class="custom-file-label">Choose file...</label>
                            </div> 
                        </div>
                        <div class="form-group">
                            <select id="customer" name="customer[]" type="text" class="form-control dual_select" multiple>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <button class="btn btn-white btn-sm" type="button">Cancel</button>
                        <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                    </div>
                </div>
            </div>
        </form>
    </section>

</div>
@endsection

@push('before-script')
    @include('parsial_script.survey')
@endpush

@push('script')
<!-- Select2 -->
<script src="{{asset('js/plugins/sweetalert/sweetalert.min.js')}}"></script>
<!-- Tags Input -->
<script>
    function selectDepo(a) {
        $('#customer').bootstrapDualListbox({
            nonSelectedListLabel: 'Non-selected',
            selectedListLabel: 'Selected',
            preserveSelectionOnMove: 'moved',
            moveOnSelect: false,
            selectorMinimalHeight: 160
        });
        axios.get('/report/survey/getDepo', {
                params: {
                    id: a.value
                }
            })
            .then(function (response) {
                $("#customer").children().remove();
                for (const i of response.data) {
                    const x = `<option value="${i.id_toko}">${i.id_toko} - ${i.nama_toko}</option>`;
                    $('#customer').append(x);
                }
                $('#customer').bootstrapDualListbox('refresh', true);

            })
            .catch(function (error) {
                console.log(error)
            })
    }
    jQuery(function () {
        // $('#tag-input').tagsinput('removeAll');
        $('#tag-input').tagsinput({
            tagClass: 'btn-danger btn-xs',
            maxTags: 4,
            confirmKeys: [32,44],
            trimValue: true
        });

    });

</script>
@endpush
@push('css')
<link href="{{asset('css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
<style>
    .swal-overlay {
        background-color: rgba(43, 165, 137, 0.45);
    }
</style>
@endpush
