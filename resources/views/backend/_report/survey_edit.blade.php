@extends('backend.app')
@section('content')
<div class="ibox">
    <div class="ibox-content">
        <div id="tab-5" class="tab-pane">
            <div class="ibox-title">
                <h5>
                    Nomer Survey: {{$survey->depo_survey->customer['survey_no']}}
                </h5>
                <div class="pull-right">
                    <a href="{{url('/report/survey/status_grid')}}" class="btn btn-danger">Kembali</a>
                    <button class="btn btn-primary btn-outline" onclick="saveEdit()"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                         Save</button>

                </div>
            </div>
            <div class="panel-body">
                <div class="col-lg-12 no-padding no-marggin">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Edit Voting Survey
                        </div>
                        <div class="panel-body">
                            <div id="edit-vote" class="row"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 no-padding no-marggin">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            Edit Order Survey
                        </div>
                        <div class="panel-body">
                            <div id="edit-order" class="row"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 no-padding no-marggin">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Edit Image Survey
                            <span class="pull-right">
                                <div class="custom-file">
                                    <input id="file" name="file" type="file" class="custom-file-input form-control-sm">
                                    <label for="logo" class="custom-file-label">Upload Image</label>
                                </div>
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="row" id="img-result">
                                @foreach ($survey->image as $item)
                                    <div class="col-2 text-center" id="image-{{$item->id}}">
                                        <a href="{{url($item->image_link)}}"  target="_blank" >
                                            <img src="{{$item->image_link}}" alt="{{$item->image_name}}" class="img-thumbnail border border-primary ">
                                        </a>
                                        <button class="btn btn-danger btn-outline btn-block m-sm apus-image" data-name="{{$item->image_name}}" data-id="{{$item->id}}" data-notif="{{$item->notif_id}}">Apus Image</button>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link href="{{asset('css/plugins/blueimp/css/blueimp-gallery.min.css')}}" rel="stylesheet">
<link href="{{asset('plugins/icheck103/skins/flat/red.css')}}" rel="stylesheet">

@endpush
@push('script')
<!-- blueimp gallery -->
<script src="{{asset('js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>
<script src="{{asset('plugins/icheck103/icheck.js')}}"></script>
<script>

const editSurvey = {
                        dataCustomer : function(customer,depo,callback)
                        {
                            axios.get('/report/survey/custoner_depo', {
                                params: {
                                    customer: customer,
                                    depo:depo
                                }
                            })
                            .then(function(response){
                                callback(response.data)
                            })
                            .catch(function(error){
                                console.error(error);
                            })
                        },
                        statusVote:function(a)
                        {
                            switch (a) {
                                case '1':
                                    return 'sangat suka'
                                    break;
                                case '2':
                                    return 'suka'
                                    break;
                                case '3':
                                    return 'cukup suka'
                                    break;
                                case '4':
                                    return 'tidak suka'
                                    break;
                                case '5':
                                    return 'samgat tidak suka'
                                    break;
                                default:
                                    break;
                            }
                        },
                        vote:function(vote,depo,callback)
                        {

                            let customer = [];
                            for(var k in vote) customer.push(k);
                            this.dataCustomer(customer,depo,function(data){
                                return data.map(function(a){
                                    let x = a;
                                        x.vote = vote[a.id_toko];
                                        x.desc = editSurvey.statusVote(vote[a.id_toko]);
                                        callback(x)
                                })
                                
                            })
                        },
                        order:function(order,depo,callback){
                            let customer = [];
                            for(var k in vote) customer.push(k);
                            
                            this.dataCustomer(customer,depo,function(data){
                                const dataOrder = data.map(function(a){
                                    a.order = order[a.id_toko];
                                    return a;
                                })
                                callback(dataOrder)
                            })
                        }
                    }
const domSurvey = {
        voteTpl:function(v)
        {
            let that = v;
            let d = [
                {vote:1,desc:'Sangat Suka',id_toko:v.id_toko},
                {vote:2,desc:'Suka',id_toko:v.id_toko},
                {vote:3,desc:'Cukup Suka',id_toko:v.id_toko},
                {vote:4,desc:'Tidak Suka',id_toko:v.id_toko},
                {vote:5,desc:'Sangat Tidak Suka',id_toko:v.id_toko},
            ].reverse();
            return d.map(function(a){
                if(that.vote == a.vote)
                a.checked = 'checked';
                return domSurvey.radio(a);
            })
        },
        view:function(a){
            let v = '<ul class="unstyled">'+this.voteTpl(a).join('')+'</ul>';
            let lab = this.labelText(a)
            const zz = `<div class="col-2 border-bottom">
                            ${lab}
                            ${v}
                        </div>`;
            $('#edit-vote').append(zz)

            $('.icheck-radio').iCheck({
                checkboxClass: 'icheckbox_flat-red',
                radioClass: 'icheckbox_flat-red',
                increaseArea: '20%' // optional
            });
        },
        radio:function(a)
        {
            return `<li><input class="icheck-radio" type="radio" value="${a.vote}" name="${a.id_toko}"  id="${a.id_toko}" ${a.checked} />
                    <label for="${a.id_toko}">
                        ${a.desc}
                    </label></li>`;
        },
        labelText:function(a,text = 'text-danger')
        {
            return `<h4><span class="font-bold ${text}">${a.id_toko}</span> <small class="font-bold">${a.nama_toko}</small></h4>`;
        },
        viewOrder:function(a){
            // console.log('view order data',a)
            const x = a.map(function(a){
                let orDer = '';
                for (const key in a.order) {
                    if (Object.hasOwnProperty.call(a.order, key)) {
                        const orderNya = a.order[key];
                        
                        orDer += `<li class="list-inline-item"><label>${key}</label>
                                  <input type="text" name="${key}" class="p-xxs form-control form-control-sm ${a.id_toko}" value="${(orderNya === null)?0:orderNya}" 
                                  style="max-width:50px;"
                                  /></li>`;
                    }
                }
                // return domSurvey.labelText(a)
                return `<div class="col-3 border-bottom">${domSurvey.labelText(a,'text-success')}<ul class="unstyled no-padding list-inline">${orDer}</ul></div>`;    
            })

            $('#edit-order').html(x.join(''))
        },
        image:function(a){
            let xd = a.map(function(x){
                return `<img src="${x.image_link}" alt="${x.image_name}" class="img-thumbnail">`;
            })
            return `<div class="col-12">${xd.join('')}</div>`
        }
}

let vote = JSON.parse(@json($survey->depo_survey->vote));
let order = JSON.parse(@json($survey->depo_survey->order));
let image = @json($survey->image);

const depo = "{{$survey->depo}}";
const surveyNomer = "{{$survey->depo_survey->customer['survey_no']}}";

// bikin data voting yang mau di edit
editSurvey.vote(vote,depo,function(a){
    domSurvey.view(a);
})

// data order yang mau diedit
editSurvey.order(order,depo,function(a){
    domSurvey.viewOrder(a)
})

// save form edit
function saveEdit() {
    for (const key in vote) {
        if (Object.hasOwnProperty.call(vote, key)) {
            const check = $('input[name="'+key+'"]:checked').val();
            // update data vote
            vote[key] = check;
            // update order
            const nameOrder = $('.'+key).each(function() {
                order[key][$(this).attr('name')] = parseInt($(this).val())
            });
            // console.log(nameOrder)
        }
    }
    // console.log('data order udah update ',order)
    // console.log(vote)
    // update survey depo
    axios.post('/report/survey/update_survey/'+surveyNomer,{
                order: order,
                vote: vote
            })
            .then(function (response) {
                // console.log(response.data.depo_survey.vote);
                // console.log(response.data.depo_survey.order);
                $('#edit-vote').empty()
                // $('#edit-order').empty()
                // bikin data voting yang mau di edit
                editSurvey.vote(response.data.depo_survey.vote,depo,function(a){
                    domSurvey.view(a);
                })
            })
            .catch(function (error) {
                console.log(error);
            });
}

// hapus images
$('.apus-image').on('click', function(a){
    const idNya = $(this).data('id');
    const notifId = $(this).data('notif');
    const imgName = $(this).data('name');
    apus_image(idNya,imgName)
   
})
function apus_image(idNya,imgName) {
     // console.log(imgName)
     axios.post('/report/survey/delete_image/'+idNya+'/'+imgName)
            .then(function(response){
                // console.log(response.data)
                if(response.data)
                {
                    $('#image-'+idNya).remove();
                }
            })
            .catch(function(error){
                console.error(error);
            })
}

// add image
$('.custom-file-input').on('change', function() {
   let fileName = $(this).val().split('\\').pop();
    $(this).next('.custom-file-label').addClass("selected").html(fileName);
    
    var formData = new FormData();
    var imagefile = document.querySelector('#file');
    formData.append("image", imagefile.files[0]);
    axios.post('/report/survey/add_image/'+surveyNomer, formData, {
        headers: {
        'Content-Type': 'multipart/form-data'
        }
    }).then(function(response){
        const img = response.data;
        const tpl = `<div class="col-2 text-center" id="image-${img.id}">
                        <a href="${img.image_link}"  target="_blank" >
                            <img src="${img.image_link}" alt="${img.image_name}" class="img-thumbnail border border-primary ">
                        </a>
                        <button class="btn btn-danger btn-outline btn-block m-sm apus-image" onclick="delImage()" >Apus Image</button>
                    </div>`
        $('#img-result').append(tpl);
        
        function delImage() {
            axios.post('/report/survey/delete_image/'+img.id+'/'+img.image_name)
            .then(function(response){
                // console.log(response.data)
                if(response.data)
                {
                    $('#image-'+img.id).remove();
                }
            })
            .catch(function(error){
                console.error(error);
            })
        }
    })
    .catch(function(error){
        console.error(error);
    })

});

</script>
@endpush
