<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Summary Report</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=JetBrains+Mono:wght@500&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Trirong">
    {{-- <link rel="stylesheet" href="{{url('fonts/jet-brain/css2.css')}}"> --}}
    {{-- <link rel="stylesheet" href="{{url('fonts/font-trirong/css.css')}}"> --}}

    <style>
        body {
            font-family: "JetBrains Mono", serif;
            font-size: 20px;
	    }
/* nama depo */
        .col-sm-6{
            position: relative;
            left: 7.5em;
            top: 10em;
            width: 600px;
            line-height: 1.5;
            padding-top:30px;
            /* height: 200px; */
	}
/* nama ceker */
        .col-sm-7{
            /* border: 3px solid #181817; */
            position: relative;
            left: 47em;
            top: -4em;
            width: 600px;
            /* height: 200px; */
	}
/* iderp text */
        .col-sm-8{
            position: relative;
            left: 60em;
            top:-3em;
	}
/* no surat jalan*/
        .col-sm-9{
            position: relative;
            left: 55em;
            top:-4.5em;
            width: 300px;
            padding-left: 25px;
            /* padding-top:20px; */
	    line-height: 1.6;
	    padding-top: 6px;
	}
/* isi nya*/
        .col-sm-10{
            position: relative;
            left: -0.5em;
            top:-2em;
	    padding-top:5px;
	    line-height:1;
	    font-size:25px;
   	    padding-left: -10px;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>

<body>
    @foreach ($sj as $item)
        <div class="col-sm-6">
            <span><strong>{{$item[0]['depo']}}</strong></span><br>
            @php($str = preg_replace('/\s\s+/', ' ', $item[0]['alamat']))
            @if(Str::length($str) < 50)
                <span>{{str_pad($str,50,' ')}}</span>
                <br />
                <br />
            @else
                <span>{{$item[0]['alamat']}}</span><br>
            @endif
            <span>{{$item[0]['phone']}}</span><br>
        </div>
        <div class="col-sm-7">
            <span><strong>CHECK </strong>  {{$item[0]['checker']}}</span><span style="padding-left: 8em;">{{Auth::user()->email}}</span><br />
            <span><strong>FORKLIFT </strong> {{$item[0]['forklif']}}</span><span style="padding-left: 4em;">{{\Carbon\Carbon::now()}}</span><br/>
        </div>
        <div class="col-sm-8">
            <h1>IDERP</h1>
        </div>
        <div class="col-sm-9">
            <span>{{$item[0]['no_sj']}}</span><br>
            <span>{{explode(' ',$item[0]['tgl'])[0]}}</span><br/>
            <span>{{$item[0]['no_so']}}</span><br />
            <span>{{$item[0]['no_customer']}}</span><br />
            <span>{{$item[0]['no_pol']}}</span><br />
            <span>{!! $item[0]['no_po'] !!}</span>
        </div>
        <div class="col-sm-10">
            <table width="90%" >
            <colgroup>
                <col width="20%" >
                <col width="30%">
                <col width="10%">
                <col width="5%">
                <col width="35%">
            </colgroup>
            @php($tot = 0) @php($weight = 0)
	    @foreach ($item as $it)
            @php($tot += $it['jml'])
            @php($weight += (isset($it['weight']) ?$it['weight'] : 0))
            <tr>
                <td class="cell" style="vertical-align: top;">{{$it['kode_barang']}}</td>
                <td class="cell" style="vertical-align: top;">{{$it['item_name']}}</td>
                <td class="cell" style="vertical-align: top; text-align: right; padding-right:40px;">{{$it['jml']}}</td>
                <td class="cell" style="vertical-align: top;">{{$it['satuan']}}</td>
                @php($satuan = $it['satuan'])
                <td class="cell" style="padding-left: 20px; vertical-align: top; font-size: 20px;">{{$it['ket']}}</td>
                {{-- @php($ket_tot = preg_replace('/\s\s+/', ' ', $it['ket']))
                @if (Str::length($ket_tot) > 25)
                    <td class="cell" style="padding-left: 20px; vertical-align: top; font-size: 25px;">{{$it['ket']}}</td>
                @else
                    <td class="cell" style="padding-left: 20px; vertical-align: top;">{{$it['ket']}}</td>
                @endif --}}
            </tr>
            @endforeach
            <tr>
                <td colspan="2" style="text-align: center;">Total</td>
                <td style="border-top: 2px solid black; text-align: right; padding-right:40px;">{{$tot}}</td>
                <td style="border-top: 2px solid black;">{{$satuan}}</td>
                <td style="border-top: 2px solid black; text-align: center;">{{number_format($weight,2)}}</td>
            </tr>
        </table>
        </div>
        @if (!$loop->last)
            <div class="page-break"></div>
        @endif
    @endforeach
</body>

</html>
