@extends('backend.app')
@section('content')
@if($errors->any())
<div class="alert alert-danger">
    <h4>{{$errors->first()}}</h4>
</div>
@endif
@if (\Session::has('success'))
    <div class="alert alert-success">
            <h4>{!! \Session::get('success') !!}</h4>
    </div>
@endif

<div class="ibox">
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-b-md">
                    <div class="btn-group float-right">
                        <a href="/report/survey/status_grid" class="btn btn-primary btn-sm ">Back</a>
                        {{-- langsung aja ngga usah load view lagi --}}
                        <a class="btn btn-warning btn-outline btn-sm " href="/report/survey/approve/{{$data->survey_no}}">Approve</a>
                        <a class="btn btn-danger btn-outline btn-sm " href="/report/survey/reject/{{$data->survey_no}}">Reject</a>
                        <a class="btn btn-primary btn-outline  btn-sm " href="/report/survey/edit_survey/{{$data->survey_no}}">Edit</a>
                        @isset($apprf_data)
                        {{-- <a href="/report/survey/reject/{{$data->id}}/{{$apprf_data['siapa']}}" class="btn btn-danger btn-sm">Approve {{$apprf_data['siapa']}}</a> --}}
                        @endisset
                    </div>
                    <h2><span class="font-bold text-primary">{{$data->survey_no}}</span></h2>
                    <hr class="border-bottom border-size-lg" />
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Status:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1">
                            <span class="label label-primary">{{$data->status->status}}</span> <br>
                            <span class="font-italic text-danger">{{$data->status->desc}}</span>
                        </dd>
                    </div>
                </dl>
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Tanggal Survey:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1"> {{ $data->tgl->format('d F Y') }}</dd>
                    </div>
                </dl>
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Depo:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1"><a href="#" class="text-navy"> {{ $data->depo_detail->id_depo }} /
                                {{ $data->depo_detail->nama_depo }}</a></dd>
                    </div>
                </dl>
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Ceramic:</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1">
                            Kode Barang: <span class="text-navy">{{$data->kode_barang}}</span>/
                            Brand: <span class="text-navy">{{$data->brand}}</span>/
                            Motif: <span class="text-navy">{{$data->motif}}</span>/
                            Ukuran: <span class="text-navy">{{$data->ukuran->title}}</span>
                        </dd>
                    </div>
                </dl>

            </div>
            <div class="col-lg-6" id="cluster_info">
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Start :</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1">{{$data->depo_survey->s_start}}</dd>
                    </div>
                </dl>
                <dl class="row mb-0">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Done :</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1"> {{$data->depo_survey->s_end}}</dd>
                    </div>
                </dl>
                <dl class="row mb-0 text-danger">
                    <div class="col-sm-4 text-sm-right">
                        <dt>Note :</dt>
                    </div>
                    <div class="col-sm-8 text-sm-left">
                        <dd class="mb-1"> {{$data->depo_survey->note}}</dd>
                    </div>
                </dl>
            </div>
        </div>

        <div class="row m-t-sm">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li><a class="nav-link active" data-toggle="tab" href="#tab-5">
                                <i class="fa fa-database"></i>
                                Survey
                            </a>
                        </li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-6">
                                <i class="fa fa-database"></i>
                                Document
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-5" class="tab-pane active">
                            <div class="panel-body">
                                @foreach ($customer['survey'] as $x => $it)
                                
                                <div class="row">
                                    <div class="col-sm-5 m-b-xs">
                                        <h5>Form: <span class="purple">{{$it['id_form']}}</span></h5>
                                    </div>
                                    <div class="col-sm-4 m-b-xs">
                                        <h5>Nama Sales: <span class="purple">{{$it['nama_sales']}}</span></h5>
                                    </div>
                                    <div class="col-sm-3">
                                    
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr class="bg-gray text-center white">
                                            <th rowspan="2">Customer</th>
                                            <th rowspan="2">Status Vote </th>
                                            <th colspan="4">Order</th>
                                        </tr>
                                        <tr>
                                            @foreach (SurveyHelper::warna($customer['warna']) as $w)
                                                <th class="bg-silver">{{$w}}</th>
                                            @endforeach
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($it['hasil'] as $em)
                                            <tr>                                            
                                                <td>{{$em['nama']}}</td>
                                                <td>{{$em['voting']}}</td>
                                                    @foreach ($em['order'] as $is => $itm)
                                                        <td>
                                                            <strong>{{$itm}}</strong>
                                                        </td>
                                                    @endforeach
                                            </tr>
                                        @endforeach
                                        
                                        </tbody>
                                    </table>
                                </div>
                                   
                                @endforeach
                            </div>
                        </div>
                        <div id="tab-6" class="tab-pane">
                            <div class="panel-body">
                                <br />
                                <div class="row">
                                    @foreach($data->image as $i)
                                        @if ($i->image_link)
                                            <div class="col-2" id="img-{{$i->id}}">
                                                <a href="{{url($i->image_link)}}" title="{{ $i->image_name }}" target="_blank">
                                                    <img class="img-thumbnail" id="{{$i->id}}" src="{{url($i->image_link)}}"
                                                        alt="{{ $i->image_name }}"/>
                                                </a>
                                                <button class="btn btn-primary btn-block dim rotate" type="button" data-name={{$i->id}}><i class="fa fa-repeat" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link href="{{asset('css/plugins/blueimp/css/blueimp-gallery.min.css')}}" rel="stylesheet">

@endpush
@push('script')
<!-- blueimp gallery -->
<script src="{{asset('js/jquery-rotate.js')}}"></script>
<script src="{{asset('js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>
<script>

    $('.rotate').on('click',function(){
        const idNya = $(this).data('name');
        axios.get('/report/survey/rotate_image/'+idNya,{
            }).then(function(response){
                $('#'+idNya).rotate(90);
                // return location.reload();
            }).catch(function(error){
                alert('file gagal di reload')
                console.error(error);
            })
    })

    $('.datatables').DataTable({
        pageLength: 7,
        // // paging: false,
        // processing: true,
        lengthChange: false,
        searching: false,
        responsive: true,
    });

</script>
@endpush
