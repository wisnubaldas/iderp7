<nav class="navbar-default navbar-static-side" role="navigation">
    <div>
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <img alt="image" class="rounded-circle" src="{{asset('img/siri.png')}}"/>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold">{{strtoupper(Auth::user()->name)}}</span>
                            <span class="text-muted text-xs block">{{Auth::user()->email}} <b class="caret"></b></span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a class="dropdown-item" href="#">Profile</a></li>
                            <li><a class="dropdown-item" href="#">Contacts</a></li>
                            <li><a class="dropdown-item" href="#">Mailbox</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="#">
                                <form action="{{route('logout')}}" method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-link">
                                        <i class="fa fa-sign-out"></i> Log out
                                    </button>
                                </form>
                            </a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        CJFI
                    </div>
                </li>
                <li class="active">
                    <a href="{{route('welcome')}}">
                            <i class="fa fa-th-large"></i> 
                            <span class="nav-label">Home</span> 
                        </a>
                </li>
                <li>
                <a href="{{route('home')}}">
                        <i class="fa fa-th-large"></i> 
                        <span class="nav-label">Dashboard</span> 
                    </a>
                </li>
            </ul>
        </div>
    
    </div>
    
    
</nav>
@push('script')
<script>
    
</script>
    <script>
            let mainMenu = @json(Session::get('menu'));
            jQuery(function(){
                const m = $('#side-menu');
                m.append(createMenu(JSON.parse(mainMenu)));
                const act = $('ul.nav li a');
                setActive(act)
            });
            
        function createMenu(v)
            {
            let x = '';
            for (const key in v) {
                if (v.hasOwnProperty(key)) {
                    const element = v[key];
                    if('sub_menu' in element){
                        x += `<li class="mm">
                            <a href="/${element.url}">
                                <i class="${element.icon}"></i> 
                                <span class="nav-label">${element.title}</span>
                                <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">`
                                for (const key in element.sub_menu) {
                                    if (element.sub_menu.hasOwnProperty(key)) {
                                        const el = element.sub_menu[key];
                                        x += `<li><a href="/${el.url}" >${el.title}</a></li>`
                                    }
                                }
                                x += `</ul>
                            </li>`;
                    }else{
                        x += `<li>
                        <a href="${element.url}">
                                <i class="${element.icon}"></i> 
                                <span class="nav-label">${element.title}</span> 
                            </a>
                        </li>`;
                    }
                }
            }
            return x;
        }
        function setActive(v)
        {
            for (const key in v) {
                if (v.hasOwnProperty(key)) {
                    const element = v[key];
                    if($(element).attr('href') === window.location.pathname)
                    {
                        $(element).parent().addClass('active special_link')
                                    .parent().addClass('in')
                                    .parent().addClass('active ');
                    }
                    
                }
            }
        }
    </script>
@endpush