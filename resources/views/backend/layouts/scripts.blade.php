<!-- Mainly scripts -->
{{-- <script src="https://unpkg.com/@babel/standalone/babel.min.js"></script> --}}
<script src="{{asset('js/babel.min.js')}}"></script>
<script src="{{asset('js/axios.min.js')}}"></script>
<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset('js/inspinia.js')}}" defer></script>
<script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>
<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- Steps -->
<script src="{{asset('js/plugins/steps/jquery.steps.min.js')}}"></script>
<!-- Switchery -->
<script src="{{asset('js/plugins/switchery/switchery.js')}}"></script>
<script src="{{asset('js/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>

{{-- <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script> --}}
<script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
<script src="{{asset('js/plugins/dataTables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>

<!-- Jquery Validate -->
<script src="{{asset('js/plugins/validate/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/plugins/dualListbox/jquery.bootstrap-duallistbox.js')}}"></script>
<script src="{{asset('js/plugins/ladda/spin.min.js')}}"></script>
<script src="{{asset('js/plugins/ladda/ladda.min.js')}}"></script>
<script src="{{asset('js/plugins/ladda/ladda.jquery.min.js')}}"></script>
@stack('before-script')
<!-- Scripts -->
{{-- <script src="{{ asset('js/app.js') }}"></script> --}}
<script>
    const event = new Date();
    document.getElementById('tgl').innerHTML = event.toString();
    jQuery(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    })

    const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
    let myScript = {
        encodeImage: async (img) => {
            const result = await toBase64(img).catch(e => Error(e));
            // console.log(await toBase64(img));
            if (result instanceof Error) {
                console.log('Error: ', result.message);
                return false;
            }
            return result;
        }
    }
    
</script>
@stack('script')
