jQuery(function() {
    $('#depo').select2();
    $('#brand-brand').select2();
    $('#ukuran').select2();
});

// depo proses blade
jQuery(function(){
    var count = 0;
    $('.belekok').on('ifChecked', function(event){
            $('#count').html(++count +` Selected`);
    });
    $('.belekok').on('ifUnchecked', function(event){
            $('#count').html(--count +` Selected`);
    });

    $('#frmDepoSurvey').on('submit',function(a){
        a.preventDefault()
        let d = $(this).serializeArray();
        axios.post(saveDepoProsses,d).then(function (response) {
                                            // console.log(response);
                                            // window.location = "/report/survey/depo_survey";
                                        })
                                        .catch(function (error) {
                                            // console.log(error);
                                            alert('error server')
                                        });
    })

   
    
})

jQuery(function(){
    $('#btn-grp').on('click',function(params) {
        // console.log($('#grp').val())
        // console.log(customerDepo)
        const jml = $('#grp').val()
        console.log(typeof jml)
        if(jml === '')
        {
            swal( "Ingin membuat berapa form survey?" ,  "Group survey harus di isi minimal 1 maksimal 10" ,  "error" )
            return false;
        }
        if(jml > 0 && jml <= 10)
        {
            // buat tab nya

            let options = [];
            // // nested table buat select customer
            //     events = ['onChoose','onStart','onEnd','onAdd','onUpdate','onSort','onRemove',
            //         'onChange','onUnchoose','afterMove'].forEach(function (name) {
            //             options[name] = function (evt) {
            //                 // console.log({
            //                 //     'event': name,
            //                 //     'this': this,
            //                 //     'item': evt.item,
            //                 //     'from': evt.from,
            //                 //     'to': evt.to,
            //                 //     'oldIndex': evt.oldIndex,
            //                 //     'newIndex': evt.newIndex
            //                 // });
            //                 // console.log(evt)
            //             };
            //         });
            // console.log(options)
            options.chosenClass = 'chosen'
            options.group = 'nested'
            options.dataIdAttr = 'data-id'
            options.animation = 100
        
            nestData = { id:'master', data:customerDepo }
            const x = tmplNest(nestData);
            $('.dd').html(x);
            yy = []
            idFrm = [];
            tabHead = [];
            for (let index = 0; index < jml; index++) {
                const idForm = 'FRP'+Math.floor(100000 + Math.random() * 900000);
                idFrm.push(idForm);
                const y = tmplGrpSurvey(idForm);
                yy.push(y)
                tabHead.push(tmplTabHeader(idForm));
            }
        
        // tempelin ke dom
        $('#form-respon').html(yy.join(''));
        $('.form-tab').html(tabHead.join(''));
        $('.harga-depo').on('input paste keyup',function(){
            $(".harga-depo").val($(this).val());
        })
        ///--------------------------------
            const foo = document.getElementById('nestable');
            Sortable.create(foo, options);
            idFrm.forEach(function(a){
                const fuck = document.getElementById(a);
                Sortable.create(fuck, options);
            })
        
            // save data form
            $('#save').on('click',function(a){
                a.preventDefault()
                // console.log(dataSurvey)
                const dataPost = []
                idFrm.forEach(function(a){
                    // console.log(a)
                    const fuck = document.getElementById(a);
                    // Sortable.create(fuck, options);
                    const nestedQuery = a;
                    const identifier = 'id';
                    function serialize(sortable) { // DAPETIN ARRAY DARI FORM NYA
                        var serialized = [];
                        var children = [].slice.call(sortable.children);
                        for (var i in children) {
                            // console.log(children[i].textContent);
                            // var nested = children[i].querySelector(nestedQuery);
                            serialized.push({
                            id: children[i].dataset[identifier],
                            // children: nested ? serialize(nested) : []
                            });
                        }
                        return serialized
                    }
                    const salesName = $('#sales-'+a).val();
                    const hargaDepo = $('.harga-depo').val();
                    // validate
                    if(salesName == '' || hargaDepo == '')
                    {
                        return false;
                    }
                    const x = {
                        form_id:a,
                        sales:salesName,
                        harga_depo:hargaDepo,
                        customer:serialize(fuck)
                    }
                    dataPost.push(x)
                    // console.log(serialize(fuck))
                }) // ond foreach

                // hook calidasi form survey
                // console.log(dataPost);
                if(dataPost.length == 0)
                {
                    swal( "Sales Harus dan harga depo di isi" ,  "Nama sales dan harga depo untuk setiap form survey harus ada" ,  "error" )
                    return false;
                }

                postDataKeServerBangke({
                    survey_no:depoAtrr.survey_no,
                    id_reg:depoAtrr.depo_detail.id_reg,
                    id_depo:depoAtrr.depo_detail.id_depo,
                    nama_depo:depoAtrr.depo_detail.nama_depo,
                    brand:depoAtrr.brand,
                    ukuran:depoAtrr.ukuran.title,
                    motif:depoAtrr.motif,
                    warna:depoAtrr.warna,
                    form_respon:dataPost
                })
                // console.log(depoAtrr)
                // console.log(dataPost)
            })

        }else{
            swal( "Ingin membuat "+jml+" form survey?" ,  "Group survey harus di isi minimal 1 maksimal 10" ,  "error" )
        }
    })

    // post data generate form survey dari depo
    const postDataKeServerBangke = function(data)
    {
        console.log(data)
        swal({
            title: "Simpan form data survey?",
            text: "Teliti terlebih dahulu sebelum di cetak, tekan cancel jika ingin memperbaiki data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#28a745",
            confirmButtonText: "Simpan",
            closeOnConfirm: false
        }, function () {
            axios.post(saveDepoProsses,data)
                .then(function (response) {
                    // handle success
                    console.log(response);
                    swal("Tersimpan!", "Data tersimpan dengan baik dan benar", "success");
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .then(function () {
                    // always executed
                    // window.location = '/report/survey/depo_survey';
                });
        });

        

    }
     // buat template nestable
     function tmplNest(grpId) // default data customer template
     {
         const d = grpId.data.map(function(a){
             return `<li class="b-r-md" data-id="${a.id_toko}/${a.nama_toko}">
                <a href="#${a.id_toko}" class="list-group-item list-group-item-primary list-group-item-action">
                    <span class="text-danger" >${a.id_toko}</span> ${a.nama_toko}</a></li>`;
         })
         const x = `<ul id="nestable" class="list-group list-unstyled">${d.join("")}</ul>   `;
         return x.replace(/(\r\n|\n|\r)/gm, "");

     }
     
     function tmplGrpSurvey(id) 
     {
         const s = `<div role="tabpanel" id="tab-${id}" class="tab-pane">
                <div class="panel-body">
                <div class="row no-paddings no-margins">
                    <div class="form-group row form-group-sm col-6">
                        <label class="col-lg-4 col-form-label">Nama Sales</label>
                        <div class="col-lg-8">
                            <input type="text" name="sales" id="sales-${id}" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="form-group row form-group-sm col-6  ">
                    <label class="col-lg-5 col-form-label">Perkiraan Harga Depo</label>
                    <div class="col-lg-7">
                        <input type="number" name="harga-depo" id="harga-${id}" class="form-control form-control-sm harga-depo">
                    </div>
                </div>
                </div>
                <div class="hr-line-solid .border-size-md no-paddings"></div>
                <div class="bg-muted">
                    <ul id="${id}" class="list-group list-unstyled list-group-primary"></ul>
                </div>
                <div class="hr-line-solid .border-size-md no-paddings"></div>
                    </div>
                </div>`;
        return s.replace(/(\r\n|\n|\r)/gm, "");
     }
     function tmplTabHeader(id){
         return `<li><a class="nav-link" data-toggle="tab" href="#tab-${id}"> ${id}</a></li>`
     }
})

// customers upload
jQuery(function(){
    let handleCustomers = {}
    handleCustomers.getBrand = function(a,b){
        const cust = a[b.brand];
        if(!cust)
        {
            swal( "Tidak ada brand "+b.brand ,  "Brand tidak terdaftar di excel customer, silahkan pilih brand" ,  "warning" );
            return false;
        }
        const custByDepo = cust.map(function(a){
                if(a.id_depo == b.depo)
                {
                    return { id:a.id_customer,nama:a.nama_toko}
                }else{ return false; }
        })
        let customer = $('#customer');
        
        for (const i of _.compact(custByDepo)) {
            // console.log(i)
            const x = `<option value="${i.id}" selected class="indigo">${i.id} - ${i.nama}</option>`;
            customer.append(x)
        }
        customer.bootstrapDualListbox('refresh', true);
    }
    $('.custom-file-input:file').on('change', function(a) {
        // console.log(a)
        let files = a.target.files
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
        
        var formData = new FormData();
        var imagefile = document.querySelector('#customer_file');
        formData.append("image", imagefile.files[0]);
        const depo = $('#depo').val();
        const brand = $('#brand').val();
        if(depo !== '' && brand !== '')
        {
            axios.post('/report/survey/upload_data_customer', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
            }).then(function (response) {
                handleCustomers.getBrand(response.data,{depo:depo,brand:brand});
            })
            .catch(function (error) {
                console.log(error);
            });
        }else{
            swal( "Depo dan Brand harus di set" ,  "Untuk upload customer form survey depo harus diisi" ,  "error" );
        }
        

    });
})