// user role
jQuery(function() {

let table = $('#tes').DataTable({
    processing: true,
    serverSide: true,
    ajax: '/system/rolebase/grid',
    columns: [
        { data: 'action',name: 'action', "orderable":false,"searchable":false},
        { data: 'name',name: 'name' },
        { data: 'guard_name',name: 'motif' },
        { data: 'permission',name: 'permission' },
    ],
    pageLength: 10,
    // paging: false,
    processing: true,
    lengthChange: false,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [{extend: 'copy'},
        {extend: 'csv'},
        {extend: 'excel',title: 'ExampleFile'},
        {extend: 'pdf',title: 'ExampleFile'},
        {
            extend: 'print',
            customize: function (win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
            }
        }
    ]
});
    // console.log(postUserRolesUrl);
    $('.select2').select2();
    $('.select-role').select2();
    let selectPermission = $('.select-permission').select2();
    $('.select-role').on('select2:select', function (e) {
        // Do something
        selectPermission.val(null);
        var data = e.params.data;
        axios.post(postUserRolesUrl,{
            role:data.id
        }).then(function (response) {
            // console.log(response);
            selectPermission.empty();
            response.data.forEach(element => {
                var option = new Option(element.name, element.name, false, false);
                selectPermission.append(option).trigger('change');
            });
          })
          .catch(function (error) {
            console.log(error);
          });
      });
});

// ###### User Manager ##########
jQuery(function(){
    
})