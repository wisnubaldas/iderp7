@extends('frontend.index')
@section('content')
<div class="row">
    <div class="col-md-3">
        
        @foreach ($brand as $i)
            <div class="">
                <a href="{{route('merek.detail',$i->id)}}">
                    <img src="{{asset($i->url)}}" class="img-thumbnail rounded" width="250" alt="atena_tile">
                </a>
            </div>
        @endforeach
    </div>
    <div class="col-md-9">
        @dump($brand->toJson())
    </div>
</div>

@endsection