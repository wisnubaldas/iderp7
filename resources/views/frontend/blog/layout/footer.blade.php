<!-- begin #footer -->
<div id="footer" class="footer">
    <!-- begin container -->
    <div class="container">
        <!-- begin row -->
        <div class="row">
            <!-- begin col-3 -->
            <div class="col-md-3 col-sm-3">
                <!-- begin section-container -->
                <div class="section-container">
                    <h4 class="footer-title">Categories</h4>
                    <ul class="categories">
                        <li><a href="#">Merek</a></li>
                        <li><a href="#">Inspirasi Desain</a></li>
                        <li><a href="#">Produk Baru</a></li>
                        <li><a href="#">Perusahaan</a></li>
                        <li><a href="#">Jaringan Distribusi</a></li>
                        <li><a href="#">Tanya CJFI</a></li>
                    </ul>
                </div>
                <!-- end section-container -->
            </div>
            <!-- end col-3 -->
            <!-- begin col-3 -->
            <div class="col-md-3 col-sm-3">
                <!-- begin section-container -->
                <div class="section-container">
                    <h4 class="footer-title">Archives</h4>
                    <ul class="archives">
                        <li><a href="#">June 2015</a> <span class="total">(102)</span></li>
                        <li><a href="#">May 2015</a> <span class="total">(46)</span></li>
                        <li><a href="#">April 2015</a> <span class="total">(84)</span></li>
                        <li><a href="#">December 2014</a> <span class="total">(25)</span></li>
                    </ul>
                </div>
                <!-- end section-container -->
            </div>
            <!-- end col-3 -->
            <!-- begin col-3 -->
            <div class="col-md-3 col-sm-3">
                <!-- begin section-container -->
                <div class="section-container">
                    <h4 class="footer-title">Recent Posts</h4>
                    <ul class="recent-post">
                        <li>
                            <h4>
                                <a href="#">Lintas Warta</a>
                                <span class="time">February 22, 2015</span>
                            </h4>
                        </li>
                        <li>
                            <h4>
                                <a href="#">Arsip</a>
                                <span class="time">July 15, 2015</span>
                            </h4>
                        </li>
                        <li>
                            <h4>
                                <a href="#">Dukungan</a>
                                <span class="time">March 21, 2015</span>
                            </h4>
                        </li>
                    </ul>
                </div>
                <!-- end section-container -->
            </div>
            <!-- end col-3 -->
            <!-- begin col-3 -->
            <div class="col-md-3 col-sm-3">
                <div class="section-container">
                    <h4 class="footer-title">About CJFI</h4>
                    <address>
                        <strong>PT Chang Jui Fang Indonesia</strong><br />
                        Jl. Raya Losarang Indramayu<br />
                        KM. 71 Desa Pangkalan Losarang Indramayu<br />
                        P: (123) 000-0000<br />
                        <br />
                        <strong>cjfi.co.id</strong><br />
                        <a href="#">cjfi@cjfi.co.id</a>
                    </address>
                </div>
                <!-- end section-container -->
            </div>
            <!-- end col-3 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end #footer -->
<!-- begin #footer-copyright -->
<div id="footer-copyright" class="footer-copyright">
    <!-- begin container -->
    <div class="container">
        <span class="copyright">&copy; 2017 SeanTheme All Right Reserved</span>
        <ul class="social-media-list">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-rss"></i></a></li>
        </ul>
    </div>
    <!-- end container -->
</div>
<!-- end #footer-copyright -->