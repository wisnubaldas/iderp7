<!-- begin #content -->
<div id="content" class="content">
    <!-- begin container -->
    <div class="container">
        @yield('content')
    </div>
    <!-- end container -->
</div>
<!-- end #content -->