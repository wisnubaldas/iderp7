<!-- begin #content -->
<div id="content" class="content">
    <!-- begin container -->
    <div class="container">
        <!-- begin row -->
        <div class="row row-space-30">
            <!-- begin col-9 -->
            <div class="col-md-9">
                @yield('content')
            </div>
            <!-- end col-9 -->
            <!-- begin col-3 -->
            <div class="col-md-3">
                <!-- begin section-container -->
                <div class="section-container m-b-5">
                    <div class="input-group sidebar-search ">
                        <input type="text" class="form-control" placeholder="Search Our Stories..." />
                        <span class="input-group-btn">
                            <button class="btn btn-inverse" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    @if ($btn_link)
                    <div class="m-t-4">
                        <a href="{{$btn_link ?? ''}}" class="btn btn-inverse btn-block">
                            <i class="fa fa-undo f-w-500 f-s-20 " aria-hidden="true"></i> 
                            {{$btn_title ?? ''}}</a>
                    </div>
                    @endif
                    
                </div>
                <!-- end section-container -->
                    @yield('sidebar')
                <!-- begin section-container -->
                <div class="section-container">
                    <h4 class="section-title"><span>Follow Us</span></h4>
                    <ul class="sidebar-social-list">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
                <!-- end section-container -->
            </div>
            <!-- end col-3 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end #content -->