<head>
	<meta charset="utf-8" />
	<title>CJFI | {{isset($page)?$page:'Home'}}</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta name="description" content="CJFI Atena Picasso Kita Keramik Granit" />
	<meta name="keywords" content="atena, picasso, kita, keramik lantai, keramik dinding, ubin keramik, interior, tile, granit, keramik, ceramic" />
	<meta name="author" content="CJFI 2020 |By Baldas">
	<meta name="robots" content="index, follow" />

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="{{asset('blog/style.css')}}" rel="stylesheet" />
	<link href="{{asset('blog/style_modif.css')}}" rel="stylesheet" />
	@yield('style')
	<!-- ================== END BASE CSS STYLE ================== -->
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{asset('blog/js/pace.js')}}"></script>
	<!-- ================== END BASE JS ================== -->
</head>