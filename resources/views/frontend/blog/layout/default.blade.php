<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
@include('frontend.blog.layout.header')

<body>
	@include('frontend.blog.layout.head')
	@include('frontend.blog.layout.page_title')
	@if (isset($sidebar))
		@include('frontend.blog.layout.content_sidebar',[
															'btn_link'=>$btn_link ?? '',
															'btn_title'=>$btn_title ?? ''
															])
	@else
		@include('frontend.blog.layout.content')
	@endif
	@include('frontend.blog.layout.footer')
	{{-- @include('frontend.blog.layout.theme_panel') --}}
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{asset('blog/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('blog/js/bootstrap.min.js')}}"></script>
	<!--[if lt IE 9]>
		<script src="../assets/crossbrowserjs/html5shiv.js"></script>
		<script src="../assets/crossbrowserjs/respond.min.js"></script>
		<script src="../assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="{{asset('blog/js/js.cookie.js')}}"></script>
	<script src="{{asset('blog/js/main.js')}}"></script>
	<!-- ================== END BASE JS ================== -->
	@yield('script')
	<script>
		$(document).ready(function() {
	        App.init();
	    });
	</script>
</body>

</html>