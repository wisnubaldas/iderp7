@extends('frontend.blog.layout.default',['sidebar'=>true])
@section('content')
<h1>Slide Koleksi Barang</h1>    
@dump($barang->toJson())
@endsection

@section('sidebar')
<div class="section-container">
    <h4 class="section-title"><span>Barang</span></h4>
    <ul class="sidebar-list">
        @foreach ($barang as $i)
            <div class="">
            <a href="{{route('merek',$i->id)}}">
                    <img src="{{asset($i->url)}}" class="img-thumbnail m-4" alt="atena_tile">
                </a>
            </div>
        @endforeach
    </ul>
</div>
@endsection