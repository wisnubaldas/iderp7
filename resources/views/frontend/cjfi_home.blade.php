@extends('frontend.index')
@section('content')
<div id="carouselExampleBigIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach ($data as $i)
                @if ($loop->first)
                    <li data-target="#{{$i['id']}}" data-slide-to="{{$i['id']}}" class="active"></li>
                @else
                    <li data-target="#{{$i['id']}}" data-slide-to="{{$i['id']}}"></li>
                @endif
        @endforeach
    </ol>
    <div class="carousel-inner">
        @foreach ($data as $i)
            @if ($loop->first)
                <div class="carousel-item active">
                    <img class="d-block w-100" src="img/slide/{{$i['slide_img']}}" alt="{{$i['slide_text_en']}}">
                    <div class="carousel-caption d-none d-md-block font-bold blockquote">
                        <p>{{$i['slide_text_en']}}</p>
                        <p>{{$i['slide_desc_en']}} </p>
                        <a class="btn btn-primary btn-rounded btn-outline btn-block" href="{{$i['slide_link']}}">Show Detail</a>
                    </div>
                </div>
            @else
            <div class="carousel-item">
                <img class="d-block w-100" src="img/slide/{{$i['slide_img']}}" alt="{{$i['slide_text_en']}}">
                <div class="carousel-caption d-none d-md-block font-bold blockquote">
                    <p>{{$i['slide_text_en']}}</p>
                    <p>{{$i['slide_desc_en']}} </p>
                    <a class="btn btn-primary btn-rounded btn-outline btn-block" href="{{$i['slide_link']}}">Show Detail</a>
                </div>
            </div>
            @endif
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleBigIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleBigIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
@endsection