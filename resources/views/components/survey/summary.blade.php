<div {{$attributes}}>
    <div class="col-lg-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li><a class="nav-link active" data-toggle="tab" href="#tab-1">
                    Sumary Toko
                </a>
                </li>
                <li><a class="nav-link" data-toggle="tab" href="#tab-2">
                    Sumary Region
                </a>
                </li>
                <li><a class="nav-link" data-toggle="tab" href="#tab-3">
                    Summary
                </a>
                </li>
                <li><a class="nav-link" data-toggle="tab" href="#tab-4">
                    Chart
                </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                       @yield('summary-toko')
                </div>
                <div id="tab-2" class="tab-pane ">
                       @yield('summary-region')
                </div>
                <div id="tab-3" class="tab-pane ">
                       @yield('summary-all')
                </div>
                <div id="tab-4" class="tab-pane ">
                    @yield('chart-survey')
             </div>
            </div>
        </div>
    </div>
</div>