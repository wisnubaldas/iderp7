<div {{$attributes}}>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Report <small>data survey...</small></h5>
            </div>
            <div class="ibox-content">
                <form role="form" id="report-form" class="form">
                    <div class="row">
                        <div class="col-sm-6 b-r">
                            <div class="form-group">
                                <label>Motif Barang</label>
                                <input type="text" class="form-control text-uppercase" name="motif">
                            </div>
                            {{-- <div class="form-group">
                                <label>Kode Barang</label>
                                <input type="text" class="form-control text-uppercase" name="kode_barang">
                            </div> --}}
                        </div>
                        {{-- <div class="col-sm-3 b-r">
                            <div class="form-group">
                                <label>Priode</label>
                                <input class="form-control" type="text" name="daterange" id="reportrange" />
                                <span></span>
                            </div>
                        </div> --}}

                        <div class="col-sm-6">
                            <p class="text-center">
                                <a href="javascript:void(0)" class="ladda-button-demo ladda-button btn btn-primary btn-lg" id="cari" data-size="l" data-style="expand-right">
                                    <i class="fa fa-search fa-flip-horizontal fa-3x"></i> Cari data survey
                                </a>
                               <hr class="" />
                                {{$error_message}}
                            </p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="ibox-content">
                <x-survey.summary class="invisible animated pulse" id="summary" />
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        {{-- @dump($dataInclude) --}}
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-4">
                        <h2>Status Survey</h2>
                        <small>Status Survey Priode {{today()->format('d F, Y')}}</small>
                        <ul class="list-group clear-list m-t legend-survey">
                            
                        </ul>
                    </div>
                    <div class="col-md-8">
                        <div class="flot-chart dashboard-chart">
                            <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                        </div>
                        <div class="row text-left">
                            <div class="col">
                                <div class=" m-l-md ">
                                <span class="h5 font-bold m-t block tot-survey"></span>
                                <small class="text-muted m-b block">Total Survey Keseluruhan</small>
                                </div>
                            </div>
                            <div class="col">
                                <span class="h5 font-bold m-t block tot-survey-month"></span>
                                <small class="text-muted m-b block">Total Survey {{today()->format('d F, Y')}}</small>
                            </div>
                            <div class="col">
                                <span class="h5 font-bold m-t block tot-survey-done"></span>
                                <small class="text-muted m-b block">Survey selesai {{today()->format('d F, Y')}}</small>
                            </div>
            
                        </div>
                    </div>
                    
                </div>
                
            </div> 
            {{-- end ibox-content --}}
            <div class="ibox-content">
                {{-- @dump($dataInclude) --}}
                <div class="tabs-container">
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach ($dataInclude['bulan'] as $item)
                            <li><a class="nav-link {{($loop->index == 0)?'active show':''}}" data-toggle="tab" href="#stab-{{$loop->index}}">{{$item}}</a></li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach ($dataInclude['bulan'] as $item)
                        <div role="tabpanel" id="stab-{{$loop->index}}" class="tab-pane {{($loop->index == 0)?'active show':''}}">
                            <div class="panel-body col-lg-12">
                                @isset($dataInclude['per_bulan'][$item])
                                    <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="5">
                                        <thead>
                                            <tr>
                                                <th data-toggle="false" colspan="2">Depo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($dataInclude['per_bulan'][$item] as $idx => $keling)
                                                <tr>
                                                    <td  data-title="ID" data-breakpoints="xs">{{$idx}}</td>
                                                    <td>
                                                        <table class="footable2 table table-stripped toggle-arrow-tiny" >
                                                            <thead>
                                                                <tr >
                                                                    <th data-hide="all" data-breakpoints="xs sm md">No Survey</th>
                                                                    <th data-hide="all" data-breakpoints="xs sm md">Tanggal</th>
                                                                    <th data-hide="all" data-breakpoints="xs sm md">Motif</th>
                                                                    <th data-hide="all" data-breakpoints="xs sm md">Warna</th>
                                                                    <th data-hide="all" data-breakpoints="xs sm md">Kode Barang</th>
                                                                    <th data-hide="all" data-breakpoints="xs sm md">Ukuran</th>
                                                                    <th data-hide="all" data-breakpoints="xs sm md">Status</th>
                                                                    
                                                                </tr> 
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($keling->keys() as $motif)
                                                                <tr data-hide="all" data-breakpoints="xs sm md">
                                                                    <td>{{$keling[$motif][0]->survey_no}}</td>
                                                                    <td>{{$keling[$motif][0]->tgl->format('d-F-Y')}}</td>
                                                                    <td>{{$motif}}</td>
                                                                    <td>{{$keling[$motif][0]->warna}}</td>
                                                                    <td>{{$keling[$motif][0]->kode_barang}}</td>
                                                                    <td>{{$keling[$motif][0]->ukuran->title}}</td>
                                                                    <td class="text-danger">{{$keling[$motif][0]->status->desc}}</td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                            
                                                        </table>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="5">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                @endisset
                            </div>
                        </div>
                        @endforeach
                        
                    </div>


                </div>
                {{-- end tab tabs-container --}}
            </div>
            {{-- end ibox-content --}}
        </div>
    </div>
    {{-- end col-12 --}}
</div>
@push('css')

@endpush
@push('script')
<!-- Flot -->
<script src="{{asset('js/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{asset('js/plugins/flot/jquery.flot.time.js')}}"></script>
<script src="{{asset('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('js/plugins/flot/jquery.flot.spline.js')}}"></script>
<script src="{{asset('js/plugins/flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('js/plugins/flot/jquery.flot.pie.js')}}"></script>

<script>
    
    const all_status = @json($dataInclude['status_survey']);
    const status1 = @json($dataInclude['data_survey']['status1']);
    const status2 = @json($dataInclude['data_survey']['status2']);
    const status3 = @json($dataInclude['data_survey']['status3']);
    const status4 = @json($dataInclude['data_survey']['status4']);
    const status5 = @json($dataInclude['data_survey']['status5']);
    const status6 = @json($dataInclude['data_survey']['status6']);
    const status7 = @json($dataInclude['data_survey']['status7']);
    const status8 = @json($dataInclude['data_survey']['status8']);
    const status9 = @json($dataInclude['data_survey']['status9']);
    const bulan_sekarang = "{{today()->format('n')}}";
    let summary = [];
    
    
    function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime();
    }
    function create_data(d){
        const a = Object.values(d).reduce((a, b) => a + b, 0)
        summary.push(a)
        var data1 = []; // 
        for (let index = 0; index < 12; index++) {
                let x = index+1
                if(x < 10)
                {
                    x = '0'+x
                }
                if(typeof d[x] == 'undefined')
                {
                    data1.push([gd(2021,x,01),0])

                }else{
                    data1.push([gd(2021,x,01),d[x]])
                }
        }
        return data1;
    }
    
    // summary
    
    
    jQuery(function(){
        // console.log(data1)
        const data_series = [
            create_data(status1),
            create_data(status2),
            create_data(status3),
            create_data(status4),
            create_data(status5),
            create_data(status6),
            create_data(status7),
            create_data(status8),
            create_data(status9)
            ];


        const color_res = ["#1C84C6","#001f3f",'#0074d9',"#7fdbff","#39cccc","#3d9970","#2ecc40","#01ff70","#c42e0c"]
        // console.log(summary)
        const al_stat = all_status.map(function(v,i){
                            let col, sum
                            // console.log(i)
                            col = color_res[i];
                            sum = summary[i]
                            
                            return `<li class="list-group-item fist-item">
                                        <span class="float-right">
                                            ${sum} Survey
                                        </span>
                                        <span class="label" style="background-color:${col}; color:#fff;">${v.id}</span> ${v.status}
                                    </li>`
                        })
    
            $('.legend-survey').html(al_stat)
            // total survey
            $('.tot-survey').html(summary.reduce((a, b) => a + b, 0))
            // total survey bulan ini,
            let sbs = data_series[bulan_sekarang].map(function(a){
                return a[1];
            })
            $('.tot-survey-month').html(sbs.reduce((a, b) => a + b, 0))
            // survey selesai bulan ini
            $('.tot-survey-done').html(sbs[9])

            $("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"),data_series,
                {
                    series: {
                        lines: {
                            show: false,
                            fill: true
                        },
                        splines: {
                            show: true,
                            tension: 0.4,
                            lineWidth: 1,
                            fill: 0.4
                        },
                        points: {
                            radius: 0,
                            show: true
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#d5d5d5",
                        borderWidth: 1,
                        color: '#d5d5d5'
                    },
                    colors: color_res,
                    xaxis:{
                        mode: "time",
                        tickSize: [1, "month"],
                        min: gd(2021, 1, 1),
                        max: gd(2021, 12, 2)
                    },
                    yaxis: {
                        ticks: 4
                    },
                    tooltip: false
                }
            );

    })
</script>
@endpush
