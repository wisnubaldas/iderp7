<div class="panel-body">
    <div class="row">
        <div class="col-md-12 text-center text-navy"><h4>Prosentase dan Jumlah Quantity Order/box per Region <hr class="hr-line-dashed no-paddings" /></h4></div>
        <div class="col-md-6">
            <div class="box">
                <div class="ibox-content text-center h-200">
                    <canvas id="barChart-1" height="200"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="ibox-content text-center h-200">
                    <canvas id="barChart-2" height="200"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="text-center">
                            <th rowspan="2" >Region </th>
                            <th rowspan="2">Nama Depo </th>
                            <th colspan="5">Komentar Barang</th>
                            <th rowspan="2">%</th>
                            <th colspan="4">QUANTITY ORDER/BOX</th>
                        </tr>
                        <tr class="text-center">
                            <th>SANGAT SUKA </th>
                            <th>SUKA</th>
                            <th>CUKUP SUKA</th>
                            <th>TIDAK SUKA</th>
                            <th>SANGAT TIDAK SUKA</th>
                            @foreach ($sData->ble_ketek()->warna as $item)
                                <th>{{$item}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @php($all = [])
                            @foreach ($gion as $k => $i)
                                @php($sData->total_order = [])
                                {{-- sumary data vote per region --}}
                                @foreach ($i as $it)
                                    <tr class="text-right">
                                        @if($loop->index == 0)
                                            <td class="text-center" rowspan="{{count($i)}}">{{$k}}</td>
                                            <td class="text-left">{{$it['depo']}}</td>
                                        @else
                                            <td class="text-left">{{$it['depo']}}</td>
                                        @endif
                                        <td>
                                            {{$sData->vote($it['voting'])->tot_per_vote('pss',count($i))->pss}}
                                        </td>
                                        <td>
                                            {{$sData->vote($it['voting'])->tot_per_vote('ps',count($i))->ps}}
                                        </td>
                                        <td>
                                            {{$sData->vote($it['voting'])->tot_per_vote('pcs',count($i))->pcs}}
                                        </td>
                                        <td>
                                            {{$sData->vote($it['voting'])->tot_per_vote('pts',count($i))->pts}}
                                        </td>
                                        <td>
                                            {{$sData->vote($it['voting'])->tot_per_vote('psts',count($i))->psts}}
                                        </td>
                                        <td>
                                            {{$sData->vote($it['voting'])->tot_per_vote('all',count($i))->p_all}}
                                        </td>
                                        @foreach ($sData->ble_ketek()->warna as $item)
                                            <th>{{$sData->region($item,$it['order'])->order}}</th>
                                        @endforeach
                                        
                                    </tr>
                                @endforeach
                                {{-- Data buat Chart --}}
                                @php($chart = [
                                        'reg'=>$k,
                                        'quis'=>[],
                                        'motif'=>$motif,
                                        'order'=>[]
                                    ])
                                    <tr class="bg-primary font-bold text-center">
                                        <td colspan="2">Total {{$k}}</td>
                                        @php($sData->tot_all_sum[$k]= $sData->sum_by_vote_region)
                                            @foreach ($sData->sum_by_vote_region as $item)
                                                @php(array_push($chart['quis'],$item))
                                                <td>{{ number_format(round($item,2),2,',','.') }}</td>
                                            @endforeach
                                        <td>100%</td>
                                        @php($sData->reset_tot_vote())
                                        
                                        @foreach ($sData->total_order() as $item)
                                                @php(array_push($chart['order'],$item))
                                                <td>{{number_format($item,0,',','.')}}</td>
                                        @endforeach
                                        {{-- end looping depo --}}
                                    </tr>
                                @php(array_push($all,$chart))
                                {{-- @dump($i) --}}
                            @endforeach
                        {{-- end looping regional --}}
                        <tr class="bg-success font-bold text-center">
                            <td colspan="2">Jumlah Komentar barang</td>
                            @foreach ($sData->tot_by_vote_region as $item)
                                <td>{{number_format(($item/7),2,',','.')}}</td>
                            @endforeach
                            <td>100%</td>
                            @foreach ($sData->grand_total_region() as $item)
                            <td>{{number_format($item,0,',','.')}}</td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>
<script>
    // const nil = @json($sData->grand_total_region());
    // console.log(nil)
    function getRandomColorHex() {
        const colorLetters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += colorLetters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    function getRandomColorRgb(z) {
        // console.log(z)
        const items = ["178,132,190","0,72,186","227,38,54","196,98,16","59,122,87","171,39,79","0,85,128"]
        var item = items[Math.floor(Math.random() * items.length)];
        
        // let red = Math.floor(Math.random() * 256);
        // let green = Math.floor(Math.random() * 256);
        // let blue = Math.floor(Math.random() * 256);

        let red = item.split(',')[0];
        let green = item.split(',')[1];
        let blue = item.split(',')[2];

        // return `rgb(${red}, ${green}, ${blue})`;
        let bgColor = `rgba(${red}, ${green}, ${blue},0.5)`;
        let brColor = `rgba(${red}, ${green}, ${blue},0.7)`;
        let pBgColor = `rgba(${red}, ${green}, ${blue},1)`;
        return {bgColor:bgColor,brColor:brColor,pBgColor:pBgColor}
    }
    function getRandomColorHsl() {
        let hue = Math.floor(Math.random() * 360);
        let saturation = Math.floor(Math.random() * 100);
        let lightness = Math.floor(Math.random() * 100);
        // return `hsl(${hue}, ${saturation}%, ${lightness}%)`;
        let bgColor = `hsl(${hue}, ${saturation}%, ${lightness}%)`;
        let brColor = `hsl(${hue-20}, ${saturation-20}%, ${lightness-20}%)`;
        let pBgColor = `hsl(${hue+20}, ${saturation+20}%, ${lightness+20}%)`;
        return {bgColor:bgColor,brColor:brColor,pBgColor:pBgColor}
    }

    function generateChart(ctx2,data_set,label,type)
    {
        var barData = {
                labels: label,
                datasets: data_set
            };

        var barOptions = {
            responsive: true,
            animation: {
                         duration: 0 // general animation time
                    },
                    hover: {
                        animationDuration: 0 // duration of animations when hovering an item
                    },
            responsiveAnimationDuration: 0,
            scales: {
                        yAxes: [{
                            ticks: {
                                suggestedMin: 0,
                                suggestedMax: 50
                            }
                        }]
            } // animation duration after a resize
        };
        return new Chart(ctx2, {type:type, data: barData, options:barOptions});
    }
    if (typeof z === 'undefined') {
        let z = []
        let x = []
        let lbl = null;
        let items;
        let zz;
    }
    zz = 0
    z = []
    x = []
    lbl = null;
    items = ["102,255,255","114,23,234","23,234,26","48,235,244","59,122,87","171,39,79","0,85,128"]
    for (const i of @json($all)) {
            // console.log(i)
            lbl = i.motif;
            const xxx = items[zz++].split(',')
            const ee = {
                        label: i.reg,
                        backgroundColor:`rgba(${xxx[0]}, ${xxx[1]}, ${xxx[2]},0.7)`,
                        borderColor:`rgba(${xxx[0]}, ${xxx[1]}, ${xxx[2]},1)`,
                        pointBackgroundColor: `rgba(${xxx[0]}, ${xxx[1]}, ${xxx[2]},1)`,
                        pointBorderColor: "#000000",
                        data: i.order,
                        barPercentage: 0.5,
                        barThickness: 6,
                        maxBarThickness: 8,
                        minBarLength: 2
                    }
            x.push(ee)

            const hsl = getRandomColorHsl();
            const dd = 
                    {
                        label: i.reg,
                        backgroundColor: `rgba(${xxx[0]}, ${xxx[1]}, ${xxx[2]},0.7)`,
                        borderColor: `rgba(${xxx[0]}, ${xxx[1]}, ${xxx[2]},1)`,
                        pointBackgroundColor: `rgba(${xxx[0]}, ${xxx[1]}, ${xxx[2]},0.5)`,
                        pointBorderColor: "#FFFFFF",
                        data: i.quis
                    }
            z.push(dd)
    }
    var ctx2 = document.getElementById("barChart-1").getContext("2d");
    generateChart(ctx2,z,["SS", "S", "CS", "TS","STS"],'line')
    var ctx2 = document.getElementById("barChart-2").getContext("2d");
    generateChart(ctx2,x,lbl,'bar')
</script>