
<div class="panel-body">
    <div class="row">
        <div class="col-md-12 text-center"><h4>SUMMARY ORDER TOKO PER-DEPO <hr class="hr-line-dashed no-paddings" /></h4></div>
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr class="text-center">
                        <th>NAMA DEPO </th>
                        <th>TOTAL ORDER </th>
                        <th>TOKO SURVEY</th>
                        <th>%</th>
                    </tr>
                    </thead>
                    <tbody class="text-right" >
                        @php($per_depo = [])
                        @foreach ($toko->depo as $item)
                        <tr>
                            <td class="text-left">{{$item}}</td>
                            <td>{{number_format($toko->depo_order[$loop->index],0,',','.')}}</td>
                            <td>{{number_format($sData->tot_toko_survey,0,',','.')}}</td>
                            @php($per_depo[] = $toko->depo_order[$loop->index]/$sData->tot_toko_survey)
                            <td>{{number_format($toko->depo_order[$loop->index]/$sData->tot_toko_survey,2,',','.')}}%</td>
                        </tr>
                        @endforeach
                        <tr class="bg-secondary white font-bold">
                            <td>Total</td>
                            <td>{{number_format($toko->total,0,',','.')}}</td>
                            <td>{{number_format($toko->tot_survey,0,',','.')}}</td>
                            <td>{{number_format($toko->total/$toko->tot_survey,2,',','.')}}%</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <canvas id="sparkline2"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <canvas id="sparkline3"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var cfgChartToko = function () {
			return {
				type: 'line',
				data: {
					labels: @json($toko->depo),
					datasets: [{
                            label: '{{number_format($toko->total,0,',','.')}}%',
                            borderColor: 'rgba(178,132,190,0.6)',
                            backgroundColor: 'rgba(178,132,190,8)',
                            pointBackgroundColor:'rgba(178,132,190,0.2)',
                            pointBorderColor: "#fff",
                            data: @json($toko->depo_order),
                            fill: true,
                        }
                    ]
				},
				options: {
					responsive: true,
					title: {
						display: true,
						text: 'Total Order per Depo' 
					}
				}
			};
		}
        var cfgChartToko2 = function () {
			return {
				type: 'line',
				data: {
					labels: @json($toko->depo),
					datasets: [
                        {
                            label: '{{number_format($toko->total/$toko->tot_survey,2,',','.')}}%',
                            borderColor: 'rgba(0,102,255,0.2)',
                            backgroundColor: 'rgba(0,51,255,0.2)',
                            pointBackgroundColor:'rgba(0,0,102,0.2)',
                            pointBorderColor: "#fff",
                            data: @json($per_depo),
                            fill: true,
                        }
                    ]
				},
				options: {
					responsive: true,
					title: {
						display: true,
						text: 'Total Order per Depo' 
					}
				}
			};
		}
    var ctxx = document.getElementById('sparkline2').getContext('2d');
    new Chart(ctxx, cfgChartToko());
    var ctxx = document.getElementById('sparkline3').getContext('2d');
    new Chart(ctxx, cfgChartToko2());
    
    // const nil = @json($orderCollect);
    // console.log(nil)

</script>