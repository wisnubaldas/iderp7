@php($idx = 0)
@foreach ($data_form as $item)
    <div class="row">
        <div class="col-6">
            <h4>Form <span class="text-navy">{{$item['form_id']}}</span></h4>
        </div>
        <div class="col-6">
            <h4>Nama Sales <span class="text-navy">{{$item['sales']}}</span></h4>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-striped table-sm" style="font-weight: bolder">
                <thead class="text-center" >
                    <tr>
                        <th rowspan="2" >Nama Toko</th>
                        <th colspan="5">Komentar Barang</th>
                        <th colspan="2">Order</th>
                        <th rowspan="2">Jumlah Order</th>
                    </tr>
                    <tr>
                        <th class="bg-info">Sangat Suka</th>
                        <th class="bg-info">Suka</th>
                        <th class="bg-info">Cukup Suka</th>
                        <th class="bg-info">Tidak Suka</th>
                        <th class="bg-info">Sangat Tidak Suka</th>
                        <th class="bg-success">YA</th>
                        <th class="bg-success">TIDAK </th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach ($item['cust'] as $it)
                        @php($idx++)
                        <tr class="text-center" >
                            <td class="text-left">{{$it['name_cus']}}</td>
                            <input type="radio" value="0" name="vot[{{$it['id_cus']}}]" hidden checked>
                            <td><div class="i-checks"><input data-index="{{$idx}}" type="radio" class="vote-req" value="1" name="vot[{{$it['id_cus']}}]"></div></td>
                            <td><div class="i-checks"><input data-index="{{$idx}}" type="radio" class="vote-req" value="2" name="vot[{{$it['id_cus']}}]"></div></td>
                            <td><div class="i-checks"><input data-index="{{$idx}}" type="radio" class="vote-req" value="3" name="vot[{{$it['id_cus']}}]"></div></td>
                            <td><div class="i-checks"><input data-index="{{$idx}}" type="radio" class="vote-req" value="4" name="vot[{{$it['id_cus']}}]"></div></td>
                            <td><div class="i-checks"><input data-index="{{$idx}}" type="radio" class="vote-req" value="5" name="vot[{{$it['id_cus']}}]"></div></td>
                            <td class=""><div class="i-checks-blue"><input type="radio" value="1" name="is_order[{{$it['id_cus']}}]" class="order-req" id="{{$it['id_cus']}}" customer="modal-{{$it['id_cus']}}"></div></td>
                            <td class=""><div class="i-checks-blue"><input type="radio" value="2" name="is_order[{{$it['id_cus']}}]" class="order-req" id="{{$it['id_cus']}}" checked></div></td>
                            <td><span id="jml-{{$it['id_cus']}}"></span></td>
                        </tr>
                        <div class="tmp-order" ></div>
                        <x-survey.modal-warna :customer="$it" :warna="$warna"/>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
           
@endforeach
@push('css')
<style>
   .table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
        background-color: #ffff99;
        color: #333300;
    }
</style>
@endpush
@push('script')
    <script>
        jQuery(function(){
            // let count = 0;
            // let idx;
            // $('.vote-req').on('ifChecked',function(event) {
            //     idx = event.target.attributes['data-index']['value'];
            //     console.log(idx)
            //     count++;
            //     // console.log(count)
            //     if(idx == 20)
            //     {
            //         $('#save-frm').removeAttr('disabled');
            //     }
            // })

            $('.order-req').on('ifChecked', function(event){

                const customer = event.currentTarget.attributes[4].textContent;
                const id = event.currentTarget.id;
                if(event.currentTarget.value == 1)
                {
                    $('#'+customer).modal({backdrop: 'static', keyboard: false});
                }
                if(event.currentTarget.value == 2)
                {
                         const x = $('.warna-'+id);
                        for(var i = 0; i < x.length; i++){ // dapetin input berdasarkan class
                            $(x[i]).val('');
                            const warna = $(x[i]).attr('data-warna');
                            const nilai = $(x[i]).val();
                        }
                        $('#jml-'+id).html(0)
                }

                $('.save-warna').on('click',function(){
                    // console.log(id)
                    const x = $('.warna-'+id);
                    let valueWarna = []
                    for(var i = 0; i < x.length; i++){ // dapetin input berdasarkan class
                        const nil = parseInt($(x[i]).val());
                        if(!isNaN(nil))
                        {
                            valueWarna.push(nil);
                        }else{
                            valueWarna.push(0);
                        }
                    }
                    // console.log(valueWarna)
                    const jml = valueWarna.reduce((a, b) => a + b)
                    $('#jml-'+id).html(jml)
                    if(jml == 0)
                    {
                        $('#'+id+'[value="2"]').prop('checked',true).iCheck('update');
                        $('#'+id+'[value="1"]').prop('checked',false).iCheck('update');
                    }
                })
                
            });
        })
    </script>
@endpush
   
{{-- @foreach($ds->depo_survey->customer['form_respon'] as $i)
                        <div class="row">
                            <div class="col-6">
                                <h4>Form <span class="text-navy">{{$i['form_id']}}</span></h4>
                            </div>
                            <div class="col-6">
                                <h4>Nama Sales <span class="text-navy">{{$i['sales']}}</span></h4>
                            </div>
                        </div>
                        
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Nama Toko</th>
                                    <th>Komentar Barang</th>
                                    <th>Order</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($i['customer'] as $item)
                        <tr>
                            <td>{{SurveyHelper::toko($item['id'])->nama}}</td>
                            <td>
                                <table>
                                    <tr class="p-sm m-md">
                                        <label>
                                            <input value="0" checked="checked" type="radio" hidden name="vot[{{SurveyHelper::toko($item['id'])->id}}]">
                                            <input value="1" type="radio" class="" name="vot[{{SurveyHelper::toko($item['id'])->id}}]" require>
                                            Sangat Suka
                                        </label>
                                    </tr>
                                    <tr class="p-sm m-md">
                                        <label>
                                            <input value="2" type="radio" class=" " name="vot[{{SurveyHelper::toko($item['id'])->id}}]" require>
                                            Suka
                                        </label>
                                    </tr>
                                    <tr class="p-sm m-md">
                                        <label >
                                            <input value="3" type="radio" class=" " name="vot[{{SurveyHelper::toko($item['id'])->id}}]" require>
                                            Cukup Suka
                                        </label>
                                    </tr>
                                    <tr class="p-sm m-md">
                                        <label>
                                            <input value="4" type="radio" class=" " name="vot[{{SurveyHelper::toko($item['id'])->id}}]" require>
                                            Tidak Suka
                                        </label>
                                    </tr>
                                    <tr class="p-sm m-md">
                                        <label>
                                            <input value="5" type="radio" class=" " name="vot[{{SurveyHelper::toko($item['id'])->id}}]" require>
                                            Sangat Tidak Suka
                                        </label>
                                    </tr>
                                </table>
                            </td>
                            <td class="text-navy">
                                <label class="indigo">
                                        <input value="1" type="radio" class="" onchange="order_vote('yes','{{SurveyHelper::toko($item['id'])->id}}')" name="order[{{SurveyHelper::toko($item['id'])->id}}]">
                                       YA
                                       <input value="2" type="radio" class="" onchange="order_vote('no','{{SurveyHelper::toko($item['id'])->id}}')" name="order[{{SurveyHelper::toko($item['id'])->id}}]" checked="checked" >
                                       TIDAK
                                </label>
                                <tr>
                                    <td colspan="3" >
                                        <div class="crott"  id="order-{{SurveyHelper::toko($item['id'])->id}}">
                                            <div class="form-group row">
                                            @foreach (SurveyHelper::warna($ds->warna) as $i)
                                                <label class="col-lg-1 col-form-label dark font-bold">{{$i}}</label>
                                                <div class="col-lg-2">
                                                    <input name="order[{{SurveyHelper::toko($item['id'])->id}}][{{$i}}]" type="number"  class="form-control form-control-sm p-xxs">
                                                </div>
                                            @endforeach
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                
                            </td>
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endforeach --}}