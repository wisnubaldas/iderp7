
<div class="row">
    {{-- modal kesimpulan --}}
    <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true" data-backdrop="static">
        <form method="POST" action="{{$hasil['req']}}">
            @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <i class="fa fa-clock-o modal-icon"></i>
                    <h4 class="modal-title">Kesimpulan</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Diajukan</label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control" name="diajukan">
                                <option value="Rusdy">Rusdy</option>
                                <option value="Kiendry">Kiendry</option>
                                <option value="Ananda">Ananda</option>
                            </select>
                        </div>
                    </div>
                    <textarea class="summernote form-group" name='kesimpulan'>
                        
                    </textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" >Save changes</button>
                </div>
            </div>
        </div>
    </form>
    </div>

    <div class="col-lg-12">
        <div class="ibox no-borders">
            <div class="ibox-title">
                <h5>SUMMARY RESPON PRODUK BARU</h5>
                {{-- <a href="{{$hasil['pdf_file']}}" class="btn btn-link">{{$hasil['pdf_file']}}</a> --}}
                <div class="ibox-tools">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal4">
                        <i class="fa fa-print"></i> Prin t Report
                    </button>
                    

                    {{-- <a class="btn btn-primary btn-sm" href="{{$hasil['req']}}" id="print-report">
                        <i class="fa fa-print"></i> Print Report
                    </a> --}}
                </div>
            </div>
            <div class="ibox-content ibox-heading">
                <div class="row">
                    <div class="col-4">
                        <h3>Brand: <span class="text-navy">{{$sData->ble_ketek()->brand }}</span></h3>
                    </div>
                    <div class="col-4">
                        <h3>Motif: <span class="text-navy">{{$sData->ble_ketek()->motif }}</span></h3>
                    </div>
                    <div class="col-4">
                        <h3>Ukuran: <span class="text-navy">{{$sData->ble_ketek()->ukuran }}</span></h3>
                    </div>
                </div>
                
            </div>
            <div class="ibox-content">
                {{-- @dump($sData->report_data) --}}
                {{-- @dump($sData->parsinge()->regional) --}}
                <div class="table-responsive">
                    <table class="table table-bordered tabel-hover">
                        <thead class="text-center font-bold ">
                            <tr>
                                <td rowspan="7" class="align-middle">REGIONAL</td>
                                <td rowspan="7" class="bg-gray align-middle">NAMA DEPO</td>
                                <td colspan="12" class="bg-gray align-middle" >KOMENTAR BARANG</td>
                                <td colspan="4" class="bg-gray align-middle" >PERKIRAAN QUANTITY ORDER PER ITEM SETIAP BULAN (BOX)</td>
                            </tr>
                            <tr >
                                <td rowspan="6" colspan="2" class="bg-gray align-middle">SANGAT SUKA</td>
                                <td rowspan="6" colspan="2" class="bg-gray align-middle">SUKA</td>
                                <td rowspan="6" colspan="2" class="bg-gray align-middle">CUKUP SUKA</td>
                                <td rowspan="6" colspan="2" class="bg-gray align-middle">TIDAK SUKA</td>
                                <td rowspan="6" colspan="2" class="bg-gray align-middle">SANGAT TIDAK SUKA</td>
                                <td rowspan="6" colspan="2" class="bg-gray align-middle">%</td>
                                <td rowspan="4" colspan="2" class="bg-gray align-middle">{{$sData->ble_ketek()->brand }}</td>
                                <td colspan="2" class="bg-gray text-left">Sangat Suka: >= 200 Box</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="bg-gray text-left">Suka: 100 - 199 Box</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="bg-gray text-left">Cukup Suka: 50 - 99 Box</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="bg-gray text-left">Tidak Suka: 0 - 49 Box</td>
                            </tr>
                            <tr>
                                <td colspan="{{count($sData->ble_ketek()->warna)}}" class="font-bold">JIKA "YA" MAU ORDER, HARUS DIISI DENGAN QUANTITY ORDER</td>
                            </tr>
                            <tr>
                                @foreach ($sData->ble_ketek()->warna as $i)
                                    <td class="font-bold maroon" width="10%">{{$i}}</td>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($gion as $k => $i)
                                {{-- sumary data vote per region --}}
                                @php($sData->total_order = [])
                                {{-- reset data total persentase --}}
                                @php( $sData->tot_sum_vote_depo = ['ss'=>0,'s'=>0,'cs'=>0,'ts'=>0,'sts'=>0]) 
                                @php($sData->nilai_motif = 0)
                                @foreach ($i as $idx => $it)
                                @php($x = $sData->vote($it['voting']))
                                    <tr class="text-right">
                                        @if($loop->index == 0)
                                            <td class="text-center align-middle" rowspan="{{count($i)+3}}">{{$k}}</td>
                                            <td class="text-left">{{$it['depo']}}</td>
                                        @else
                                            <td class="text-left">{{$it['depo']}}</td>
                                        @endif
                                        <td>{{$sData->sum_vote_depo($it['voting'],'ss')}}</td>
                                        <td>
                                            {{number_format($x->tot_per_vote('pss',count($i))->pss,2,',','.')}}%
                                        </td>
                                        <td>{{$sData->sum_vote_depo($it['voting'],'s')}}</td>
                                        <td>
                                            {{number_format($x->tot_per_vote('ps',count($i))->ps,2,',','.')}}%
                                        </td>
                                        <td>{{$sData->sum_vote_depo($it['voting'],'cs')}}</td>
                                        <td>
                                            {{number_format($x->tot_per_vote('pcs',count($i))->pcs,2,',','.')}}%
                                        </td>
                                        <td>{{$sData->sum_vote_depo($it['voting'],'ts')}}</td>
                                        <td>
                                            {{number_format($x->tot_per_vote('pts',count($i))->pts,2,',','.')}}%
                                        </td>
                                        <td>{{$sData->sum_vote_depo($it['voting'],'sts')}}</td>
                                        <td>
                                            {{number_format($x->tot_per_vote('psts',count($i))->psts,2,',','.')}}%
                                        </td>
                                        <td>{{$sData->tot_persen_vote(count($it['order']))}}</td>
                                        <td>
                                            {{$x->tot_per_vote('all',count($i))->p_all}}%
                                        </td>
                                        @foreach ($sData->ble_ketek()->warna as $item)
                                            <th>{{$sData->region($item,$it['order'])->order}}</th>
                                        @endforeach
                                    </tr>
                                    
                                @endforeach
                                    <tr class="text-center align-middle font-bold" style="font-size: 14px;">
                                        <td class="text-left">Sub Total</td>
                                            @php($sData->tot_all_sum[$k]= $sData->sum_by_vote_region)
                                            @php($sum_by_vote_region = $sData->sum_by_vote_region)
                                            @foreach ($sum_by_vote_region as $item)
                                                @php($stvd = $sData->sub_tot_vote_depo())
                                                @switch($loop->index)
                                                    @case(0)
                                                        <td>{{$stvd->ss}}</td>
                                                        @php($grandTot['ss'][] = $sData->grandTotal($item))
                                                        @php($sData->chart_sum_subtot($k,$stvd->ss))
                                                        @break
                                                    @case(1)
                                                        <td>{{$stvd->s}}</td>
                                                        @php($grandTot['s'][] = $sData->grandTotal($item))
                                                        @php($sData->chart_sum_subtot($k,$stvd->s))
                                                        @break
                                                    @case(2)
                                                        <td>{{$stvd->cs}}</td>
                                                        @php($grandTot['cs'][] = $sData->grandTotal($item))
                                                        @php($sData->chart_sum_subtot($k,$stvd->cs))
                                                        @break
                                                    @case(3)
                                                        <td>{{$stvd->ts}}</td>
                                                        @php($grandTot['ts'][] = $sData->grandTotal($item))
                                                        @php($sData->chart_sum_subtot($k,$stvd->ts))
                                                        @break
                                                    @case(4)
                                                        <td>{{$stvd->sts}}</td>
                                                        @php($grandTot['sts'][] = $sData->grandTotal($item))
                                                        @php($sData->chart_sum_subtot($k,$stvd->sts))
                                                        @break
                                                    @default
                                                        
                                                @endswitch
                                                <td>{{ number_format(round($item,2),2,',','.') }}</td>
                                               
                                            @endforeach
                                        @php($sData->reset_tot_vote())
                                        <td>{{$sData->sub_tot_persen}}</td>
                                        {{-- // reset subtotal persentase --}}
                                        @php($sData->sub_tot_persen = 0)
                                        <td>100%</td>
                                        @php($grandTotOrder = 0) @php($grandTotOrder += $sData->total_order()->sum())
                                        <td colspan="" rowspan="3" class="text-center align-middle  font-bold">{{ number_format($sData->total_order()->sum(),0,',','.')}}</td>
                                        {{-- end looping depo --}}
                                    </tr>
                                    <tr class="text-center align-middle font-bold bg-gray white" style="font-size: 14px;">
                                        <td class="text-left">Sub Total X Nilai Skor</td>
                                        @foreach ($sum_by_vote_region as $item)
                                            @switch($loop->index)
                                                @case(0)
                                                    @php($subTot[0] = $sData->tofloat($sData->sub_total_x($item,5)))
                                                    <td colspan="2" class="text-center">{{$subTot[0]}}</td>
                                                    @php($sData->chart_sum_nilai($k,$subTot[0]))
                                                    @break
                                                @case(1)
                                                    @php($subTot[1] = $sData->tofloat($sData->sub_total_x($item,4)))
                                                    <td colspan="2" class="text-center">{{$subTot[1]}}</td>
                                                    @php($sData->chart_sum_nilai($k,$subTot[1]))
                                                    @break
                                                @case(2)
                                                    @php($subTot[2] = $sData->tofloat($sData->sub_total_x($item,3)))
                                                    <td colspan="2" class="text-center">{{$subTot[2]}}</td>
                                                    @php($sData->chart_sum_nilai($k,$subTot[2]))
                                                    @break
                                                @case(3)
                                                    @php($subTot[3] = $sData->tofloat($sData->sub_total_x($item,2)))
                                                    <td colspan="2" class="text-center">{{$subTot[3]}}</td>
                                                    @php($sData->chart_sum_nilai($k,$subTot[3]))
                                                    @break
                                                @case(4)
                                                    @php($subTot[4] = $sData->tofloat($sData->sub_total_x($item,1)))
                                                    <td colspan="2" class="text-center">{{$subTot[4]}}</td>
                                                    @php($sData->chart_sum_nilai($k,$subTot[4]))
                                                    @break
                                                @default
                                                    
                                            @endswitch
                                        @endforeach
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr class="text-center align-middle font-bold bg-gray white">
                                        <td class="text-left">Nilai Motif</td>
                                        {{-- <td colspan="12" class="text-center">{{round($sData->nilai_motif,2)/4}}</td> --}}
                                        <td colspan="12" class="text-center">{{array_sum($subTot)}}</td>
                                    </tr>
                                {{-- @dump($i) --}}
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr class="text-center align-middle font-bold" style="font-size: 14px;">
                                <td colspan="2">Total</td>
                                @php($t_ss = round(array_sum($grandTot['ss'])/7,2))
                                <td colspan="2" class="text-center font-bold">{{$t_ss}}%</td>
                                @php($t_s = round(array_sum($grandTot['s'])/7,2))
                                <td colspan="2" class="text-center font-bold">{{$t_s}}%</td>
                                @php($t_cs = round(array_sum($grandTot['cs'])/7,2))                                   
                                <td colspan="2" class="text-center font-bold">{{$t_cs}}%</td>
                                @php($t_ts = round(array_sum($grandTot['ts'])/7,2))                                    
                                <td colspan="2" class="text-center font-bold">{{$t_ts}}%</td>
                                @php($t_sts = round(array_sum($grandTot['sts'])/7,2))
                                <td colspan="2" class="text-center font-bold">{{$t_sts}}%</td>
                                <td colspan="2" class="text-center font-bold">100%</td>
                                <td rowspan="4" colspan="3" class="text-center font-bold align-middle ">Jumlah Quantity order/box </td>
                                <td rowspan="4" class="text-center font-bold align-middle">{{$grandTotOrder}}</td>
                                @php($sData->chart_data_komentar(compact('t_ss','t_s','t_cs','t_ts','t_sts')))
                            </tr>
                            <tr class="text-center align-middle font-bold" >
                                <td colspan="2">Nilai Skor</td>
                                <td colspan="2" class="text-center font-bold">X5</td>
                                <td colspan="2" class="text-center font-bold">X4</td>
                                <td colspan="2" class="text-center font-bold">X3</td>
                                <td colspan="2" class="text-center font-bold">X2</td>
                                <td colspan="2" class="text-center font-bold">X1</td>
                                <td colspan="2" class="text-center font-bold">-</td>
                            </tr>
                            <tr class="text-center align-middle font-bold bg-gray" style="font-size: 14px;">
                                <td colspan="2">Total X Nilai Skor</td>
                                @php($txns_ss = round(array_sum($grandTot['ss'])/7*5/100,2))
                                <td colspan="2" class="text-center font-bold">{{$txns_ss}}</td>
                                @php($txns_s = round(array_sum($grandTot['s'])/7*4/100,2))
                                <td colspan="2" class="text-center font-bold">{{$txns_s}}</td>
                                @php($txns_cs = round(array_sum($grandTot['cs'])/7*3/100,2))
                                <td colspan="2" class="text-center font-bold">{{$txns_cs}}</td>
                                @php($txns_ts = round(array_sum($grandTot['ts'])/7*2/100,2))
                                <td colspan="2" class="text-center font-bold">{{$txns_ts}}</td>
                                @php($txns_sts = round(array_sum($grandTot['sts'])/7*1/100,2))
                                <td colspan="2" class="text-center font-bold">{{$txns_sts}}</td>
                                <td colspan="2" class="text-center font-bold">-</td>
                            </tr>
                            <tr class="align-middle text-center font-bold" style="font-size: 14px;">
                                <td colspan="2">Grand Total (Skala 5)</td>
                                <td colspan="12" class=" text-danger">{{array_sum(compact('txns_ss','txns_s','txns_cs','txns_ts','txns_sts'))}}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            height: 150,
            toolbar: [
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['para', ['ul', 'ol', 'paragraph']],
                    ],
        });
   });
</script>
