<div class="modal inmodal" id="modal-{{$warna_customer['id_cus']}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <i class="fa fa-laptop modal-icon"></i>
                <h4 class="modal-title">{{$warna_customer['name_cus']}}</h4>
                <p class="modal-title2">{{$warna_customer['id_cus']}}<br/> <span class="text-danger font-bold">Jumlah permintaan berdasarkan tipe motif....</span></p>
            </div>
            <div class="modal-body">
                <div class="row">
                    @foreach ($warnanya as $it)
                        @if($it)
                        <div class="col-4">
                            <div class="form-group">
                                <label>{{$it}}</label> 
                                <input type="text" class="form-control warna-{{$warna_customer['id_cus']}}" name="order[{{$warna_customer['id_cus']}}][{{$it}}]"  data-warna="{{$it}}" >
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-white btn-sm" data-dismiss="modal" >Batal</button> --}}
                <button type="button" class="btn btn-primary btn-sm save-warna" data-dismiss="modal" >Save Data</button>
            </div>
        </div>
    </div>
</div> 

