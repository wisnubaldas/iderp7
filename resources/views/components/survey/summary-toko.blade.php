@section('summary-toko')
    @php
        $order = 0; $customer = 0; $persen = [];
        $orderCollect = collect($hasil['summary_toko'])->pluck('order');
        $gion = [];
        $motif = null;
        foreach($hasil['summary_region'] as $x)
        {
            $z = [];
            if (isset($x['survey']['vote'])) {
                $z['voting'] = array_count_values($x['survey']['vote']);
            }else{
                $z['voting'] = ['1'=>0,'2'=>0,'3'=>0,'4'=>0,'5'=>0];
            }
            if (isset($x['survey']['order'])) {
                foreach ($x['survey']['order'] as $v) {
                    $z['order'][] = $v;
                    $motif = array_keys($v);
                }
            }else{
                $z['order'][] = ['££££'=>0,'£££'=>0,'££'=>0,'£'=>0];
            }
            $z['depo'] = $x['nama_depo'];
            $gion[$x['nama_reg']][] = $z;
            
        }
        $sData = new App\Helpers\Report($hasil);
        $toko = $sData->depo_order()->crot;
    @endphp
       
    @include('components.survey.summary-toko-table')
@endsection

{{-- Tabel region  --}}
@section('summary-region')
    @include('components.survey.summary-toko-detail')
@endsection

@section('summary-all')
    @include('components.survey.summary-all')
@endsection

@section('chart-survey')
    @include('components.survey.summary-chart')
@endsection