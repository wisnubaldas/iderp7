<div class="panel-body">
    <div class="row">
        <div class="col-md-12 text-center text-navy">
            <h4>Grafik Prosentase dan Jumlah Quantity Order <hr class="hr-line-dashed no-paddings" /></h4>
        </div>
        @php($gcss = $sData->generate_chart_sum_subtot())
        @php($gcsn = $sData->generate_chart_sum_nil())
        
        <div class="col-md-6">
            <div class="box">
                <div class="ibox-content text-center h-200">
                    <canvas id="sum-tot" height="200"></canvas>
                </div>
            </div>
        </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="ibox-content text-center h-200">
                        <canvas id="sum-sub" height="200"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="ibox-content text-center h-200">
                        <canvas id="sum-sum" height="200"></canvas>
                    </div>
                </div>
            </div>
    </div>
</div>
{{-- <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script> --}}
<script>

    function bikin_chart_sub_total(data,region,id,title)
    {
        let dataSet = {};
        dataSet.ss = []
        dataSet.s = [] 
        dataSet.cs = [] 
        dataSet.ts = [] 
        dataSet.sts = []
        for (const dc of data) {
            if(typeof dc[0] == 'string')
            {
                dataSet.ss.push(parseFloat(dc[0].replace(',','.')))
                dataSet.s.push(parseFloat(dc[1].replace(',','.')))
                dataSet.cs.push(parseFloat(dc[2].replace(',','.')))
                dataSet.ts.push(parseFloat(dc[3].replace(',','.')))
                dataSet.sts.push(parseFloat(dc[4].replace(',','.')))
            }else{
                dataSet.ss.push(dc[0])
                dataSet.s.push(dc[1])
                dataSet.cs.push(dc[2])
                dataSet.ts.push(dc[3])
                dataSet.sts.push(dc[4])
            }
           
        }
        let barChartData = {
            labels:region,
            datasets:[
                {
                    label: 'Sangat Suka',
                    backgroundColor:"#006600",
                    data: dataSet.ss
                },
                {
                    label: 'Suka',
                    backgroundColor:'rgba(0,204,0,0.7)',
                    data: dataSet.s
                },
                {
                    label: 'Cukup Suka',
                    backgroundColor:'rgba(0,255,255,0.7)',
                    data: dataSet.cs
                },
                {
                    label: 'Tidak Suka',
                    backgroundColor:'rgba(204,51,255,0.7)',
                    data: dataSet.ts
                },
                {
                    label: 'Sangat Tidak Suka',
                    backgroundColor:'rgba(255,0,51,0.7)',
                    data: dataSet.sts
                }
            ]
        }
        let ctz = document.getElementById(id).getContext("2d");
        let myLineChart = new Chart(ctz, {
                    type: 'bar',
                    data: barChartData,
                    options: {
                        title: {
                            display: true,
                            text: title
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                        responsive: true
                    }
        });
    }
    function bikin_chart_komentar_barang(data,region,id,title)
    {
        let barChartData = {
            labels:region,
            datasets:[
                
                {
                    label: 'Komentar Barang',
                    backgroundColor:'rgba(255,0,51,0.7)',
                    data: data,
                    datalabels: {
                                    formatter: function(value, context) {
                                        return value+ '%';
                                    },
                                    color: '#FFFF00',
                                    font:{
                                        weight:'bold',
                                        size:'15'

                                    }
                                }
                }
            ]
        }
        let ctz = document.getElementById(id).getContext("2d");
        let myLineChart = new Chart(ctz, {
                    type: 'bar',
                    data: barChartData,
                    plugins: [ChartDataLabels],
                    options: {
                        title: {
                            display: true,
                            text: title
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                        responsive: true
                    }
        });
    }
</script>

<script>

    var region = @json($gcss->region);
    var data = @json($gcss->data);
    bikin_chart_sub_total(data,region,"sum-sub",'Sub Total Vote Region')
    var datax = @json($gcsn->datax);
    bikin_chart_sub_total(datax,region,"sum-sum",'Subtotal X Nilai Skor')
    var dat_tot = @json($sData->cdkb);
    bikin_chart_komentar_barang(dat_tot,['Sangat Suka','Suka','Cukup Suka','Tidak Suka','Sangat Tidak Suka'],"sum-tot",'Komentar Barang')
</script>