<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
            <div class="ibox-content p-xl">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Kepada Yth:</h5>
                            <address>
                                <strong>{{$sj[0]['depo']}}</strong><br>
                                {{$sj[0]['alamat']}}<br>
                                <abbr title="Phone">P:</abbr> {{$sj[0]['phone']}}
                            </address>
                            <p>
                                <span><strong>Forklift:</strong> {{$sj[0]['forklif']}}</span><br/>
                                <span><strong>Checker:</strong>  {{$sj[0]['checker']}}</span>
                            </p>
                        </div>

                        <div class="col-sm-6 text-right">
                            <h4>No Surat Jalan.</h4>
                            <h4 class="text-navy">{{$sj[0]['no_sj']}}</h4>
                            <p>
                                <span><strong>Tgl Pengiriman:</strong>  {{explode(' ',$sj[0]['tgl'])[0]}}</span><br/>
                                <span><strong>No Order:</strong> {{$sj[0]['no_so']}}</span><br />
                                <span><strong>No Customer:</strong> {{$sj[0]['no_customer']}}</span><br />
                                <span><strong>No Polisi:</strong> {{$sj[0]['no_pol']}}</span><br />
                                <span><strong>No P.O:</strong> {{$sj[0]['no_po']}}</span>
                            </p>
                        </div>
                    </div>

                    <div class="table-responsive m-t">
                        <table class="table invoice-table">
                            <thead>
                            <tr>
                                <th>Code Barang</th>
                                <th>Nama Item</th>
                                <th>Banyaknya</th>
                                <th>Satuan</th>
                                <th>Keterangan</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php($tot = 0)
                                @foreach ($sj as $item)
                                @php($tot += $item['jml'])
                                <tr>
                                    <td>{{$item['kode_barang']}}</td>
                                    <td>{{$item['item_name']}}</td>
                                    <td>{{$item['jml']}}</td>
                                    <td>{{$item['satuan']}}</td>
                                    <td>{{$item['ket']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /table-responsive -->

                    <table class="table invoice-total">
                        <tbody>
                        <tr>
                            <td><strong>Total Barang :</strong></td>
                            <td>{{number_format($tot)}}</td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="text-right">
                        <a class="btn btn-primary" href="{!! route('surat_jalan.get',['sj'=>$sj[0]['no_sj'],'pdf'=>true]) !!}"><i class="fa fa-print"></i> Print Surat Jalan</a>
                    </div>

                    <div class="well m-t"><strong>Catatan: </strong>
                        <span>Warna Putih untuk Perusahaan, </span>
                        <span>Warna Merah untuk Penerima Barang, </span>
                        <span>Warna Kuning untuk Angkutan,</span>
                        <span>Warna Biru untuk Gudang, </span>
                        <span>Warna Hijau untuk Accounting, </span>
                    </div>
                </div>
        </div>
    </div>
</div>