'use strict'
class myApp {
    generateMenu()
    {
        localforage.getItem('menu').then(function(value) {
            // This code runs once the value has been loaded
            // from the offline store.
            // console.log(value);
            if(value)
            {
               const dd =  myApp.prototype.menuTemplate(value)
                jQuery(function(){
                    const m = $('#side-menu');
                    m.append(dd);
                    // let men = $('#side-menu').metisMenu()
                  
                    // $('.mm').click(function(){
                    //     $(this).find('ul').toggleClass('collapse').slow();
                    // })
                });
            }
        }).catch(function(err) {
            // This code runs if there were any errors
            console.log(err);
        });
    }
    menuTemplate(v)
    {
        let x = '';
        for (const key in v) {
            if (v.hasOwnProperty(key)) {
                const element = v[key];
                console.log(element.sub_menu.length)
                if(element.sub_menu.length >= 0)
                {
                    x += `<li class="mm">
                    <a href="${element.url}">
                        <i class="${element.icon}"></i> 
                        <span class="nav-label">${element.title}</span>
                        <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">`
                        for (const key in element.sub_menu) {
                            if (element.sub_menu.hasOwnProperty(key)) {
                                const el = element.sub_menu[key];
                                x += `<li><a href="${el.url}" >${el.title}</a></li>`
                            }
                        }
                    x += `</ul>
                </li>`;
                }else{
                    x += `<li>
                    <a href="${element.url}">
                            <i class="${element.icon}"></i> 
                            <span class="nav-label">${element.title}</span> 
                        </a>
                    </li>`;
                }
            }
        }
        return x;
    }
}
export default myApp;