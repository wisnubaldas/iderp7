/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import myApp from './myApp.js';
// import * as VueGoogleMaps from 'vue2-google-maps'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

require('./bootstrap');
require('datatables.net-bs4');
require('datatables.net-buttons-bs4');

window.Vue = require('vue');
// Install BootstrapVue
window.Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
window.Vue.use(IconsPlugin)
// import datePicker from 'vue-bootstrap-datetimepicker';
// Vue.use(VueGoogleMaps, {
//     load: {
//       key: 'AIzaSyDYxcj9zQErZ6KkleQahg_vuY2cRg5yfEU',
//       libraries: 'places', // This is required if you use the Autocomplete plugin
//       // OR: libraries: 'places,drawing'
//       // OR: libraries: 'places,drawing,visualization'
//       // (as you require)
  
//       //// If you want to set the version, you can do so:
//       // v: '3.26',
//     },
  
//     //// If you intend to programmatically custom event listener code
//     //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
//     //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
//     //// you might need to turn this on.
//     // autobindAllEvents: false,
  
//     //// If you want to manually install components, e.g.
//     //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
//     //// Vue.component('GmapMarker', GmapMarker)
//     //// then set installComponents to 'false'.
//     //// If you want to automatically install all the components this property must be set to 'true':
//     installComponents: true
//   })

// import 'bootstrap/dist/css/bootstrap.css';
// import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';

// Vue.use(datePicker);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
// import 'bootstrap-vue/dist/bootstrap-vue.css'

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
// const systemComponent = require.context('./systemComponent/', true, /\.vue$/i)
// systemComponent.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
// custom component
Vue.component('merek-component', require('./components/bloger/MerekComponent.vue').default);
Vue.component('slide-component', require('./components/bloger/SlideComponent.vue').default);
Vue.component('produk-component', require('./components/bloger/ProdukComponent.vue').default);
Vue.component('barang-component', require('./components/bloger/BarangComponent.vue').default);

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
// Library Component
// Vue.component('VueSelect', require("vue-select").default)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.mixin({
    methods: {
        bytesToSize: bytes => {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Byte';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        },
        toExcelHeader:(index) => {
		    if(index <= 0) {
		        throw new Error("index must be 1 or greater");
		    }
		    index--;
		    let charCodeOfA = 97 // ("a").charCodeAt(0); // you could hard code to 97
		    let charCodeOfZ = 122 //("z").charCodeAt(0); // you could hard code to 122
		    let excelStr = "";
		    let base24Str = (index).toString(charCodeOfZ - charCodeOfA + 1);

		    for(let base24StrIndex = 0; base24StrIndex < base24Str.length; base24StrIndex++) {
		        let base24Char = base24Str[base24StrIndex];
		        let alphabetIndex = (base24Char * 1 == base24Char) ? base24Char : (base24Char.charCodeAt(0) - charCodeOfA + 10);
		        // bizarre thing, A==1 in first digit, A==0 in other digits
		        if(base24StrIndex == 0) {
		            // alphabetIndex -= 1;
		            // console.log('alfabet--->',alphabetIndex);
		        }
		        excelStr += String.fromCharCode(charCodeOfA*1 + alphabetIndex*1);
		    }
		    return excelStr.toUpperCase();
		},
		kodeToBrand:(kode,brand) => {
			const r = kode.substr(0, 1);
			let rs = new Object
			_.forEach(brand, function(a) {
				if(a.kode == r)
				{
					rs.kode = a.kode
					rs.nama = a.nama
				}
			})
			return rs;
		}
    }
})


const app = new Vue({
    el: '#app'
});


const x = new myApp;
x.generateMenu();

