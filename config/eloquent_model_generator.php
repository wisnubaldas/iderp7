<?php

return [
    'namespace'       => 'App\Models\Blog',
    'base_class_name' => \Illuminate\Database\Eloquent\Model::class,
    'output_path'     => 'app\Models\Blog',
    'no_timestamps'   => false,
    'date_format'     => 'U',
    'connection'      => 'cjfi',
    'backup'          => false,
];