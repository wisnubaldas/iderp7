<?php
Route::namespace('Report')->prefix('report')->group(function () {
    // Aplikasi Survey
    Route::get('/survey','SurveyController@index');
    Route::get('/survey/getDepo','SurveyController@getDepo');
    Route::post('/survey/submit','SurveyController@submitSurvey');
    Route::get('/survey/status_grid','SurveyController@grid_status');
    Route::get('/survey/get_status','SurveyController@get_status');
    Route::get('/survey/get_customer','SurveyController@getCustomer');
    Route::get('/survey/custoner_depo','SurveyController@custoner_depo');
    Route::get('/survey/depo_survey/{any?}','SurveyController@depo_survey')->name('report.deposurvey');
    Route::get('/survey/proses_depo_survey/{id}','SurveyController@proses_depo_survey')->name('report.survey.proses');
    Route::get('/survey/batal_depo_survey/{id}','SurveyController@batal_depo_survey')->name('report.survey.batal');
    Route::post('/survey/save_proses_depo_survey','SurveyController@save_proses_depo_survey')->name('saveDepoProsses');
    Route::get('/survey/post_depo_survey/{id}','SurveyController@post_depo_survey');
    Route::post('/survey/post_depo_survey','SurveyController@update_data_survey');
    Route::get('/survey/review_depo/{id}/{siapa?}/{user?}','SurveyController@review_depo')->name('sign_review');
    Route::get('/survey/approve/{id}','SurveyController@approve_survey');
    Route::post('/survey/approve/{id}','SurveyController@approve_survey');
    Route::get('/survey/approve_kadep/{id}/{user}','SurveyController@approve_kadep'); // kadep dan rm approval
    Route::get('/survey/reject/{id}','SurveyController@reject_survey');
    Route::get('/survey/edit_survey/{id}','SurveyController@edit_survey');
    Route::get('/survey/print_survey/{id}','SurveyController@print_survey');
    Route::get('/survey/finish_survey/{id}','SurveyController@finish_survey');
    Route::post('/survey/upload_data_customer','SurveyController@upload_data_customer');
    Route::get('/survey/depo_review_reject/{id}','SurveyController@depo_review_reject');
    Route::post('/survey/delete_image/{id}/{name}','SurveyController@delete_image');
    Route::post('/survey/add_image/{notif_id}','SurveyController@add_image');
    Route::get('/survey/rotate_image/{id}','SurveyController@rotate_image');

    Route::post('/survey/update_survey/{notif_id}','SurveyController@update_survey');

    Route::get('/survey/report','ReportSurveyController@index');
    Route::get('/survey/report/grid','ReportSurveyController@grid');
    Route::get('/survey/report/create/{motif}','ReportSurveyController@create');
    Route::get('/survey/report/get_motif','ReportSurveyController@get_motif');
    Route::post('/survey/report/get_motif','ReportSurveyController@get_motif');
    Route::post('/survey/report/get_all_motif','ReportSurveyController@get_all_motif');
    Route::get('/survey/upload_penjualan','SurveyController@upload_penjualan');
    Route::post('/survey/upload_penjualan','SurveyController@upload_penjualan');
    Route::get('/survey/delete_survey/{id}','SurveyController@delete_survey');
    // kadep survey
    Route::get('/survey/kadep_approval','SurveyKadepController@index');
    Route::get('/survey/kadep_approval/confirm/{id}','SurveyKadepController@confirm');
    Route::get('/survey/kadep_approval/detail_survey/{id}','SurveyKadepController@detail_survey');
});