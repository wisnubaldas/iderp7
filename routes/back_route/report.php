<?php
Route::namespace('Report')->group(function () {
    Route::prefix('report')->group(function(){
        Route::get('/ranking', 'RankingController@index')->name('ranking_profit');
        Route::post('/ranking','RankingController@get_rangking');
        Route::get('/ranking/brand','RankingController@get_brand');
        Route::get('/ranking/depo','RankingController@get_depo');
  
        Route::get('/target','TargetController@index');
        Route::get('/target/get_depo','TargetController@get_depo');
        Route::post('/target/save_target','TargetController@save_target');
  
        Route::get('/penilaian','PenilaianController@index');
        Route::get('/penilaian/get_data','PenilaianController@get_data');
        Route::post('/penilaian/get_penjualan','PenilaianController@get_penjualan');
        
        // nyari Surat Jalan
        Route::get('/surat_jalan','ReportSuratJalan@index')->name('surat_jalan');
        Route::get('/surat_jalan/get','ReportSuratJalan@get_sj')->name('surat_jalan.get');
  
        // Laporan Simulasi Harga Jual
        Route::get('/lshjadlh','ReportSimHargaJualController@index')->name('lshjadlh');
        Route::post('/lshjadlh/save','ReportSimHargaJualController@save');
        Route::get('/lshjadlh/grid','ReportSimHargaJualController@grid');
        Route::get('/lshjadlh/edit/{id}','ReportSimHargaJualController@edit');
        Route::post('/lshjadlh/update/{id}','ReportSimHargaJualController@update');
        Route::post('/lshjadlh/upload','ReportSimHargaJualController@upload');
        Route::post('/lshjadlh/export','ReportSimHargaJualController@export');
  
        Route::get('/tes','ReportSurveyController@tes');
  
    });
  });