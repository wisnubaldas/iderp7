<?php
// system management user dan lain-lain
Route::namespace('Sys')->prefix('system')->group(function(){
    Route::get('/menu','MenuController@index')->name('menu');
    Route::get('/menu/grid','MenuController@grid');
    Route::get('/user_manager','UserManagerController@index')->name('user_manager');
    Route::get('/user_manager/edit/{id}','UserManagerController@edit_user')->name('user_manager.edit');
    Route::get('/user_manager/delete/{id}','UserManagerController@delete_user')->name('user_manager.delete');
  
    Route::post('/user_manager/save_user/{id}','UserManagerController@save_user');
    Route::get('/user_manager/role_permission/{status}/{permission}/{id}','UserManagerController@role_permission');
  
  
    // Route::get('/user_manager/grid','UserManagerController@grid');
    // Route::post('/user_manager/update','UserManagerController@update');
    Route::post('/user_manager/insert','UserManagerController@insert');
    Route::get('/user_manager/assign_user_role/{id}','UserManagerController@assign_user_role');
    Route::post('/user_manager/assign_user_role/{id}','UserManagerController@assign_user_role')->name('post.userroles');
    Route::post('/user_manager/menu_module/{id}','UserManagerController@menu_module')->name('post.menumodule');
    Route::get('/user_manager/create_user','UserManagerController@create_user')->name('user_manager.create');
  
    Route::get('/user_manager/get_permission','UserManagerController@get_permission');
    Route::get('/rolebase','RolebaseController@index');
    Route::get('/rolebase/grid','RolebaseController@grid');
    Route::post('/rolebase/{stat}','RolebaseController@insert')->name('create.role');
    
  });