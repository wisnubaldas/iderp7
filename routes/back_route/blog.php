<?php
Route::namespace('Blog')->group(function(){
    Route::prefix('blog')->group(function(){
      // Route::get('/depo','DepoController@index');
      // Route::post('/depo/store','DepoController@store');
      // Route::get('/depo/grid','DepoController@grid');
      // Route::post('/depo/delete','DepoController@delete');
      // Route::resource('merek', 'MerekController');
      Route::resource('slide', 'SlideController');
      // Route::resource('produk', 'ProdukController');
      // Route::resource('barang', 'BarangController');
    });
  });