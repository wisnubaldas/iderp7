<?php
Route::namespace('Finance')->prefix('finance')->group(function(){
    Route::get('/sample','SampleController@index');
    Route::get('/sample/get_data','SampleController@get_data');
    Route::post('/sample/post_data','SampleController@post_data');

});