<?php

use Illuminate\Support\Facades\Route;
use App\Traits\AppTrait;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include(base_path('routes').'/frontend.php');
// Route::get('/', function () {
//     return view('frontend.welcome');
// })->name('welcome');

// backend route
Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
    'confirm' => false,
  ]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/menu/{id}','HomeController@get_menu')->name('get_menu');

Route::group(['middleware' => ['auth']], function () {
  AppTrait::include_route_files(__DIR__.'/back_route/');
});