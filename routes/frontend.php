<?php
// front end route
// Route::get('/','WelcomeController@index')->name('welcome');
Route::get('/','Auth\LoginController@showLoginForm')->name('welcome');

Route::get('/apprx/{id}/{siapa}','WelcomeController@approve')->name('review.survey');
Route::get('/php_info','WelcomeController@php_info');
Route::get('/distributor','WelcomeController@distributor');

Route::namespace('Frontend')->group(function(){
  Route::get('/merek/{brand?}/{produk?}','MerekController@index')->name('merek');
  Route::get('/atena','AtenaController@index')->name('atena');
});