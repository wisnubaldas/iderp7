/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/views/parsial_script/survey.js":
/*!**************************************************!*\
  !*** ./resources/views/parsial_script/survey.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

jQuery(function () {
  $('#depo').select2();
  $('#brand-brand').select2();
  $('#ukuran').select2();
}); // depo proses blade

jQuery(function () {
  var count = 0;
  $('.belekok').on('ifChecked', function (event) {
    $('#count').html(++count + " Selected");
  });
  $('.belekok').on('ifUnchecked', function (event) {
    $('#count').html(--count + " Selected");
  });
  $('#frmDepoSurvey').on('submit', function (a) {
    a.preventDefault();
    var d = $(this).serializeArray();
    axios.post(saveDepoProsses, d).then(function (response) {// console.log(response);
      // window.location = "/report/survey/depo_survey";
    })["catch"](function (error) {
      // console.log(error);
      alert('error server');
    });
  });
});
jQuery(function () {
  $('#btn-grp').on('click', function (params) {
    // console.log($('#grp').val())
    // console.log(customerDepo)
    var jml = $('#grp').val();
    console.log(_typeof(jml));

    if (jml === '') {
      swal("Ingin membuat berapa form survey?", "Group survey harus di isi minimal 1 maksimal 10", "error");
      return false;
    }

    if (jml > 0 && jml <= 10) {
      // buat tab nya
      var options = []; // // nested table buat select customer
      //     events = ['onChoose','onStart','onEnd','onAdd','onUpdate','onSort','onRemove',
      //         'onChange','onUnchoose','afterMove'].forEach(function (name) {
      //             options[name] = function (evt) {
      //                 // console.log({
      //                 //     'event': name,
      //                 //     'this': this,
      //                 //     'item': evt.item,
      //                 //     'from': evt.from,
      //                 //     'to': evt.to,
      //                 //     'oldIndex': evt.oldIndex,
      //                 //     'newIndex': evt.newIndex
      //                 // });
      //                 // console.log(evt)
      //             };
      //         });
      // console.log(options)

      options.chosenClass = 'chosen';
      options.group = 'nested';
      options.dataIdAttr = 'data-id';
      options.animation = 100;
      nestData = {
        id: 'master',
        data: customerDepo
      };
      var x = tmplNest(nestData);
      $('.dd').html(x);
      yy = [];
      idFrm = [];
      tabHead = [];

      for (var index = 0; index < jml; index++) {
        var idForm = 'FRP' + Math.floor(100000 + Math.random() * 900000);
        idFrm.push(idForm);
        var y = tmplGrpSurvey(idForm);
        yy.push(y);
        tabHead.push(tmplTabHeader(idForm));
      } // tempelin ke dom


      $('#form-respon').html(yy.join(''));
      $('.form-tab').html(tabHead.join(''));
      $('.harga-depo').on('input paste keyup', function () {
        $(".harga-depo").val($(this).val());
      }); ///--------------------------------

      var foo = document.getElementById('nestable');
      Sortable.create(foo, options);
      idFrm.forEach(function (a) {
        var fuck = document.getElementById(a);
        Sortable.create(fuck, options);
      }); // save data form

      $('#save').on('click', function (a) {
        a.preventDefault(); // console.log(dataSurvey)

        var dataPost = [];
        idFrm.forEach(function (a) {
          // console.log(a)
          var fuck = document.getElementById(a); // Sortable.create(fuck, options);

          var nestedQuery = a;
          var identifier = 'id';

          function serialize(sortable) {
            // DAPETIN ARRAY DARI FORM NYA
            var serialized = [];
            var children = [].slice.call(sortable.children);

            for (var i in children) {
              // console.log(children[i].textContent);
              // var nested = children[i].querySelector(nestedQuery);
              serialized.push({
                id: children[i].dataset[identifier] // children: nested ? serialize(nested) : []

              });
            }

            return serialized;
          }

          var salesName = $('#sales-' + a).val();
          var hargaDepo = $('.harga-depo').val(); // validate

          if (salesName == '' || hargaDepo == '') {
            return false;
          }

          var x = {
            form_id: a,
            sales: salesName,
            harga_depo: hargaDepo,
            customer: serialize(fuck)
          };
          dataPost.push(x); // console.log(serialize(fuck))
        }); // ond foreach
        // hook calidasi form survey
        // console.log(dataPost);

        if (dataPost.length == 0) {
          swal("Sales Harus dan harga depo di isi", "Nama sales dan harga depo untuk setiap form survey harus ada", "error");
          return false;
        }

        postDataKeServerBangke({
          survey_no: depoAtrr.survey_no,
          id_reg: depoAtrr.depo_detail.id_reg,
          id_depo: depoAtrr.depo_detail.id_depo,
          nama_depo: depoAtrr.depo_detail.nama_depo,
          brand: depoAtrr.brand,
          ukuran: depoAtrr.ukuran.title,
          motif: depoAtrr.motif,
          warna: depoAtrr.warna,
          form_respon: dataPost
        }); // console.log(depoAtrr)
        // console.log(dataPost)
      });
    } else {
      swal("Ingin membuat " + jml + " form survey?", "Group survey harus di isi minimal 1 maksimal 10", "error");
    }
  }); // post data generate form survey dari depo

  var postDataKeServerBangke = function postDataKeServerBangke(data) {
    swal({
      title: "Simpan form data survey?",
      text: "Teliti terlebih dahulu sebelum di cetak, tekan cancel jika ingin memperbaiki data!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#28a745",
      confirmButtonText: "Simpan",
      closeOnConfirm: false
    }, function () {
      axios.post(saveDepoProsses, data).then(function (response) {
        // handle success
        console.log(response);
        swal("Tersimpan!", "Data tersimpan dengan baik dan benar", "success");
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {
        // always executed
        window.location = '/report/survey/depo_survey';
      });
    });
  }; // buat template nestable


  function tmplNest(grpId) // default data customer template
  {
    var d = grpId.data.map(function (a) {
      return "<li class=\"b-r-md\" data-id=\"".concat(a.id_toko, "/").concat(a.nama_toko, "\">\n                <a href=\"#").concat(a.id_toko, "\" class=\"list-group-item list-group-item-primary list-group-item-action\">\n                    <span class=\"text-danger\" >").concat(a.id_toko, "</span> ").concat(a.nama_toko, "</a></li>");
    });
    var x = "<ul id=\"nestable\" class=\"list-group list-unstyled\">".concat(d.join(""), "</ul>   ");
    return x.replace(/(\r\n|\n|\r)/gm, "");
  }

  function tmplGrpSurvey(id) {
    var s = "<div role=\"tabpanel\" id=\"tab-".concat(id, "\" class=\"tab-pane\">\n                <div class=\"panel-body\">\n                <div class=\"row no-paddings no-margins\">\n                    <div class=\"form-group row form-group-sm col-6\">\n                        <label class=\"col-lg-4 col-form-label\">Nama Sales</label>\n                        <div class=\"col-lg-8\">\n                            <input type=\"text\" name=\"sales\" id=\"sales-").concat(id, "\" class=\"form-control form-control-sm\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row form-group-sm col-6  \">\n                    <label class=\"col-lg-5 col-form-label\">Perkiraan Harga Depo</label>\n                    <div class=\"col-lg-7\">\n                        <input type=\"number\" name=\"harga-depo\" id=\"harga-").concat(id, "\" class=\"form-control form-control-sm harga-depo\">\n                    </div>\n                </div>\n                </div>\n                <div class=\"hr-line-solid .border-size-md no-paddings\"></div>\n                <div class=\"bg-muted\">\n                    <ul id=\"").concat(id, "\" class=\"list-group list-unstyled list-group-primary\"></ul>\n                </div>\n                <div class=\"hr-line-solid .border-size-md no-paddings\"></div>\n                    </div>\n                </div>");
    return s.replace(/(\r\n|\n|\r)/gm, "");
  }

  function tmplTabHeader(id) {
    return "<li><a class=\"nav-link\" data-toggle=\"tab\" href=\"#tab-".concat(id, "\"> ").concat(id, "</a></li>");
  }
});

/***/ }),

/***/ 1:
/*!********************************************************!*\
  !*** multi ./resources/views/parsial_script/survey.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/iderp7/resources/views/parsial_script/survey.js */"./resources/views/parsial_script/survey.js");


/***/ })

/******/ });