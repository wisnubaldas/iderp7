/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/views/parsial_script/survey.js":
/*!**************************************************!*\
  !*** ./resources/views/parsial_script/survey.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

jQuery(function () {
  $('#depo').select2();
  $('#brand-brand').select2();
  $('#ukuran').select2();
}); // depo proses blade

jQuery(function () {
  var count = 0;
  $('.belekok').on('ifChecked', function (event) {
    $('#count').html(++count + " Selected");
  });
  $('.belekok').on('ifUnchecked', function (event) {
    $('#count').html(--count + " Selected");
  });
  $('#frmDepoSurvey').on('submit', function (a) {
    a.preventDefault();
    var d = $(this).serializeArray();
    axios.post(saveDepoProsses, d).then(function (response) {
      console.log(response);
      window.location = "/report/survey/depo_survey";
    })["catch"](function (error) {
      // console.log(error);
      alert('error server');
    });
  });
});
jQuery(function () {
  $('#btn-grp').on('click', function (params) {
    // console.log($('#grp').val())
    // console.log(customerDepo)
    var jml = $('#grp').val();

    if (jml > 0 && jml <= 10) {
      var options = []; // nested table buat select customer

      events = ['onChoose', 'onStart', 'onEnd', 'onAdd', 'onUpdate', 'onSort', 'onRemove', 'onChange', 'onUnchoose', 'afterMove'].forEach(function (name) {
        options[name] = function (evt) {// console.log({
          //     'event': name,
          //     'this': this,
          //     'item': evt.item,
          //     'from': evt.from,
          //     'to': evt.to,
          //     'oldIndex': evt.oldIndex,
          //     'newIndex': evt.newIndex
          // });
          // console.log(evt)
        };
      }); // console.log(options)

      var dataSurvey = []; // temporary data survey nya

      options.chosenClass = 'chosen';
      options.group = 'nested';
      options.dataIdAttr = 'data-id';
      options.animation = 100;

      options.onEnd = function (evt) {
        if (typeof evt.item != "undefined") {
          // collect data dari form2 yang di generate
          var idForm = evt.item.parentNode.id;
          var dataForm = evt.item.dataset.id;
          var dariFrom = evt.from.id; // console.log(idForm)
          // console.log(depoAtrr)

          console.log(dataSurvey[evt.from.id]);

          if (_typeof(dataSurvey[idForm]) !== 'object') {
            // dataSurvey[idForm] = []
            dataSurvey[idForm] = [dataForm];
          } else {
            if (dataSurvey[idForm].indexOf(dataForm) == -1) // cek data array yang sama
              {
                if (dataSurvey[evt.from.id]) {
                  delete dataSurvey[evt.from.id][dataForm];
                }

                dataSurvey[idForm].push(dataForm);
              }
          } // console.log($('#sales-'+idForm).val())

        }

        console.log(dataSurvey);
      };

      $('#save').on('click', function (a) {
        a.preventDefault();
        console.log(dataSurvey);
      });
      nestData = {
        id: 'master',
        data: customerDepo
      };
      var x = tmplNest(nestData);
      $('.dd').html(x);
      yy = [];
      idFrm = [];

      for (var index = 0; index < jml; index++) {
        var idForm = 'FRP' + Math.floor(100000 + Math.random() * 900000);
        idFrm.push(idForm);
        var y = tmplGrpSurvey(idForm);
        yy.push(y);
      } // tempelin ke dom


      $('#form-respon').html(yy.join(''));
      var foo = document.getElementById('nestable');
      Sortable.create(foo, options);
      idFrm.forEach(function (a) {
        var fuck = document.getElementById(a);
        Sortable.create(fuck, options);
      });
    } else {
      alert('jumlah group survey minimal 1 maksimal 10');
    }
  }); // buat template nestable

  function tmplNest(grpId) {
    var d = grpId.data.map(function (a) {
      return "<li class=\"b-r-md\" data-id=\"".concat(a.id_toko, "\"><a href=\"#").concat(a.id_toko, "\" class=\"list-group-item list-group-item-primary list-group-item-action\">").concat(a.id_toko, "/").concat(a.nama_toko, "</a></li>");
    });
    var x = "<ul id=\"nestable\" class=\"list-group list-unstyled\">".concat(d.join(""), "</ul>");
    return x.replace(/(\r\n|\n|\r)/gm, "");
  }

  function tmplGrpSurvey(id) {
    var s = "<div class=\"col-6\">\n                    <div class=\"ibox\">\n                        <div class=\"ibox-title border-bottom\">\n                            <label>Form Respon Produk Baru ".concat(id, "</label>\n                            <input type=\"text\" name=\"sales\" id=\"sales-").concat(id, "\" placeholder=\"Sales Name\" class=\"form-control form-control-sm\">\n                        </div>\n                        <div class=\"ibox-content\">\n                            <ul id=\"").concat(id, "\" class=\"list-group list-unstyled list-group-primary\">\n                            </ul>\n                        </div>\n                    </div>\n                </div>");
    return s.replace(/(\r\n|\n|\r)/gm, "");
  }
});

/***/ }),

/***/ 1:
/*!********************************************************!*\
  !*** multi ./resources/views/parsial_script/survey.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/iderp7/resources/views/parsial_script/survey.js */"./resources/views/parsial_script/survey.js");


/***/ })

/******/ });